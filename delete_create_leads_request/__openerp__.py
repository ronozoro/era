# -*- coding: utf-8 -*-
{
    'name': "Delete Create Leads Request",

    'summary': """
        This model delete quick create""",

    'description': """
      This model delete quick create from leads and buyer requst and seller request
          """,

    'author': "ITSystem=corporation -- Eman Ahmed",
    'website': "www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'crm',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','crm','mt_crm_brokerage_manager'],

    # always loaded
    'data': [
        'views/crm_lead_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}