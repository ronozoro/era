# -*- coding: utf-8 -*-
{
    'name': "Survey Partner Domain",

    'description': """
        Logged in user only views his existing contacts in partner mail survey form
    """,

    'author': "Ahmed Tarek - IT Systems",
    'website': "www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Marketing',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','survey'],

    # always loaded
    'data': [
        'survey_email_compose_message.xml',
    ],

}