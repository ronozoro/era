# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.osv import osv


class CaseCategory(osv.osv):
    _inherit = 'mt.case.categ'
    _rec_name = 'categ_type'

    categ_type = fields.Selection([('residential', 'Residential'),
                                   ('commercial', 'Commercial'),
                                   ('projects', 'Projects')], string="Type", required=True)


class CrmLeadInherit(models.Model):
    _inherit = 'crm.lead'

    property_request_ids = fields.Many2many('product.template', 'crm_product_rel', string="Property")
    buyer_request_ids = fields.Many2many('crm.lead', 'crm_lead_rel', 'buyer1', 'buyer2', string="Request")
    agent_mobile = fields.Char(related='user_id.mobile', string='Agent Mobile', readonly=True)

    unit_mobile = fields.Char(related='operating_unit_id.unit_mobile', string='Branch Mobile', readonly=True)


    @api.multi
    def search_property_criteria(self):
        domain = []
        for rec in self:
            #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            #                       residential + rent buyer
            #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            if (rec.category.categ_type == 'residential') and (rec.service_ids == 'buyer_rent'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))

                if rec.category:
                    domain.append(('category.categ_type', '=', 'residential'))

                if rec.service_ids:
                    domain.append(('service_type', '=', 'seller_rent'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2 ))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))



                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

                # if rec.budget:
                #     domain.append(('asking_price', '>=', (float(rec.budget) - ((float(rec.budget) * 12)/100))))

                # ==============================================================
                #               residential + Buy buyer
                # ==============================================================

            elif (rec.category.categ_type == 'residential') and (rec.service_ids == 'buy'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'residential'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'sale'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

                # ==============================================================
                #               residential + share buyer
                # ==============================================================
            elif (rec.category.categ_type == 'residential') and (rec.service_ids == 'share_buyer'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'residential'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'share_seller'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))


            # ******************************************************************************************************************
            #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            #                       residential + rent seller
            #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            elif (rec.category.categ_type == 'residential') and (rec.service_ids == 'seller_rent'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'residential'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'seller_rent'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

                # ==============================================================
                #               residential + Sell seller
                # ==============================================================

            elif (rec.category.categ_type == 'residential') and (rec.service_ids == 'sale'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'residential'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'sale'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

                # ==============================================================
                #               residential + share seller
                # ==============================================================
            elif (rec.category.categ_type == 'residential') and (rec.service_ids == 'share_seller'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'residential'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'share_seller'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))


            # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            #             commercial
            # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            #                       commercial + rent buyer
            #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            elif (rec.category.categ_type == 'commercial') and (rec.service_ids == 'buyer_rent'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                # if rec.bedrooms_no_ids.id:
                #     domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'commercial'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'seller_rent'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))



                # ==============================================================
                #               commercial + Buy buyer
                # ==============================================================

            elif (rec.category.categ_type == 'commercial') and (rec.service_ids == 'buy'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                # if rec.bedrooms_no_ids.id:
                #     domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'commercial'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'sale'))


                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

                # ==============================================================
                #               commercial + share buyer
                # ==============================================================
            elif (rec.category.categ_type == 'commercial') and (rec.service_ids == 'share_buyer'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                # if rec.bedrooms_no_ids.id:
                #     domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'commercial'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'share_seller'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))


            # ******************************************************************************************************************
            #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            #                       commercial + rent seller
            #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            elif (rec.category.categ_type == 'commercial') and (rec.service_ids == 'seller_rent'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                # if rec.bedrooms_no_ids.id:
                #     domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'commercial'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'seller_rent'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

                # ==============================================================
                #               commercial + Sell seller
                # ==============================================================

            elif (rec.category.categ_type == 'commercial') and (rec.service_ids == 'sale'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                # if rec.bedrooms_no_ids.id:
                #     domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'commercial'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'sale'))


                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

                # ==============================================================
                #               commercial + share seller
                # ==============================================================
            elif (rec.category.categ_type == 'commercial') and (rec.service_ids == 'share_seller'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                # if rec.bedrooms_no_ids.id:
                #     domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'commercial'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'share_seller'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))


            # &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            #                     "Projects"
            # &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            #                       projects + rent buyer
            #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            elif (rec.category.categ_type == 'projects') and (rec.service_ids == 'buyer_rent'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                if rec.payment:
                    domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'projects'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'seller_rent'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

                # ==============================================================
                #               projects + Buy buyer
                # ==============================================================

            elif (rec.category.categ_type == 'projects') and (rec.service_ids == 'buy'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                if rec.payment:
                    domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'projects'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'sale'))


                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

                # ==============================================================
                #               projects + share buyer
                # ==============================================================
            elif (rec.category.categ_type == 'projects') and (rec.service_ids == 'share_buyer'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                if rec.payment:
                    domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'projects'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'share_seller'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

                # ******************************************************************************************************************
                #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
                #                       projects + rent seller
                #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            elif (rec.category.categ_type == 'projects') and (rec.service_ids == 'seller_rent'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                if rec.payment:
                    domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'projects'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'seller_rent'))

                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

                # ==============================================================
                #               projects + Sell seller
                # ==============================================================

            elif (rec.category.categ_type == 'projects') and (rec.service_ids == 'sale'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                if rec.payment:
                    domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'projects'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'sale'))


                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))


                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

                # ==============================================================
                #               projects + share seller
                # ==============================================================
            elif (rec.category.categ_type == 'projects') and (rec.service_ids == 'share_seller'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                if rec.payment:
                    domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_type_id', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'projects'))
                if rec.service_ids:
                    domain.append(('service_type', '=', 'share_seller'))


                if rec.budget2:
                    if rec.currency_id.name == 'EGP':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'USD':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                    if rec.currency_id.name == 'EUR':
                        domain.append(('asking_price', '<=', (rec.budget2 + (rec.budget2 * 12) / 100)))
                        domain.append(('asking_price', '>=', rec.budget2))
                        domain.append(('currency_id', '=', rec.currency_id.id))

                # if rec.budget2:
                #     domain.append(('asking_price', '<=', (float(rec.budget2) + ((float(rec.budget2) * 12) / 100))))

        properties = self.env['product.template'].search(domain)
        self.property_request_ids = properties
        # return self.property_ids

    @api.multi
    def search_buyer_criteria(self):
        domain = []
        for rec in self:
            # ******************************************************************************************************************
            #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            #                       residential + rent seller
            #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            if (rec.service_ids == 'seller_rent') and (rec.category.categ_type == 'residential'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_ids', '=', rec.property_ids.id))
                if rec.category.categ_type:
                    domain.append(('category.categ_type', '=', 'residential'))

                if rec.service_ids:
                    domain.append(('service_ids', '=', 'buyer_rent'))

                if rec.request_type:
                    domain.append(('request_type', '=', 'buyer'))

                if rec.asking_price:
                    # domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                    if rec.currency_id3.name == 'EGP':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'USD':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'EUR':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                # ==============================================================
                #               residential + Sell seller
                # ==============================================================

            elif (rec.service_ids == 'sale') and (rec.category.categ_type == 'residential'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_ids', '=', rec.property_ids.id))

                if rec.category.categ_type:
                    domain.append(('category.categ_type', '=', 'residential'))

                if rec.service_ids:
                    domain.append(('service_ids', '=', 'buy'))
                #
                if rec.request_type:
                    domain.append(('request_type', '=', 'buyer'))

                if rec.asking_price:
                    if rec.currency_id3.name == 'EGP':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'USD':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'EUR':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))


                # if rec.asking_price:
                #     domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))

                # ==============================================================
                #               residential + share seller
                # ==============================================================
            elif (rec.service_ids == 'share_seller') and (rec.category.categ_type == 'residential'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_ids', '=', rec.property_ids.id))
                if rec.category.categ_type:
                    domain.append(('category.categ_type', '=', 'residential'))
                if rec.service_ids:
                    domain.append(('service_ids', '=', 'share_buyer'))

                if rec.request_type:
                    domain.append(('request_type', '=', 'buyer'))


                if rec.asking_price:
                    if rec.currency_id3.name == 'EGP':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'USD':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'EUR':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                # if rec.asking_price:
                #     domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    #             commercial
                    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                # ******************************************************************************************************************
                #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
                #                       commercial + rent seller
                #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            elif (rec.category.categ_type == 'commercial') and (rec.service_ids == 'seller_rent'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                # if rec.bedrooms_no_ids.id:
                #     domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_ids', '=', rec.property_ids.id))

                if rec.category:
                    domain.append(('category.categ_type', '=', 'commercial'))

                if rec.service_ids:
                    domain.append(('service_ids', '=', 'buyer_rent'))

                if rec.request_type:
                    domain.append(('request_type', '=', 'buyer'))

                # if rec.asking_price:
                #     domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))

                if rec.asking_price:
                    if rec.currency_id3.name == 'EGP':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'USD':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'EUR':
                        domain.append(('budget2', '<=', (rec.asking_price + ((rec.asking_price * 12) / 100))))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                # ==============================================================
                #               commercial + Sell seller
                # ==============================================================

            elif (rec.category.categ_type == 'commercial') and (rec.service_ids == 'sale'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                # if rec.bedrooms_no_ids.id:
                #     domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_ids', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'commercial'))

                if rec.service_ids:
                    domain.append(('service_ids', '=', 'buy'))
                if rec.request_type:
                    domain.append(('request_type', '=', 'buyer'))


                if rec.asking_price:
                    if rec.currency_id3.name == 'EGP':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'USD':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'EUR':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                # if rec.asking_price:
                #     domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                # ==============================================================
                #               commercial + share seller
                # ==============================================================
            elif (rec.category.categ_type == 'commercial') and (rec.service_ids == 'share_seller'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                # if rec.bedrooms_no_ids.id:
                #     domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                # if rec.payment:
                #     domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_ids', '=', rec.property_ids.id))

                if rec.category:
                    domain.append(('category.categ_type', '=', 'commercial'))

                if rec.service_ids:
                    domain.append(('service_ids', '=', 'share_buyer'))

                if rec.request_type:
                    domain.append(('request_type', '=', 'buyer'))

                if rec.asking_price:
                    if rec.currency_id3.name == 'EGP':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'USD':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'EUR':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                # if rec.asking_price:
                #     domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))

            # &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            #                     "Projects"
            # &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

                # ******************************************************************************************************************
                #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
                #                       projects + rent seller
                #     == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
            elif (rec.category.categ_type == 'projects') and (rec.service_ids == 'seller_rent'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                if rec.payment:
                    domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_ids', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'projects'))
                if rec.service_ids:
                    domain.append(('service_ids', '=', 'buyer_rent'))

                if rec.request_type:
                    domain.append(('request_type', '=', 'buyer'))

                if rec.asking_price:
                    if rec.currency_id3.name == 'EGP':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'USD':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'EUR':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                # if rec.asking_price:
                #     domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                # ==============================================================
                #               projects + Sell seller
                # ==============================================================

            elif (rec.category.categ_type == 'projects') and (rec.service_ids == 'sale'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                if rec.payment:
                    domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_ids', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'projects'))

                if rec.service_ids:
                    domain.append(('service_ids', '=', 'buy'))

                if rec.request_type:
                    domain.append(('request_type', '=', 'buyer'))


                # if rec.asking_price:
                #     domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                if rec.asking_price:
                    if rec.currency_id3.name == 'EGP':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'USD':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'EUR':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                # ==============================================================
                #               projects + share seller
                # ==============================================================
            elif (rec.category.categ_type == 'projects') and (rec.service_ids == 'share_seller'):
                if rec.district_ids.id:
                    domain.append(('district_ids', '=', rec.district_ids.id))

                if rec.neighborhood_ids.id:
                    domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

                if rec.bedrooms_no_ids.id:
                    domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

                if rec.payment:
                    domain.append(('payment', '=', rec.payment))

                if rec.property_ids.id:
                    domain.append(('property_ids', '=', rec.property_ids.id))
                if rec.category:
                    domain.append(('category.categ_type', '=', 'projects'))

                if rec.service_ids:
                    domain.append(('service_ids', '=', 'share_buyer'))

                if rec.request_type:
                    domain.append(('request_type', '=', 'buyer'))

                if rec.asking_price:
                    if rec.currency_id3.name == 'EGP':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'USD':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                    if rec.currency_id3.name == 'EUR':
                        domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
                        domain.append(('budget2', '>=', rec.asking_price))
                        domain.append(('currency_id', '=', rec.currency_id3.id))

                # if rec.asking_price:
                #     domain.append(('budget2', '<=', (rec.asking_price + (rec.asking_price * 12) / 100)))
        req = self.env['crm.lead'].search(domain)
        self.buyer_request_ids = req
        # return self.property_request_ids
        # return self.property_ids




class ProductTemplate(models.Model):

    _inherit = 'product.template'

    @api.model
    def _get_default_operating_unit(self):
        team_id = self.env['crm.case.section']._get_default_team_id()
        print(team_id)
        team = self.env['crm.case.section'].sudo().browse(team_id)
        if team.operating_unit_id:
            return team.operating_unit_id
        else:
            return self.env['res.users'].operating_unit_default_get(self._uid)

    operating_unit_id = fields.Many2one('operating.unit', 'Operating Unit',
                                        related='agent_id.default_operating_unit_id',
                                        default=_get_default_operating_unit)

    unit_mobile = fields.Char(related='operating_unit_id.unit_mobile', string='Branch Mobile', readonly=True)

