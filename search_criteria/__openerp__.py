# -*- coding: utf-8 -*-
{
    'name': "search_criteria",

    'summary': """
        This Module Enhance in Search Criteria""",

    'description': """
        Enhance in Search Criteria 
    """,

    'author': "Ahmed Gaber",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base' , 'mt_crm_brokerage_manager' ,'mt_crm_proprety' , 'client_req_enhancement' ],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}