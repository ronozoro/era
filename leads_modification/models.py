# -*- coding: utf-8 -*-

from openerp import models, fields, api

class leads_modification(models.Model):
    _inherit = 'crm.lead'


    def on_change_nom(self, cr, uid, ids, service_ids, view_id=None, view_type='form', context=None, toolbar=False,
                      submenu=False):
        res = {}
        if service_ids:
            if service_ids in ('sale', 'seller_rent', 'share_seller'):
                return {'value': {'request_type': 'seller',
                                  'is_buyer': False
                                  }}
            else:
                if service_ids in ('buy', 'buyer_rent', 'share_buyer'):
                    return {'value': {'request_type': 'buyer',
                                      'is_buyer': True
                                      }}
        return {'value': {'request_type': None,
                          }}
