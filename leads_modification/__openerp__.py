# -*- coding: utf-8 -*-
{
    'name': "Leads Modification",

    'summary': """
        In Leads -> Show/hide (Property) field according to Service Type""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Ahmed Tarek - IT Systems",
    'website': "www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Sale',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','client_req_enhancement'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],

}