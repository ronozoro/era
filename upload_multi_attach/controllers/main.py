# -*- coding: utf-8 -*-
import StringIO
import base64
import functools
import logging
import mimetypes
import os
import zipfile

import simplejson
import werkzeug

from openerp import http
from openerp.addons import mail, web
from openerp.addons.web.controllers.main import content_disposition
from openerp.http import request, serialize_exception as _serialize_exception

_logger = logging.getLogger(__name__)


def serialize_exception(f):
    @functools.wraps(f)
    def wrap(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception, e:
            _logger.exception("An exception occured during an http request")
            se = _serialize_exception(e)
            error = {
                'code': 200,
                'message': "Odoo Server Error",
                'data': se
            }
            return werkzeug.exceptions.InternalServerError(simplejson.dumps(error))

    return wrap


class Binary(web.controllers.main.Binary):

    @http.route('/web/binary/upload_attachment', type='http', auth="user")
    @serialize_exception
    def upload_attachment(self, callback, model, id, ufile, multi=False):

        if multi:
            Model = request.session.model('ir.attachment')
            out = """<script language="javascript" type="text/javascript">
                    var win = window.top.window;
                    win.jQuery(win).trigger(%s, %s);
                </script>"""
            attachment_id=False
            try:
                extensions = {".jpg", ".png", ".gif", ".jpeg"}
                if (model == 'product.template' or model=='crm.lead') and any(ufile.filename.lower().endswith(ext) for ext in extensions):
                    model_internal='product.image'
                    if model=='crm.lead':
                        model_internal='lead.image'
                    Model = request.session.model(model_internal)

                    if model=='product.template':
                        Model.create({
                            'name': ufile.filename,
                            'image_alt': ufile.filename,
                            'product_tmpl_id': int(id),
                            'image': base64.encodestring(ufile.read()),
                            'image_small': base64.encodestring(ufile.read())
                        })
                    else:

                        img_base64=base64.encodestring(ufile.read())
                        print ('111111111111111111111111',img_base64)
                        Model.create({
                            'name': ufile.filename,
                            'image_alt': ufile.filename,
                            'lead_id': int(id),
                            'image': img_base64,
                            'image_small': img_base64
                        })
                        property_id=request.session.model('crm.lead').browse(int(id)).property.id
                        print ('2222222222222222222222222222222222', img_base64)
                        request.session.model('product.image').create({
                            'name': ufile.filename,
                            'image_alt': ufile.filename,
                            'product_tmpl_id': int(property_id),
                            'image': img_base64,
                            'image_small':img_base64
                        })
                else:
                    attachment_id = Model.create({
                        'name': ufile.filename,
                        'datas': base64.encodestring(ufile.read()),
                        'datas_fname': ufile.filename,
                        'res_model': model,
                        'res_id': int(id)
                    }, request.context)
                    args = {
                        'filename': ufile.filename,
                        'id': attachment_id
                    }

            except Exception:
                args = {'error': "Something horrible happened"}
                return out % (simplejson.dumps(callback), simplejson.dumps(args))
            if model == 'product.template':
                return out % (simplejson.dumps(callback), simplejson.dumps({'filename': '',
                                                                            'id': False}))
            else:
                return str(attachment_id)
        else:
            return super(Binary, self).upload_attachment(callback, model, id, ufile)


Binary()


class MailController(mail.controllers.main.MailController):

    @http.route('/mail/download_attachment', type='http', auth='user')
    def download_attachment(self, model, id, method, attachment_id, downloadall=False, **kw):
        if downloadall:
            message_id = request.registry.get('mail.message').browse(request.cr, request.session.uid, [int(id)], {})
            if message_id.parent_id:
                zip_subdir = message_id.parent_id.subject or 'attachment'
            else:
                zip_subdir = message_id.subject or 'attachment'
            filenames = []
            for attachment_id in message_id.attachment_ids:
                Model = request.registry.get(model)
                res = getattr(Model, method)(request.cr, request.uid, int(id), int(attachment_id))

                if res:
                    filecontent = base64.b64decode(res.get('base64'))
                    filename = res.get('filename')
                    content_type = mimetypes.guess_type(filename)
                    attachment_file = open(str("/tmp/%s" % filename), "wb")
                    attachment_file.write(filecontent)
                filenames.append('/tmp/%s' % filename)

            zip_filename = "%s.zip" % zip_subdir.replace(" ", "").lower()

            strIO = StringIO.StringIO()

            zip_file = zipfile.ZipFile(strIO, "w", zipfile.ZIP_DEFLATED)

            for file_path in filenames:
                file_dir, file_name = os.path.split(str(file_path))
                zip_path = os.path.join(zip_subdir, file_name)

                zip_file.write(file_path, zip_path)
            zip_file.close()

            return request.make_response(
                strIO.getvalue(),
                headers=[('Content-Type', 'application/x-zip-compressed'),
                         ('Content-Disposition', content_disposition(zip_filename))])
        else:
            return super(MailController, self).download_attachment(model, id, method, attachment_id)


MailController()
