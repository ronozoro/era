# -*- coding: utf-8 -*-
{
    "name": "Mail Multi Attachments", 
    "version": "1.0",
    "summary": "Upload & Download multiple attachments in the mail at once",
    "description": """ """,
    "depends": ["mail"],
    'data': ["views/mail_multi_attach.xml"],
    'qweb': ['static/src/xml/*.xml'],
    "installable": True, 
    "auto_install": False
}
