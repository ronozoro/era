# -*- coding: utf-8 -*-
{
    'name': "Hide Reference ERA",

    'summary': """
        Hide Reference and Reference 2 in Request conditions tab in Request view """,

    'description': """
        Hide Reference and Reference 2 in Request conditions tab in Request view
    """,

    'author': "ITSystem corporation -  By doaa khaled",
    'website': "http://www.itsyscorp.com",

    'category': 'CRM',
    'version': '0.1',

    'depends': ['base','crm'],

    'data': [

        'views/crm_lead_view.xml',

    ],

}