# -*- coding: utf-8 -*-


from openerp.osv import fields, osv
from openerp.tools.translate import _
import uuid
import time
import datetime
import openerp.addons.decimal_precision as dp

class mt_search_property(osv.osv_memory):

    _name = 'mt.search_property'


    def inprog_button_ok(self, cr, uid, ids, context=None):


        data_obj = self.pool.get('ir.model.data')
#         id1 = data_obj._get_id(cr, uid, 'mt_crm_proprety', 'mt_property_kanban')
#         if id1:
#                 id1 = data_obj.browse(cr, uid, id1, context=context).res_id
        id2 = data_obj._get_id(cr, uid, 'mt_crm_proprety', 'product_template_form_view_b2b')
        if id2:
                id2 = data_obj.browse(cr, uid, id2, context=context).res_id
        id3 = data_obj._get_id(cr, uid, 'mt_crm_proprety', 'mt_developer_tree')
        if id3:
                id3 = data_obj.browse(cr, uid, id3, context=context).res_id

        value = {

                'context':context  ,
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'product.template',
                'view_id': False,
                'views': [(id3,'tree'),(id2,'form')],
#                 'domain': [['category', '!=', False]],
                'type': 'ir.actions.act_window',
                    }

        return value

    def on_change_category(self, cr, uid, ids, category, context=None):
        if context is None:
            context = {}
        lst = []
        res = {'value': {'type_lead': '0'}}
        catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', '=', category)],
                                                          context=context)
        propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
        categ_type = self.pool.get('mt.case.categ').browse(cr, uid, category, context=context)

        if category:
            if categ_type.name == 'Residential':
                res = {'domain': {'property_type_id': [('id', 'in', propr.ids)]}, 'value': {'type_lead': '0'}}

            elif categ_type.name == 'Commercial':
                res = {'domain': {'property_type_id': [('id', 'in', propr.ids)]}, 'value': {'type_lead': '1'}}
            # res = {'domain': {'property_type_id': [('id', 'in', propr.ids)]}}
        else:
            res = {'domain': {'property_type_id': [('id', 'in', lst)]}}

        return res


    def on_change_property_type_id(self, cr, uid, ids, property_type_id, context=None):
        lst = []
        if not property_type_id:
            return {'domain': {'property_type_id': [('id', '=', lst)]}}

        type_prop = type_villa = '0'
        #         prop = self.browse(cr, uid, ids)
        if property_type_id:
            #             ids = property_ids[0][2]
            #             if ids and len(ids) > 0:

            for categ in self.pool.get('mt.property.type').browse(cr, uid, property_type_id):

                if categ.name in ('Ground floor with Garden', 'Villa'):
                    type_prop = '1'
                if categ.name == 'Villa':
                    type_villa = '1'

        return {'value': {'type_prop': type_prop, 'type_villa': type_villa}}

#
    _columns ={

#                 'name': fields.char('name'),
                'category': fields.many2one('mt.case.categ', 'Division'),
                'property_type_id': fields.many2one('mt.property.type', 'Property Type'),
                'currency_id': fields.many2one('res.currency', string='Currency'),
                'area_sqm': fields.float('Garden Area (Sqm)'),
                'finsishing_id': fields.many2one('mt.finsishing', 'Finishing'),
                'neighborhood_ids': fields.many2one('mt.neighborhood', string='Neighborhood'),
                'district_ids': fields.many2one('mt.city', string='District'),
                'view_ids': fields.many2one('mt.project.view', 'View'),
                'facilities_ids': fields.many2one('mt.facilities', 'Facilities'),
                'bathrooms_no_ids': fields.many2one('mt.bathrooms', string='No. of Bathrooms'),
                'blaconies_nbr': fields.selection([('un', '1'),
                                    ('deux', ' 2'),
                                    ('trois', '3'),
                                    ('quatre', '4'),
                                    ('cinq', '5'),
                                    ('six', '6'),
                                    ('sept', '7')], "No. of Balconies"),
                'reception_nbr': fields.selection([('un', '1'),
                                    ('deux', ' 2'),
                                    ('trois', '3'),
                                    ('quatre', '4'),
                                    ('cinq', '5'),
                                    ('six', '6'),
                                    ('sept', '7')], "No. of Receptions"),
               'kitchen_type_id': fields.many2one('mt.kitchen_type', 'Kitchen Type'),
               'asking_price_moins_que':fields.float('Maximum asking price'),
               'asking_price_plus_que':fields.float('Minimum asking price'),
               'type_lead':fields.char('type_lead'),
               }






mt_search_property()
