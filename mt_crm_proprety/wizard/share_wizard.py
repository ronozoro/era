from openerp import models, fields, api


class ShareWizard(models.TransientModel):
    _name = 'share.wizard.social'

    name = fields.Char(readonly=True)
    active_id = fields.Char()
    active_model = fields.Char()
    share_url = fields.Char(readonly=True)

    @api.model
    def default_get(self, fields):
        res = super(ShareWizard, self).default_get(fields)
        res['active_model'] = self._context.get('active_model')
        res['active_id'] = self._context.get('active_id')
        if self._context.get('active_id') != 'product.template' and self._context.get('prod_id'):
            active_record = self.env['product.template'].browse(self._context.get('prod_id'))
            res['name'] = active_record.name
            res['share_url'] = active_record.request_url()
        else:
            active_record = self.env[res['active_model']].browse(res['active_id'])
            res['name'] = active_record.name
            res['share_url'] = active_record.request_url()
        return res
