# -*- coding: utf-8 -*-
from openerp.osv import osv
from openerp.report import report_sxw


class project(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        self.ids_to_print = context.get('active_ids', False)
        super(project, self).__init__(cr, uid, name, context=context)
        active_id = context.get('active_ids', False)
        self.localcontext.update({
        'get_line_project' : self._get_line_project,
         
        })
   
    
    
    
    def _get_line_project(self):
        project_obj=self.pool.get('product.template')
        
        res=[]
        for project in project_obj.browse(self.cr,self.uid,self.ids_to_print) :
            line={
                'name':project.name ,
                'delivery_date': project.delivery_date,
                'division_id': project.division_id.name,
                'state_id': project.state_id.name,
                'developper_id': project.developper_id.name,
                'land_rea': project.land_rea,
                'building_number': project.building_number,
                  }
            res.append(line)
        return   res
      
class report_projects(osv.AbstractModel):
    _name = 'report.mt_crm_proprety.report_project'
    _inherit = 'report.abstract_report'
    _template = 'mt_crm_proprety.report_project'
    _wrapped_report_class = project