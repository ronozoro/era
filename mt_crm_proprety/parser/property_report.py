# -*- coding: utf-8 -*-
from openerp.osv import osv
from openerp.report import report_sxw


class Property(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        self.ids_to_print = context.get('active_ids', False)
        super(Property, self).__init__(cr, uid, name, context=context)
        active_id = context.get('active_ids', False)
        self.localcontext.update({
        'get_line_property' : self._get_line_property,
         
        })
   
    
    
    
    def _get_line_property(self):
        property_obj=self.pool.get('product.template')
        
        res=[]
        for prop in property_obj.browse(self.cr,self.uid,self.ids_to_print) :
            line={
                'name':prop.name ,
                'reference_property': prop.reference_property,
                'create_date': prop.create_date,
                'service_type': prop.service_type,
                'district_ids': prop.district_ids.name,
                'neighborhood_ids': prop.neighborhood_ids.name,
                'project_id': prop.project_id.name,
                'agent_id': prop.agent_id.name,
                'partner_id': prop.partner_id.name,
                'sale_price': prop.sale_price,
                'finsishing_id': prop.finsishing_id.name,
                'property_sorting_id': prop.property_sorting_id.name,
                'asking_price': prop.asking_price,
                'states': prop.states,
                  }
            res.append(line)
        return   res
      
class report_property(osv.AbstractModel):
    _name = 'report.mt_crm_proprety.report_property'
    _inherit = 'report.abstract_report'
    _template = 'mt_crm_proprety.report_property'
    _wrapped_report_class = Property