# -*- coding: utf-8 -*-
from openerp.osv import osv
from openerp.report import report_sxw


class developers(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        self.ids_to_print = context.get('active_ids', False)
        super(developers, self).__init__(cr, uid, name, context=context)
        active_id = context.get('active_ids', False)
        self.localcontext.update({
        'get_line_developers' : self._get_line_developers,
         
        })
   
    
    
    
    def _get_line_developers(self):
        developers_obj=self.pool.get('product.template')
        
        res=[]
        for dev in developers_obj.browse(self.cr,self.uid,self.ids_to_print) :
            line={
                'name':dev.name ,
                'create_date': dev.create_date,
                  }
            res.append(line)
        return   res
      
class report_developers(osv.AbstractModel):
    _name = 'report.mt_crm_proprety.mt_crm_proprety.report_developer'
    _inherit = 'report.abstract_report'
    _template = 'mt_crm_proprety.report_developer'
    _wrapped_report_class = developers