#  -*- coding: utf-8 -*-

{
    'name': 'Egyptian Property',
    'version': '0.1',
    'author': 'MT-Consulting',
    'category': 'Brokerage',
    'complexity': 'easy',
    'description': '''
        Brokerage Module
    ''',
    'website': 'http://www.innoviacc.com',
    'depends': ['mt_crm_brokerage_manager', 'web_tree_image', 'web_rtl', 'web_kanban', 'account','website_sale'],
    'data': [

        # data
        'wizard/share_wizard_view.xml',
        'data/project_division.xml',
        'data/facilities_data.xml',
        "data/project_view_data.xml",
        "data/product_id_data.xml",
        "data/finishing_data.xml",
        "data/group.xml",
        #             "data/rules.xml",
        "data/groups.xml",
        "data/rules_new.xml",

        # security
        'security/ir.model.access.csv',
        #             'security/product.template.csv',
        # views
        #             "views/assets.xml",
        'views/menu_ignored.xml',
        'views/project_view.xml',
        'views/proprety_view.xml',
        'views/developer_view.xml',
        'views/proprety_view_hide.xml',
        # wizard
        'wizard/mt_search_property.xml',

        # report
        'report/report.xml',
        'report/report_property.xml',
        'report/report_projects.xml',
        'report/report_developer.xml',
    ],
    'installable': True,
    'auto_install': False,
}
#  vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
