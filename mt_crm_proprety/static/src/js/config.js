odoo
		.define(
				'slnee_communication.slnee_communication',
				function(require) {
					"use strict";
					var core = require('web.core');
					var ajax = require('web.ajax');
					var Dialog = require('web.Dialog');
					var QWeb = core.qweb;
					var _t = core._t;

					$(document)
							.ready(
									function() {

										//load_transactions();
										function load_transactions() {
											return ajax
													.jsonRpc(
															'/slnee_communication/transactions',
															'call', {})
													.then(
															function(results) {
																if (results
																		|| results.length > 0) {
																	var content = '';
																	_
																			.each(
																					results,
																					function(
																							item) {
																						content += '<tr class="clickable-row" data-href="web#id='
																								+ item['id']
																								+ '&view_type=form&model=transaction.message"><td>'
																								+ item['name']
																								+ '</td><td>'
																								+ item['subject']
																								+ '</td><td>'
																								+ item['instructionname']
																								+ '</td></tr>';
																					});

																	if (content != '') {
																		$(
																				'<div id="dialog" title="معاملات جديدة"><table class="o_list_view table table-condensed table-striped" id="transactions"><thead><th> المسلسل</th><th>الموضوع</th><th>الإجراء</th></thead><tbody></tbody></table></div>')
																				.prependTo(
																						'body');
																		$(
																				"#transactions tbody")
																				.html(
																						content);

																		$(
																				"#dialog")
																				.dialog(
																						{
																							modal : true,
																							resizable : false,

																							dialogClass : 'ui-dialog-osx',
																							buttons : [ {
																								text : "إغلاق",
																								"class" : "btn btn-primary btn-sm ",
																								click : function() {
																									$(
																											this)
																											.dialog(
																													"close");
																								}
																							} ],

																							maxWidth : '500px',
																							width : "500",
																							show : 'bounce',
																							hide : 'blind',
																							open : function() {
																										$(
																												this)
																												.parent()
																												.css(
																														"padding",
																														"0px"),
																										$(
																												this)
																												.css(
																														"padding",
																														"0px")
																							},

																						});
																	}

																}
															});
										}
										;
										setTimeout(load_transactions, 2000);
										$(function() {
											$(document)
													.on(
															'click',
															'.clickable-row',
															function() {
																window.document.location = $(
																		this)
																		.data(
																				"href");
																$('#dialog')
																		.dialog(
																				"close");
															});
										});

									});

				});
