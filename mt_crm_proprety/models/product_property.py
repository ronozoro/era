from openerp import models, api


class Property(models.Model):
    _inherit = 'product.template'

    @api.multi
    def request_url(self):
        local_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        # local_url = 'http://era-egypt.com'
        property_url = '/residential/property/%s' % (str(self.id))
        final_url = local_url + property_url
        base_url = "https://www.facebook.com/sharer/sharer.php?u=%s&t=%s menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600" % (
            final_url, self.name)
        return base_url


Property()
