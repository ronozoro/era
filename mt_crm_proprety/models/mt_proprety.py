# -*- coding: utf-8 -*-
import datetime

import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv
from openerp.tools.translate import _
import os
from openerp.tools import image_resize_image
from openerp import SUPERUSER_ID, tools
from openerp.modules.module import get_module_resource
from openerp import tools, api, addons, SUPERUSER_ID
import openerp.addons.decimal_precision as dp


# -------------------------------------------------------------------------------
# crm_lead
# -------------------------------------------------------------------------------
class crm_lead(osv.osv):
    _inherit = "crm.lead"
    _columns = {
        'finsishing_id': fields.many2one('mt.finsishing', 'Finishing'),
    }


crm_lead()


# -------------------------------------------------------------------------
# crm_proprety
# -------------------------------------------------------------------------
class crm_proprety(osv.osv):
    _inherit = "product.template"
    _order = "create_date desc"

    def _default_state_id(self, cr, uid, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        ref = mod_obj.get_object_reference(
            cr, uid, 'mt_egyptian_cities', 'res_country_state_eg_1')
        if ref[1]:
            return ref[1]
        return False

    def _default_country_id(self, cr, uid, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        ref = mod_obj.get_object_reference(cr, uid, 'base', 'eg')
        if ref[1]:
            return ref[1]
        return False

    def _default_SQM(self, cr, uid, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        uom = mod_obj.get_object_reference(
            cr, uid, 'mt_crm_brokerage_manager', 'product_uom_sqm0')
        if uom[1]:
            return uom[1]
        return False

    def _default_currency(self, cr, uid, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        uom = mod_obj.get_object_reference(
            cr, uid, 'base', 'EGP')
        if uom[1]:
            return uom[1]
        return False

    def _get_default_image(self, cr, uid, context=None):
        image_path = get_module_resource('mt_crm_proprety', 'static/src/img/', 'images.png')
        return tools.image_resize_image_big(open(image_path, 'rb').read().encode('base64'))

    def action_count(self, cr, uid, ids, field_name, arg, context=None):
        result = dict.fromkeys(ids, 0)
        #         lead = self.pool.get('crm.lead').browse(
        #             cr, uid, ids, context=context).phone
        cr.execute(
            "select count (*) from crm_lead where type = 'opportunity' and request_type='buyer' and property ='%s'" % str(
                ids[0]))
        value = cr.fetchone()[0] or 0
        result[ids[0]] = value
        return result

    def action_count_lead(self, cr, uid, ids, field_name, arg, context=None):
        result = dict.fromkeys(ids, 0)
        #         lead = self.pool.get('crm.lead').browse(
        #             cr, uid, ids, context=context).phone
        cr.execute(
            "select count (*) from crm_lead where type = 'lead' and property ='%s'" % str(ids[0]))
        value = cr.fetchone()[0] or 0
        result[ids[0]] = value
        return result

    _columns = {
        'request_domain': fields.char('Select Requests', oldname='domain'),
        'request_id': fields.many2one('crm.lead', "Request"),
        'prop_id': fields.many2one('product.template', string='Show', domain=[('product_type', '=', 'property')]),
        'action_count': fields.function(action_count, type='integer'),
        'action_count_lead': fields.function(action_count_lead, type='integer'),
        'product_type': fields.selection([('property', 'Property'),
                                          ('project', ' Project'),
                                          ('developer', 'Developer'), ], "Type"),
        'reference_property': fields.char("Property ID", readonly=True),
        'created_time': fields.datetime("Created Time", readonly=True),
        'created_by': fields.many2one('res.users', "Created By"),
        'partner_id': fields.many2one('res.partner', "Owner"),
        'states': fields.selection([('available', 'Available'),
                                    ('Reserved', ' Reserved'),
                                    ('blocked', 'Blocked'),
                                    ('rented', 'Rented'),
                                    ('removed', 'Removed'),
                                    ('sold', 'Sold')], "State"),
        'property_type_id': fields.many2one('mt.property.type', 'Property Type'),
        'service_type': fields.selection(
            [('buy', 'Buy'), ('sale', 'Sale'), ('buyer_rent', 'Rent (Buyer)'), ('seller_rent', 'Rent (Seller)'),
             ('share_buyer', 'Share (Buyer)'), ('share_seller', 'Share (Seller)')], 'Service Type', translate=True),
        'agent_id': fields.many2one("res.users", string="Agent"),
        'reffered': fields.boolean("Referral Source"),
        'hide_request': fields.boolean("Hide Request"),
        'reffered_text': fields.char("Referral Source Description"),
        'outside_broker': fields.boolean("Outside Broker"),
        'outside_broker_text': fields.char("Outside Broker Description"),
        'country_id': fields.many2one('res.country', 'Country'),
        'state_id': fields.many2one('res.country.state', 'City'),
        'city_id': fields.many2one('mt.city', 'District'),
        'street': fields.char('Street Address'),
        'zip': fields.char('ZIP'),
        'street2': fields.char('Street'),
        'agreement': fields.selection(
            [('none', 'None'), ('normal', 'Normal'), ('exclusive', 'Exclusive'), ('standard', ' Standard'), ],
            'Agreement Type'),
        'agreement_serial': fields.char('Agreement Serial'),
        'start_date': fields.date('Start Date'),
        'end_date': fields.date('End Date'),
        'payment': fields.selection([('cash', 'Cash'), ('installments', 'Installments')], 'Payment type'),
        'area': fields.char("Area"),
        'finsishing_id': fields.many2one('mt.finsishing', 'Finishing'),
        'view_ids': fields.many2one('mt.project.view', 'View'),
        'age': fields.char("Age", size=64),
        'facilities_ids': fields.many2many('mt.facilities', 'property_facilities_rel', 'property_id', 'facilities_id',
                                           'Facilities'),
        'appliances_ids': fields.many2many('mt.appliances', 'property_appliances_rel', 'property_id', 'appliances_id',
                                           'Appliances'),
        'amenitie_id': fields.many2many('mt.amenities', 'property_amenities_rel', 'property_id', 'amenities_id',
                                        "Amenities"),
        'features_id': fields.many2many('mt.features', 'property_features_rel', 'property_id', 'features_id',
                                        "Features"),
        'licenses_id': fields.many2one('mt.licenses', "Licenses"),
        'origine_id': fields.many2one('crm.lead', "Origine"),
        'comment': fields.text("Comments"),
        'type_lead': fields.char('Type'),
        'type_prop': fields.char('Type'),
        'category': fields.many2one('mt.case.categ', 'Category List'),
        'property_sorting_id': fields.many2one('mt.property.sorting', 'Property Sorting'),
        'neighborhood_ids': fields.many2one('mt.neighborhood', string='Neighborhood'),
        'district_ids': fields.many2one('mt.city', string='District'),
        'project_id': fields.many2one('product.template', string='Project', domain=[('product_type', '=', 'project')]),
        'bedrooms_no_ids': fields.many2one('mt.bedrooms', string='No. of Bedrooms'),
        'bathrooms_no_ids': fields.many2one('mt.bathrooms', string='No. of Bathrooms'),
        'blaconies_nbr': fields.selection([('un', '1'),
                                           ('deux', ' 2'),
                                           ('trois', '3'),
                                           ('quatre', '4'),
                                           ('cinq', '5'),
                                           ('six', '6'),
                                           ('sept', '7')], "No. of Balconies"),
        'reception_nbr': fields.selection([('un', '1'),
                                           ('deux', ' 2'),
                                           ('trois', '3'),
                                           ('quatre', '4'),
                                           ('cinq', '5'),
                                           ('six', '6'),
                                           ('sept', '7')], "No. of Receptions"),
        'kitchen_type_id': fields.many2one('mt.kitchen_type', 'Kitchen Type'),

        'area_sqm': fields.float('Garden Area (Sqm)', digits_compute=dp.get_precision('Account')),
        'product_unit_id8': fields.many2one('product.uom', 'unit of mesure'),
        'bua_gross': fields.float('BUA Gross (Sqm)'),
        'bua_gross2': fields.float('BUA Gross (Sqm)'),
        'bua_net': fields.float('BUA Net (Sqm)'),
        'gardean_area': fields.float('Garden Area (Sqm)'),
        'roof_area': fields.float('Roof Area (Sqm)'),
        'commer_fron': fields.float('Commercial Frontage M'),
        'ceiling_hei': fields.float('Ceiling Height M'),
        'land_area': fields.float('Land Area from'),
        'land_area_to': fields.float('Land Area from'),
        'product_unit_id': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id1': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id2': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id3': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id4': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id5': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id6': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id7': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id12': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id13': fields.many2one('product.uom', 'unit of mesure'),
        'asking_price': fields.float('Asking Price', digits_compute=dp.get_precision('Account')),
        'sale_price': fields.float('Sale Price', digits_compute=dp.get_precision('Account')),
        'asking_price_moins_que': fields.function(lambda *a, **k: {}, method=True,
                                                  type='float', string="Asking price moins que"),
        'asking_price_plus_que': fields.function(lambda *a, **k: {}, method=True,
                                                 type='float', string="Asking price plus que"),
        'comment': fields.text("Comment", track_visibility='onchange'),
        # Commercial inforamtion
        'total_space_erea': fields.float('Total space area'),
        'number_floors': fields.integer('Number of floors'),
        'floor_area': fields.float('Floor area'),
        'available_floors': fields.integer('Available floors'),
        'currency_id': fields.many2one('res.currency', 'Currency')
    }

    #     def _default_admin(self, cr, uid, context=None):
    #         manager_karim = self.pool.get('res.users').search(cr, uid, [('login', 'ilike', 'karim.fahim@era-egypt.com')], context=context)
    #         if manager_karim :
    #             return manager_karim[0]
    #         return False

    _defaults = {
        'product_unit_id': _default_SQM,
        'product_unit_id1': _default_SQM,
        'product_unit_id2': _default_SQM,
        'product_unit_id3': _default_SQM,
        'product_unit_id4': _default_SQM,
        'product_unit_id5': _default_SQM,
        'product_unit_id6': _default_SQM,
        'product_unit_id7': _default_SQM,
        'product_unit_id8': _default_SQM,
        'product_unit_id12': _default_SQM,
        'product_unit_id13': _default_SQM,
        'reference_property': lambda obj, cr, uid, context: '/',
        'created_time': datetime.date.today(),
        'created_by': lambda self, cr, uid, context: uid,
        'agent_id': lambda self, cr, uid, context: uid,
        #         'admin_id':_default_admin,
        'states': 'available',
        'state': 'draft',
        'state_id': _default_state_id,
        'country_id': _default_country_id,
        'image_medium': _get_default_image,
        'currency_id': _default_currency,
    }

    def act_view_request(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        list = []
        list2 = []
        list3 = []
        if context and context.get('domain', False):
            mailing_domain = context.get('domain')
            if type(mailing_domain) == str:
                list = mailing_domain.split('[')
                list2 = list[3].split(']')
                list3 = list2[0].split(',')
            else:
                list3 = mailing_domain[0][2]

            data_obj = self.pool.get('ir.model.data')
            id1 = data_obj._get_id(cr, uid, 'mt_crm_brokerage_manager', 'view_crm_lead_tree')
            if id1:
                id1 = data_obj.browse(cr, uid, id1, context=context).res_id
            id2 = data_obj._get_id(cr, uid, 'crm', 'crm_case_form_view_oppor')
            if id2:
                id2 = data_obj.browse(cr, uid, id2, context=context).res_id

            value = {

                'context': context,
                'view_type': 'form',
                'view_mode': 'tree,form ',
                'res_model': 'crm.lead',
                'view_id': False,
                'views': [(id1, 'tree'), (id2, 'form')],
                'domain': [('id', 'in', list3)],
                'type': 'ir.actions.act_window',
            }

            return value

    def name_search(self, cr, uid, name='', args=None, operator='ilike', context=None, limit=100):
        if context and context.get('domain', False):
            list = []
            list2 = []
            list3 = []
            mailing_domain = context.get('domain')
            if type(mailing_domain) == str:
                list = mailing_domain.split('[')
                list2 = list[3].split(']')
                list3 = list2[0].split(',')
            else:
                list3 = mailing_domain[0][2]
            search_ids = self.search(cr, uid, [('id', 'in', list3)], context=context)
            return self.name_get(cr, uid, search_ids, context)
        return super(crm_proprety, self).name_search(cr, uid, name, args, operator, context, limit)

    def create(self, cr, uid, vals, context=None):
        id = super(crm_proprety, self).create(cr, uid, vals, context=context)
        values = {}
        if vals.get('reference_property', '/') == '/':
            vals['reference_property'] = self.pool.get('ir.sequence').get(cr, uid, 'product.template') or '/'
        vals['prop_id'] = id
        super(crm_proprety, self).write(cr, uid, id, vals, context=context)
        return id

    def write(self, cr, uid, ids, vals, context=None):
        # stage change: update date_last_stage_update
        #         id = super(crm_lead, self).write(cr, uid, ids, vals, context=context)
        if not ids:
            return True
        if isinstance(ids, (int, long)):
            ids = [ids]
        image_path = get_module_resource('mt_crm_proprety', 'static/src/img/', 'sold.png')
        image_sold = tools.image_resize_image_big(open(image_path, 'rb').read().encode('base64'))
        if vals.get('states', False) == 'sold':
            vals.update({'image_medium': image_sold})

        return super(crm_proprety, self).write(cr, uid, ids, vals, context=context)

    def onchange_city_id(self, cr, uid, ids, city_id):
        res = {}
        if city_id:
            city = self.pool.get('mt.city').browse(cr, uid, city_id)
            res['value'] = {
                'zip': city.zip,
                'city': city.long_name,
                'country_id': city.country_id.id,
                'state_id': city.state_id.id,
                'city_id': city.id,
                'district_ids': city.id,
            }
            res['domain'] = {'neighborhood_ids': [('id', 'in', city.neighborhood_ids.ids)]}
        return res

    def onchange_city_id2(self, cr, uid, ids, city_id):
        res = {}
        if city_id:
            city = self.pool.get('mt.city').browse(cr, uid, city_id)

            res['domain'] = {'neighborhood_ids': [('id', 'in', city.neighborhood_ids.ids)]}
        return res

    def on_change_service_type(self, cr, uid, id, category, service_type):
        result = {}
        name_all = ""
        if service_type:
            if category:
                categ = self.pool.get('mt.case.categ').browse(cr, uid, category)
                mod_obj = self.pool.get('ir.model.data')
                # service_type = buy
                ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'categ_oppor20')
                if service_type == 'buy' and categ.id == ref[1]:
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_buy_residential')
                    if ref2[1]:
                        name_all = ref2[1]
                ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'categ_oppor21')
                if service_type == 'buy' and categ.id == ref[1]:
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_buy_commercial')
                    if ref2[1]:
                        name_all = ref2[1]
                # service_type = rent
                ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'categ_oppor20')
                if service_type in ('buyer_rent', 'seller_rent') and categ.id == ref[1]:
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_rent_residential')
                    if ref2[1]:
                        name_all = ref2[1]
                ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'categ_oppor21')
                if service_type in ('buyer_rent', 'seller_rent') and categ.id == ref[1]:
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_rent_commercial')
                    if ref2[1]:
                        name_all = ref2[1]
                # service_type = share
                ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'categ_oppor20')
                if service_type in ('share_buyer', 'share_seller') and categ.id == ref[1]:
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_rent_residential')
                    if ref2[1]:
                        name_all = ref2[1]
                ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'categ_oppor21')
                if service_type in ('share_buyer', 'share_seller') and categ.id == ref[1]:
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_rent_commercial')
                    if ref2[1]:
                        name_all = ref2[1]
                # service_type = sale
                ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'categ_oppor20')
                if service_type == 'sale' and categ.id == ref[1]:
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_rent_residential')
                    if ref2[1]:
                        name_all = ref2[1]
                ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'categ_oppor21')
                if service_type == 'sale' and categ.id == ref[1]:
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_rent_commercial')
                    if ref2[1]:
                        name_all = ref2[1]
            else:
                categ = self.pool.get('mt.case.categ').browse(cr, uid, category)
                mod_obj = self.pool.get('ir.model.data')
                # service_type = buy
                if service_type == 'buy':
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_buy')
                    if ref2[1]:
                        name_all = ref2[1]
                # service_type = rent
                if service_type in ('buyer_rent', 'seller_rent'):
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_rent')
                    if ref2[1]:
                        name_all = ref2[1]
                # service_type = share
                if service_type in ('share_buyer', 'share_seller'):
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_rent')
                    if ref2[1]:
                        name_all = ref2[1]
                # service_type = sale
                if service_type == 'sale':
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_rent')
                    if ref2[1]:
                        name_all = ref2[1]

        result['public_categ_ids'] = [(6, 0, [name_all])]
        return {'value': result}
        # ---------------------------------------------------------------------------

    #         return  {}
    #
    # def on_change_category(self, cr, uid, ids, category, service_type, context=None):
    #     if context is None:
    #         context = {}
    #     lst = []
    #     res = {'value': {'type_lead': '0'}}
    #     catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', '=', category)],
    #                                               context=context)
    #     propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
    #
    #     if category:
    #         res = {'domain': {'property_type_id': [('id', 'in', propr.ids)]}}
    #     else:
    #         res = {'domain': {'property_type_id': [('id', 'in', lst)]}}
    #
    #     return res



    # def on_change_property_type_id(self, cr, uid, ids, property_type_id, context=None):
    #     lst = []
    #     if not property_type_id:
    #         return {'domain': {'property_type_id': [('id', '=', lst)]}}
    #
    #     type_prop = type_villa = '0'
    #     #         prop = self.browse(cr, uid, ids)
    #     if property_type_id:
    #         #             ids = property_ids[0][2]
    #         #             if ids and len(ids) > 0:
    #
    #         for categ in self.pool.get('mt.property.type').browse(cr, uid, property_type_id):
    #
    #             if categ.name in ('Ground floor with Garden', 'Villa'):
    #                 type_prop = '1'
    #             if categ.name == 'Villa':
    #                 type_villa = '1'
    #
    #     return {'value': {'type_prop': type_prop, 'type_villa': type_villa}}

    # def on_change_category(self, cr, uid, ids, category, service_type, context=None):
    #     if context is None:
    #         context = {}
    #     lst = []
    #     user = self.pool.get('res.users').browse(cr, uid, context=context)
    #
    #     res = {'value': {'type_lead': '1'}}
    #     catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', '=', category)],
    #                                                       context=context)
    #     propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
    #
    #     categ_type = self.pool.get('mt.case.categ').browse(cr, uid, category, context=context)
    #
    #     if user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id'):
    #         categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')],
    #                                                            context=context)
    #         cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
    #
    #         if res.get('domain'):
    #             res['domain']['category'] = [('id', 'in', propr.ids)]
    #             res['value']['category'] = cats2[0]
    #             res['value']['type_lead'] = '0'

        # if category:
        #
        #     if categ_type.name == 'Residential':
        #         res = {'domain': {'property_type_id': [('id', 'in', propr.ids)]}, 'value': {'type_lead': '0'}}
        #
        #     elif categ_type.name == 'Commercial':
        #         res = {'domain': {'property_type_id': [('id', 'in', propr.ids)]}, 'value': {'type_lead': '1'}}
        # else:
        #     res = {'domain': {'property_type_id': [('id', 'in', lst)]}}


        print res

        return res

# -------------------------------------------------------------------------
# mt_view
# -------------------------------------------------------------------------


class mt_view(osv.osv):
    _name = "mt.project.view"

    _columns = {
        'name': fields.char("View Name", required=True),
    }


# -------------------------------------------------------------------------
# mt_facilities
# -------------------------------------------------------------------------
class mt_facilities(osv.osv):
    _name = "mt.facilities"

    _columns = {
        'name': fields.char("Facilitie Name", required=True),
    }


# -------------------------------------------------------------------------
# mt_kitchen_type
# -------------------------------------------------------------------------
class mt_kitchen_type(osv.osv):
    _name = "mt.kitchen_type"

    _columns = {
        'name': fields.char("Kitchen Type Name", required=True),
    }


# -------------------------------------------------------------------------
# mt_appliances
# -------------------------------------------------------------------------
class mt_appliances(osv.osv):
    _name = "mt.appliances"

    _columns = {
        'name': fields.char("Appliance Name", required=True),
    }


# -------------------------------------------------------------------------
# mt_amenities
# -------------------------------------------------------------------------
class mt_amenities(osv.osv):
    _name = "mt.amenities"

    _columns = {
        'name': fields.char("Amenitie Name", required=True),
    }


# -------------------------------------------------------------------------
# mt_features
# -------------------------------------------------------------------------
class mt_features(osv.osv):
    _name = "mt.features"

    _columns = {
        'name': fields.char("Feature Name", required=True),
    }


# -------------------------------------------------------------------------
# mt_finsishing
# -------------------------------------------------------------------------
class mt_finsishing(osv.osv):
    _name = "mt.finsishing"

    _columns = {
        'name': fields.char("Name", required=True),
    }


# -------------------------------------------------------------------------
# mt_property_sorting
# -------------------------------------------------------------------------
class mt_property_sorting(osv.osv):
    _name = "mt.property.sorting"
    _description = 'Property Sorting'
    _columns = {
        'name': fields.char("Name", required=True),
        'category_id': fields.many2one('mt.case.categ', 'Division'),
    }
