from openerp import api, fields, models


class Property(models.Model):
    _inherit = 'product.template'
    is_agent = fields.Boolean(compute='bool_get_agent')

    @api.one
    def bool_get_agent(self):
        for record in self:
            if self.env.user.has_group('mt_crm_brokerage_manager.group_agent_id') :
                record.is_agent = True
            else:
                record.is_agent = False

    @api.multi
    def property_print(self):
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.env['report'].get_action(self, 'mt_crm_proprety.report_property')


Property()
