# -*- coding: utf-8 -*-
import datetime

import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv
from openerp.tools.translate import _


#-------------------------------------------------------------------------
# mt_project
#-------------------------------------------------------------------------
class mt_project(osv.osv):
    _inherit = "product.template"
    
    
    def on_change_division_id(self, cr, uid, id,division_id):
         result = {}
         name_all = ""
         if division_id :
                categ = self.pool.get('mt.case.categ').browse(cr, uid, division_id)
                mod_obj = self.pool.get('ir.model.data')
                # service_type = buy
                ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_proprety', 'project_division_1')
                if  categ.id == ref[1] :
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_project_residential')
                    if ref2[1]:
                        name_all = ref2[1]
                ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_proprety', 'project_division_2')
                if categ.id == ref[1] :
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_project_commercial')
                    if ref2[1]:
                        name_all = ref2[1]
                 
                ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_proprety', 'project_division_8')
                if categ.id == ref[1] :
                    ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_project_exclusive')
                    if ref2[1]:
                        name_all = ref2[1]
                
#             else :
#                 categ = self.pool.get('mt.case.categ').browse(cr, uid, category)
#                 mod_obj = self.pool.get('ir.model.data')
#                 if service_type == 'buy' :
#                     ref2 = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_project')
#                     if ref2[1]:
#                         name_all = ref2[1]
#               
                 
         result['public_categ_ids'] =    [(6,0,[name_all])]       
         return {'value': result} 
#-------------------------------------------------------------------------------
#         return  {}
    
    _columns = {
        'delivery_date': fields.date("Project Delivery Date"),
        'division_id': fields.many2one('mt.project.division', 'Division'),
        'state_id': fields.many2one('res.country.state', 'Location'),
        'facility_ids': fields.many2many('mt.project.facility', 'mt_project_facility_rel', 'project_id', 'facility_id', 'Facilities'),
        'developper_id': fields.many2one('product.template', string='Developper', domain=[('product_type', '=', 'developer')]),
        'view_project_ids': fields.many2many('mt.project.view', 'mt_project_views_rel', 'project_id', 'view_id', 'Project View'),
        'land_rea': fields.float("Land Area SQM"),
        'building_number': fields.float("Number of Buildings"),
        'proprety_ids': fields.one2many('product.template', 'project_id', 'Properties', readonly=True),
        
        'neighborhood_ids': fields.many2one('mt.neighborhood', string='Neighborhood' ,required=True),
        'district_ids': fields.many2one('mt.city', string='District' ,required=True),
        'payment': fields.selection([('cash', 'Cash'), ('credit', 'Credit')], 'Payment type'),
        'payment_term': fields.char("Payment Terms", size=64),
        'property_type_id': fields.many2one('mt.property.type', 'Property Type', required=True),
        'area_id': fields.many2one('mt.project.areas', 'Areas'),
        'prices_id': fields.many2one('mt.project.prices', 'Prices'),
        'resale':fields.boolean('Resale'),
        'prime':fields.boolean('Prime'),
    }
    
    
    _defaults = {
       
        'delivery_date': datetime.date.today(),
    }

#-------------------------------------------------------------------------
# mt_developper
#-------------------------------------------------------------------------


class mt_developper(osv.osv):
    _inherit = "product.template"
    _columns = {
        'project_ids': fields.one2many('product.template', 'developper_id', 'Projects', readonly=True),
    }
    
    def _default_division_id(self, cr, uid, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        ref = mod_obj.get_object_reference(cr, uid, 'website_property', 'public_category_project_developer')
        if ref[1]:
            return [(6,0,[ref[1]])]       
        return False
    _defaults = {
         
        'public_categ_ids': _default_division_id,
    }
    #---------------------------------------------------------------------------

#-------------------------------------------------------------------------
# project_division
#-------------------------------------------------------------------------


class project_division(osv.osv):
    _name = "mt.project.division"
    _description = 'Project Division'
    _columns = {
        'name': fields.char("Category", required=True),
    }


#-------------------------------------------------------------------------
# project_facility
#-------------------------------------------------------------------------
class project_facility(osv.osv):
    _name = "mt.project.facility"
    _columns = {
        'name': fields.char("Name", required=True),
    }
    
#-------------------------------------------------------------------------
# areas
#-------------------------------------------------------------------------
class project_areas(osv.osv):
    _name = "mt.project.areas"
    _columns = {
        'name': fields.char("Name", required=True),
    }

#-------------------------------------------------------------------------
# prices
#-------------------------------------------------------------------------
class project_prices(osv.osv):
    _name = "mt.project.prices"
    _columns = {
        'name': fields.char("Name", required=True),
    }
    



#-------------------------------------------------------------------------
# project_view
#-------------------------------------------------------------------------
class project_view(osv.osv):
    _name = "mt.project.view"
    _columns = {
        'name': fields.char("Name", required=True),
    }
