# -*- coding: utf-8 -*-

from openerp import models, fields, api

class unique_property_id(models.Model):
    _inherit = "product.template"

    def copy(self, cr, uid, id, default={}, context=None):
        if not default:
            default = {}

        default.update({
            'reference_property': 'PT '+str(id)
        })

        return super(unique_property_id, self).copy(cr, uid, id, default, context=context)
