# -*- coding: utf-8 -*-
{
    'name': "Unique property id",

    'description': """
        Unique property id
    """,

    'author': "Ahmed Tarek - IT Systems",
    'website': "www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'crm',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','mt_crm_proprety'],


}