# -*- coding: utf-8 -*-

from openerp import models, fields, api

class division_advanced_search(models.Model):

    _inherit = 'product.template'

    category = fields.Many2one('mt.case.categ', 'Division', selectable=True)
    division_id = fields.Many2one('mt.project.division','Division ID', selectable=False)


    # def fields_get(self, cr, uid, fields=None, context=None, write_access=True):
    #     res = super(division_advanced_search, self).fields_get(cr, uid, fields, context)
    #     res['category']['selectable'] = True
    #     res['category']['string'] = 'Division'
    #     res['division_id']['selectable'] = False
    #     return res