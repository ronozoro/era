# -*- coding: utf-8 -*-
{
    'name': "Division in advanced search",

    'summary': """
        Fixing (searching by division) in advanced search in (Properties)""",

    'description': """
        Fixing (searching by division) in advanced search in (Properties)
    """,

    'author': "Ahmed Tarek - IT Systems",
    'website': "www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Sales',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','mt_crm_proprety'],


}