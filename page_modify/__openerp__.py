# -*- coding: utf-8 -*-
{
    'name': "Internal Notes",

    'summary': """
        Internal Note Page add in request menus""",

    'description': """
       Internal Note Page add in request  Sales >client Request
    """,

    'author': "ITSYS Omar Salama",
    'website': "http://www.it-syscorp.com",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','crm'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],

}
