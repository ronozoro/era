# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
#
#    Coded by: Borni DHIFI  (dhifi.borni@gmail.com)
#
#-------------------------------------------------------------------------


from datetime import date
from openerp.osv import osv, orm, fields
from openerp import tools, api, addons, SUPERUSER_ID
import base64


#-------------------------------------------------------------------------------
# mt_city
#-------------------------------------------------------------------------------
class mt_city(osv.osv):
    _inherit = "mt.city"
    _columns = {
                'search_home':fields.boolean('Display in home page'), 
                }
mt_city()               

#-------------------------------------------------------------------------
# product_image
#-------------------------------------------------------------------------
class product_image(osv.Model):
    _name = 'product.image'

    _columns = {
        'name': fields.char('Name'),
        'description': fields.text('Description'),
        'image_alt': fields.text('Image Label'),
        'image': fields.binary('Image'),
        'image_small': fields.binary('Small Image'),
        'product_tmpl_id': fields.many2one('product.template', 'Product'),
        'lead_id': fields.many2one('crm.lead', 'Lead'),
        'sequence': fields.integer(string='Sequence'),
        'is_profile_pic': fields.boolean(string='For Profile'),
    }
product_image()


#-------------------------------------------------------------------------
# product_template
#-------------------------------------------------------------------------
class product_template(osv.Model):
    _inherit = 'product.template'

    _columns = {
        'images': fields.one2many('product.image', 'product_tmpl_id',
                                  string='Images'),
    }
product_template()


#-------------------------------------------------------------------------
# mt_project
#-------------------------------------------------------------------------
class mt_project(osv.Model):
    _inherit = "product.template"
    _columns = {
        'featured_residential_project': fields.boolean('Featured Residential Project'),
        'featured_commercial_project': fields.boolean('Featured Commercial Project'),
    }
    _defaults = {
        'featured_residential_project': False,
        'featured_commercial_project': False,
    }

#-------------------------------------------------------------------------
# mt_property
#-------------------------------------------------------------------------


class mt_property(osv.Model):
    _inherit = "product.template"
    _columns = {
        'featured_residential_property': fields.boolean('Featured Residential Property'),
        'featured_commercial_property': fields.boolean('Featured Commercial Property'),
        'featured_residential_investement': fields.boolean('Featured Residential Investement'),
        'featured_commercial_investement': fields.boolean('Featured Commercial Investement'),
        
        
    }
    _defaults = {
        'featured_residential_property': False,
        'featured_commercial_property': False,
    }

#-------------------------------------------------------------------------
# crm_lead
#-------------------------------------------------------------------------
class crm_lead(osv.Model):
    _inherit = 'crm.lead'

    _columns = {
        'images': fields.one2many('product.image', 'lead_id', string='Images'),
    }
    
    def add_image(self, cr, uid, ids, context=None):
        """ Mark the case as lost: state=cancel and probability=0
        """
        prop_obj = self.pool.get('product.template')
        image_obj = self.pool.get('product.image')
        obj = self.browse(cr, uid, ids[0], context=context)
        img2 = obj.images[0]
        img3 = image_obj.browse(cr, uid, img2.id, context=context).image
        prop_obj.write(cr,uid,obj.property.id,{'image_medium':img3})
        imgs=[]
        for img in obj.images :
            vals={
                   'name': img.name,
                    'description': img.description,
                    'image_alt': img.image_alt,
                    'image': img.image,
                    'image_small':img.image_small,
                    'product_tmpl_id': obj.property.id,
                  
                  }
            image_obj.create(cr,uid,vals,context)
            
            
    
crm_lead()
#-------------------------------------------------------------------------
# website
#-------------------------------------------------------------------------
class website_sale(orm.Model):
    _inherit = 'website'

    def get_featured_residential_project(self, cr, uid, domain=[], context=None):
        domain=domain+[('featured_residential_project', '=', 'True'),('website_published', '=', 'True')]
        prod_ids = self.pool.get('product.template').search(
            cr, uid,domain , context=context)
        price_list = self.price_list_get(cr, uid, context)
        product_data = self.pool.get('product.template').browse(
            cr, uid, prod_ids, {'pricelist': int(price_list)})
        return product_data

    def get_featured_commercial_project(self, cr, uid, domain, context=None):
        domain=domain+[('featured_commercial_project', '=', 'True'),('website_published', '=', 'True')]
        prod_ids = self.pool.get('product.template').search(
            cr, uid, domain, context=context)
        price_list = self.price_list_get(cr, uid, context)
        product_data = self.pool.get('product.template').browse(
            cr, uid, prod_ids, {'pricelist': int(price_list)})
        return product_data

    def get_featured_project(self, cr, uid, ids, context=None):
        prod_ids = self.pool.get('product.template').search(
            cr, uid, ['|',('featured_residential_project', '=', 'True'), ('featured_commercial_project', '=', 'True'),('website_published', '=', 'True')], context=context)
        price_list = self.price_list_get(cr, uid, context)
        product_data = self.pool.get('product.template').browse(
            cr, uid, prod_ids, {'pricelist': int(price_list)})
        return product_data 
        
    
    def get_featured_residential_property(self, cr, uid, domain, context=None):
        domain=domain+[('featured_residential_property', '=', 'True'),('website_published', '=', 'True')]
        prod_ids = self.pool.get('product.template').search(
            cr, uid,domain, context=context)
        price_list = self.price_list_get(cr, uid, context)
        product_data = self.pool.get('product.template').browse(
            cr, uid, prod_ids, {'pricelist': int(price_list)})
        return product_data

    def get_featured_commercial_property(self, cr, uid, domain, context=None):
        domain=domain+[('featured_commercial_property', '=', 'True'),('website_published', '=', 'True')]
        prod_ids = self.pool.get('product.template').search(
            cr, uid, domain, context=context)
        price_list = self.price_list_get(cr, uid, context)
        product_data = self.pool.get('product.template').browse(
            cr, uid, prod_ids, {'pricelist': int(price_list)})
        return product_data

    def get_featured_residential_investement(self, cr, uid, domain, context=None):
        domain=domain+[('featured_residential_investement', '=', 'True'),('website_published', '=', 'True')]
        prod_ids = self.pool.get('product.template').search(
            cr, uid,domain, context=context)
        price_list = self.price_list_get(cr, uid, context)
        product_data = self.pool.get('product.template').browse(
            cr, uid, prod_ids, {'pricelist': int(price_list)})
        return product_data

    def get_featured_commercial_investement(self, cr, uid, domain, context=None):
        domain=domain+[('featured_commercial_investement', '=', 'True'),('website_published', '=', 'True')]
        prod_ids = self.pool.get('product.template').search(
            cr, uid, domain, context=context)
        price_list = self.price_list_get(cr, uid, context)
        product_data = self.pool.get('product.template').browse(
            cr, uid, prod_ids, {'pricelist': int(price_list)})
        return product_data
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
