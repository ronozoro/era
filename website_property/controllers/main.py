# -*- coding: utf-8 -*-
import re
import werkzeug

from openerp import SUPERUSER_ID
from openerp import http
import openerp
from openerp.addons import website_sale
from openerp.addons.auth_signup.controllers.main import AuthSignupHome
from openerp.addons.website.models.website import slug
from openerp.http import request
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import base64
import datetime 
from dateutil.relativedelta import relativedelta

class AuthSignupHomeExtension(AuthSignupHome):

    def do_signup(self, qcontext):
        """ Shared helper that creates a res.partner out of a token """
        values = dict((key, qcontext.get(key))
                      for key in ('login', 'name', 'password', 'mobile'))
        assert any(
            [k for k in values.values()]), "The form was not properly filled in."
        assert values.get('password') == qcontext.get(
            'confirm_password'), _("Passwords do not match; please retype them.")
        values['lang'] = request.lang
        self._signup_with_values(qcontext.get('token'), values)
        request.cr.commit()

class QueryURL(object):
    def __init__(self, path='', **args):
        self.path = path
        self.args = args

    def __call__(self, path=None, **kw):
        if not path:
            path = self.path
        for k,v in self.args.items():
            kw.setdefault(k,v)
        l = []
        for k,v in kw.items():
            if v:
                if isinstance(v, list) or isinstance(v, set):
                    l.append(werkzeug.url_encode([(k,i) for i in v]))
                else:
                    l.append(werkzeug.url_encode([(k,v)]))
        if l:
            path += '?' + '&'.join(l)
        return path

class WebsiteSaleControllerDerived(website_sale.controllers.main.website_sale):
    
    @http.route(['/residential/property/<model("product.template"):product>'], type='http', auth="public", website=True)
    def product(self, product, category='', search='', **kwargs):
        cr, uid, context, pool = request.cr, SUPERUSER_ID, request.context, request.registry
        category_obj = pool['product.public.category']
        template_obj = pool['product.template']

        context.update(active_id=product.id)

        if category:
            category = category_obj.browse(cr, uid, int(category), context=context)

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int,v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])

        keep = QueryURL('/residential', category=category and category.id, search=search, attrib=attrib_list)

        category_ids = category_obj.search(cr, uid, [], context=context)
        category_list = category_obj.name_get(cr, uid, category_ids, context=context)
        category_list = sorted(category_list, key=lambda category: category[1])

        pricelist = self.get_pricelist()

        from_currency = pool.get('product.price.type')._get_field_currency(cr, uid, 'list_price', context)
        to_currency = pricelist.currency_id
        compute_currency = lambda price: pool['res.currency']._compute(cr, uid, from_currency, to_currency, price, context=context)

        if not context.get('pricelist'):
            context['pricelist'] = int(self.get_pricelist())
            product = template_obj.browse(cr, uid, int(product), context=context)

        values = {
            'search': search,
            'category': category,
            'pricelist': pricelist,
            'attrib_values': attrib_values,
            'compute_currency': compute_currency,
            'attrib_set': attrib_set,
            'keep': keep,
            'category_list': category_list,
            'main_object': product,
            'product': product,
            'get_attribute_value_ids': self.get_attribute_value_ids
        }
        return request.website.render("website_sale.product", values)


    @http.route(['/residential',
        '/residential/page/<int:page>',
        '/residential/buy/<model("product.public.category"):category>',
        '/residential/buy/<model("product.public.category"):category>/page/<int:page>'
    ], type='http', auth="public", website=True)
    def shop(self, page=0, category=None, search='', **post):
        cr, uid, context, pool = request.cr, SUPERUSER_ID, request.context, request.registry

        domain = request.website.sale_product_domain()
        if search:
            for srch in search.split(" "):
                domain += ['|', '|', '|', ('name', 'ilike', srch), ('description', 'ilike', srch),
                    ('description_sale', 'ilike', srch), ('product_variant_ids.default_code', 'ilike', srch)]
        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int,v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])

        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domain += [('attribute_line_ids.value_ids', 'in', ids)]
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domain += [('attribute_line_ids.value_ids', 'in', ids)]

        keep = website_sale.controllers.main.QueryURL('/residential', category=category and int(category), search=search, attrib=attrib_list)

        if not context.get('pricelist'):
            pricelist = self.get_pricelist()
            context['pricelist'] = int(pricelist)
        else:
            pricelist = pool.get('product.pricelist').browse(cr, uid, context['pricelist'], context)

        product_obj = pool.get('product.template')

        url = "/residential"
        domain += [('website_published', '=', 'True')]
        city_retain_id = 0
        if post.get('city_id', False) and int(post['city_id']) !=0 :
            domain += [('city_id.id', '=', int(post['city_id']))]
            city_retain_id = int(post['city_id'])
        
        property_type_retain_id = 0
        if post.get('property_type_id', False) and int(post['property_type_id']) !=0 :
            domain += [('property_type_id.id', '=', int(post['property_type_id']))]
            property_type_retain_id = int(post['property_type_id'])
                    
        
        
        product_count = product_obj.search_count(cr, uid, domain, context=context)
        if search:
            post["search"] = search
        if category:
            category = pool['product.public.category'].browse(cr, uid, int(category), context=context)
            url = "/residential/buy/%s" % slug(category)
        pager = request.website.pager(url=url, total=product_count, page=page, step=website_sale.controllers.main.PPG, scope=7, url_args=post)
        product_ids = product_obj.search(cr, uid, domain, limit=website_sale.controllers.main.PPG, offset=pager['offset'], order='website_published desc, website_sequence desc', context=context)
        products = product_obj.browse(cr, uid, product_ids, context=context)

        style_obj = pool['product.style']
        style_ids = style_obj.search(cr, uid, [], context=context)
        styles = style_obj.browse(cr, uid, style_ids, context=context)

        category_obj = pool['product.public.category']
        category_ids = category_obj.search(cr, uid, [('parent_id', '=', False)], context=context)
        categs = category_obj.browse(cr, uid, category_ids, context=context)

        attributes_obj = request.registry['product.attribute']
        attributes_ids = attributes_obj.search(cr, uid, [], context=context)
        attributes = attributes_obj.browse(cr, uid, attributes_ids, context=context)

        from_currency = pool.get('product.price.type')._get_field_currency(cr, uid, 'list_price', context)
        to_currency = pricelist.currency_id
        compute_currency = lambda price: pool['res.currency']._compute(cr, uid, from_currency, to_currency, price, context=context)
        
        city_obj = pool['mt.city']
        city_ids = city_obj.search(cr, SUPERUSER_ID, [], context=context)
        cities = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)

        property_type_obj= pool.get('mt.property.type')
        property_type_ids = property_type_obj.search( cr, SUPERUSER_ID, [], context=context)
        property_types = property_type_obj.browse( cr, SUPERUSER_ID, property_type_ids, context)
         
        
        values = {
            'search': search,
            'category': category,
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'pager': pager,
            'pricelist': pricelist,
            'products': products,
            'bins': website_sale.controllers.main.table_compute().process(products),
            'rows': website_sale.controllers.main.PPR,
            'styles': styles,
            'categories': categs,
            'attributes': attributes,
            'compute_currency': compute_currency,
            'keep': keep,
            'style_in_product': lambda style, product: style.id in [s.id for s in product.website_style_ids],
            'attrib_encode': lambda attribs: werkzeug.url_encode([('attrib',i) for i in attribs]),
            'cities': cities,
            'city_retain_id': city_retain_id,
            'property_types': property_types,
            'property_type_retain_id': property_type_retain_id,
        }
        return request.website.render("website_sale.products", values)
    

class property_theme(http.Controller):


    @http.route(['/','/page/homepage'], type='http', auth="public", website=True)
    def home_page(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        city_obj = pool['mt.city']
        
        
        city_ids = city_obj.search(cr, SUPERUSER_ID, [('search_home','=',True)], context=context)   
        
        domain_res_project=[]
        city_res_project_retain_id = 0
        if post.get('city_res_project_id', False) and int(post['city_res_project_id']) !=0 :
            domain_res_project= [('district_ids.id', '=', int(post['city_res_project_id']))]
            city_res_project_retain_id = int(post['city_res_project_id'])
        res_projects=pool['website'].get_featured_residential_project(cr,uid,domain_res_project,context)   
        cities_res_project = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)

        domain_comm_project=[]
        city_comm_project_retain_id = 0
        if post.get('city_comm_project_id', False) and int(post['city_comm_project_id']) !=0 :
            domain_comm_project= [('district_ids.id', '=', int(post['city_comm_project_id']))]
            city_comm_project_retain_id = int(post['city_comm_project_id'])
        comm_projects=pool['website'].get_featured_commercial_project(cr,uid,domain_comm_project,context)   
        cities_comm_project = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)
                
        domain_res_property=[]
        city_res_property_retain_id = 0
        if post.get('city_res_property_id', False) and int(post['city_res_property_id']) !=0 :
            domain_res_property= [('city_id.id', '=', int(post['city_res_property_id']))]
            city_res_property_retain_id = int(post['city_res_property_id'])
        res_propertys=pool['website'].get_featured_residential_property(cr,uid,domain_res_property,context)   
        cities_res_property = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)

        domain_comm_property=[]
        city_comm_property_retain_id = 0
        if post.get('city_comm_property_id', False) and int(post['city_comm_property_id']) !=0 :
            domain_comm_property= [('city_id.id', '=', int(post['city_comm_property_id']))]
            city_comm_property_retain_id = int(post['city_comm_property_id'])
        comm_propertys=pool['website'].get_featured_commercial_property(cr,uid,domain_comm_property,context)   
        cities_comm_property = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)
                        

        domain_res_inv_property=[]
        city_res_inv_property_retain_id = 0
        if post.get('city_res_inv_property_id', False) and int(post['city_res_inv_property_id']) !=0 :
            domain_res_inv_property= [('city_id.id', '=', int(post['city_res_inv_property_id']))]
            city_res_inv_property_retain_id = int(post['city_res_inv_property_id'])
        res_inv_propertys=pool['website'].get_featured_residential_investement(cr,uid,domain_res_inv_property,context)   
        cities_res_inv_property = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)

        domain_comm_inv_property=[]
        city_comm_inv_property_retain_id = 0
        if post.get('city_comm_inv_property_id', False) and int(post['city_comm_inv_property_id']) !=0 :
            domain_comm_inv_property= [('city_id.id', '=', int(post['city_comm_inv_property_id']))]
            city_comm_inv_property_retain_id = int(post['city_comm_inv_property_id'])
        comm_inv_propertys=pool['website'].get_featured_commercial_investement(cr,uid,domain_comm_inv_property,context)   
        cities_comm_inv_property = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)
                
        values = {
            'cities_res_project':cities_res_project,
            'city_res_project_retain_id':city_res_project_retain_id,
            'res_projects':res_projects,
            
            'cities_comm_project':cities_comm_project,
            'city_comm_project_retain_id':city_comm_project_retain_id,
            'comm_projects':comm_projects,

            'cities_res_property':cities_res_property,
            'city_res_property_retain_id':city_res_property_retain_id,
            'res_propertys':res_propertys,

            'cities_comm_property':cities_comm_property,
            'city_comm_property_retain_id':city_comm_property_retain_id,
            'comm_propertys':comm_propertys,


            'cities_res_inv_property':cities_res_inv_property,
            'city_res_inv_property_retain_id':city_res_inv_property_retain_id,
            'res_inv_propertys':res_inv_propertys,

            'cities_comm_inv_property':cities_comm_inv_property,
            'city_comm_inv_property_retain_id':city_comm_inv_property_retain_id,
            'comm_inv_propertys':comm_inv_propertys,
            
                                    
            }
 
        return request.website.render("website.homepage", values)
    
    @http.route(['/page/website_property.featured_residential_project'
                 ], type='http', auth="public", website=True)
    def featured_residential_project(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        city_obj = pool['mt.city']
        
        
        city_ids = city_obj.search(cr, SUPERUSER_ID, [('search_home','=',True)], context=context)   
        
        domain_res_project=[]
        city_res_project_retain_id = 0
        if post.get('city_res_project_id', False) and int(post['city_res_project_id']) !=0 :
            domain_res_project= [('district_ids.id', '=', int(post['city_res_project_id']))]
            city_res_project_retain_id = int(post['city_res_project_id'])
        res_projects=pool['website'].get_featured_residential_project(cr,uid,domain_res_project,context)   
        cities_res_project = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)

        
                
        values = {
            'cities_res_project':cities_res_project,
            'city_res_project_retain_id':city_res_project_retain_id,
            'res_projects':res_projects,
                                    
            }
 
        return request.website.render("website_property.featured_residential_project", values)
    
    @http.route(['/page/website_property.featured_commercial_project'
                 ], type='http', auth="public", website=True)
    def featured_commercial_project(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        city_obj = pool['mt.city']
        
        
        city_ids = city_obj.search(cr, SUPERUSER_ID, [('search_home','=',True)], context=context)   
        
        domain_comm_project=[]
        city_comm_project_retain_id = 0
        if post.get('city_comm_project_id', False) and int(post['city_comm_project_id']) !=0 :
            domain_comm_project= [('district_ids.id', '=', int(post['city_comm_project_id']))]
            city_comm_project_retain_id = int(post['city_comm_project_id'])
        comm_projects=pool['website'].get_featured_commercial_project(cr,uid,domain_comm_project,context)   
        cities_comm_project = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)

        
                
        values = {
            'cities_comm_project':cities_comm_project,
            'city_comm_project_retain_id':city_comm_project_retain_id,
            'comm_projects':comm_projects,
            
           
            
                                    
            }
 
        return request.website.render("website_property.featured_commercial_project", values)
    
    
    @http.route(['/page/website_property.featured_residential_property'
                 ], type='http', auth="public", website=True)
    def featured_residential_property(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        city_obj = pool['mt.city']
        
        
        city_ids = city_obj.search(cr, SUPERUSER_ID, [('search_home','=',True)], context=context)   
        
        domain_res_property=[]
        city_res_property_retain_id = 0
        if post.get('city_res_property_id', False) and int(post['city_res_property_id']) !=0 :
            domain_res_property= [('city_id.id', '=', int(post['city_res_property_id']))]
            city_res_property_retain_id = int(post['city_res_property_id'])
        res_propertys=pool['website'].get_featured_residential_property(cr,uid,domain_res_property,context)   
        cities_res_property = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)

        
                
        values = {
            'cities_res_property':cities_res_property,
            'city_res_property_retain_id':city_res_property_retain_id,
            'res_propertys':res_propertys,
                                    
            }
 
        return request.website.render("website_property.featured_residential_property", values)
    
    @http.route(['/page/website_property.featured_commercial_property'
                 ], type='http', auth="public", website=True)
    def featured_commercial_property(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        city_obj = pool['mt.city']
        
        
        city_ids = city_obj.search(cr, SUPERUSER_ID, [('search_home','=',True)], context=context)   
        
        domain_comm_property=[]
        city_comm_property_retain_id = 0
        if post.get('city_comm_property_id', False) and int(post['city_comm_property_id']) !=0 :
            domain_comm_property= [('city_id.id', '=', int(post['city_comm_property_id']))]
            city_comm_property_retain_id = int(post['city_comm_property_id'])
        comm_propertys=pool['website'].get_featured_commercial_property(cr,uid,domain_comm_property,context)   
        cities_comm_property = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)

        
                
        values = {
            'cities_comm_property':cities_comm_property,
            'city_comm_property_retain_id':city_comm_property_retain_id,
            'comm_propertys':comm_propertys,
                                    
            }
 
        return request.website.render("website_property.featured_commercial_property", values)
    
    
    @http.route(['/page/website_property.featured_residential_investment_property'
                 ], type='http', auth="public", website=True)
    def featured_residential_investment_property(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        city_obj = pool['mt.city']
        
        
        city_ids = city_obj.search(cr, SUPERUSER_ID, [('search_home','=',True)], context=context)   
        
        domain_res_inv_property=[]
        city_res_inv_property_retain_id = 0
        if post.get('city_res_inv_property_id', False) and int(post['city_res_inv_property_id']) !=0 :
            domain_res_inv_property= [('city_id.id', '=', int(post['city_res_inv_property_id']))]
            city_res_inv_property_retain_id = int(post['city_res_inv_property_id'])
        res_inv_propertys=pool['website'].get_featured_residential_investement(cr,uid,domain_res_inv_property,context)   
        cities_res_inv_property = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)

        
                
        values = {
            'cities_res_inv_property':cities_res_inv_property,
            'city_res_inv_property_retain_id':city_res_inv_property_retain_id,
            'res_inv_propertys':res_inv_propertys,
                                    
            }
 
        return request.website.render("website_property.featured_residential_investment_property", values)
    
    @http.route(['/page/website_property.featured_commercial_investment_property'
                 ], type='http', auth="public", website=True)
    def featured_commercial_investment_property(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        city_obj = pool['mt.city']
        
        
        city_ids = city_obj.search(cr, SUPERUSER_ID, [('search_home','=',True)], context=context)   
        
        domain_comm_inv_property=[]
        city_comm_inv_property_retain_id = 0
        if post.get('city_comm_inv_property_id', False) and int(post['city_comm_inv_property_id']) !=0 :
            domain_comm_inv_property= [('city_id.id', '=', int(post['city_comm_inv_property_id']))]
            city_comm_inv_property_retain_id = int(post['city_comm_inv_property_id'])
        comm_inv_propertys=pool['website'].get_featured_commercial_investement(cr,uid,domain_comm_inv_property,context)   
        cities_comm_inv_property = city_obj.browse(cr, SUPERUSER_ID, city_ids, context=context)

        
                
        values = {
            'cities_comm_inv_property':cities_comm_inv_property,
            'city_comm_inv_property_retain_id':city_comm_inv_property_retain_id,
            'comm_inv_propertys':comm_inv_propertys,
                                    
            }
 
        return request.website.render("website_property.featured_commercial_investment_property", values)
    
    
    @http.route(['/page/website_property.add_property'
                 ], type='http', auth="public", website=True)
    def page_add_property(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        values = {}
        orm_property_type = pool.get('mt.property.type')
        orm_finishing_type = pool.get('mt.finsishing')
        orm_category = pool.get('mt.case.categ')
        orm_district = pool.get('mt.city')
        orm_city = pool.get('res.country.state')
        orm_type_area = pool.get('product.uom')
        orm_currency = pool.get('res.currency')


        property_type_ids = orm_property_type.search( cr, SUPERUSER_ID, [], context=context)
        property_types = orm_property_type.browse( cr, SUPERUSER_ID, property_type_ids, context)
        values.update({'property_types': property_types})
        
        finishing_type_ids = orm_finishing_type.search( cr, SUPERUSER_ID, [], context=context)
        finishing_types = orm_finishing_type.browse( cr, SUPERUSER_ID, finishing_type_ids, context)
        values.update({'finishing_types': finishing_types})
        
        category_ids = orm_category.search( cr, SUPERUSER_ID, [], context=context)
        categories = orm_category.browse( cr, SUPERUSER_ID, category_ids, context)       
        values.update({'categories': categories})
        
        #district
        district_ids = orm_district.search(cr, SUPERUSER_ID, [], context=context)
        districts = orm_district.browse(cr, SUPERUSER_ID, district_ids, context)
        values.update({'districts': districts})
        #citys
        city_id = orm_city.search(cr, SUPERUSER_ID, [], context=context)
        citys = orm_city.browse(cr, SUPERUSER_ID, city_id, context)
        values.update({'citys': citys})
        #type_area
        type_area_id = orm_type_area.search(cr, SUPERUSER_ID, [], context=context)
        type_areas = orm_type_area.browse(cr, SUPERUSER_ID, type_area_id, context)
        values.update({'type_areas': type_areas})
        #currency
        currency_ids = orm_currency.search( cr, SUPERUSER_ID, [], context=context)
        currencys = orm_currency.browse( cr, SUPERUSER_ID, currency_ids, context)       
        values.update({'currencys': currencys})
 
        return request.website.render("website_property.add_property", values)

    @http.route(['/property/add'
                 ], type='http', auth="user", website=True)
    def add_property(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        partner = pool.get('res.users').browse(
            cr, SUPERUSER_ID, request.uid, context).partner_id

        name = post.pop('name', '')
        phone = post.pop('phone', '')
        email = post.pop('email', '')
        property_type_id = post.pop('property_type_id', False)
        location = post.pop('unit_location', '')
        # size = post.pop('size', '')
        price = post.pop('price', '')
        finishing_id = post.pop('finishing_id', False)
        description = post.pop('description', '')
        service_type_id = post.pop('service_type_id', '')
        category_id = post.pop('category_id', False)
        payment_method = post.pop('payment_method', False)
        district_ids = post.pop('district_ids',False)
        city_id = post.pop('city_id',False)
        area = post.pop('area')
        type_area_id = post.pop('type_area_id',False)
        currency_id = post.pop('currency_id', False)
        
        
        
        lead_val = {
            'contact_name': name,
            'street' : location,
            'name': name,
            'phone': phone,
            'area_sqm': area,
            'product_unit_id8':type_area_id,
            'email_from': email ,
            'property_ids': int(property_type_id),
            'finsishing_id':finishing_id,
            'asking_price': price,
            'type': 'opportunity', 
            'description': description,
            'service_ids': service_type_id,
            'payment' : payment_method,
            'district_ids': district_ids,
            'city_id' : city_id,
            'category' : category_id,
            'medium_id2': request.registry['ir.model.data'].xmlid_to_res_id(request.cr, SUPERUSER_ID, 'crm.crm_medium_website'),
            'section_id': request.registry['ir.model.data'].xmlid_to_res_id(request.cr, SUPERUSER_ID, 'website.salesteam_website_sales'),
            'active': True,
            'currency_id':currency_id,
        }
        res = pool.get('crm.lead').onchange_client_phone(cr, uid, [], int(phone), context=None)
        if res and len(res)>0 : 
                lead_val.update(res.get('value',{}))
        crm_id = request.registry['crm.lead'].create(
            cr, SUPERUSER_ID, lead_val, context=dict(context, mail_create_nosubscribe=True))
        
        if post['image1']:
            image1= base64.encodestring(post['image1'].read())
            request.registry['product.image'].create(cr, SUPERUSER_ID, {'image':image1, 'lead_id':crm_id,'name':name}, context=dict(context, mail_create_nosubscribe=True))
        if post['image2']:
            image2= base64.encodestring(post['image2'].read())
            request.registry['product.image'].create(cr, SUPERUSER_ID, {'image':image2, 'lead_id':crm_id,'name':name}, context=dict(context, mail_create_nosubscribe=True))
        if post['image3']:
            image3= base64.encodestring(post['image3'].read())
            request.registry['product.image'].create(cr, SUPERUSER_ID, {'image':image3, 'lead_id':crm_id,'name':name}, context=dict(context, mail_create_nosubscribe=True))
        #create notif
        user_ids=[]
        alarm_ids=[]
        event_obj = pool.get('calendar.event')
        models_data = pool.get('ir.model.data')
        alarm_notif_id = models_data.get_object_reference(cr,uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
        manager = pool.get('res.users').search(cr, uid, [('login', 'ilike', 'karim.fahim@era-egypt.com')], context=context)
        manager = pool.get('res.users').browse(cr, uid, manager[0], context=context)
        user_ids.append(manager.partner_id.id)
        engy_manager_ids = pool.get('res.users').search(cr, uid, [('login', 'ilike', 'engy.ashraf@era-egypt.com')], context=context)
        engy_manager = pool.get('res.users').browse(cr, uid, engy_manager_ids[0], context=context)
        user_ids.append(engy_manager.partner_id.id)
        alarm_ids.append(alarm_notif_id)
        date_alert = datetime.datetime.now()+relativedelta(minutes=+ 1)
        date_fin = datetime.datetime.now()+relativedelta(minutes=+ 20)
        event_values = {
                                 'name' :"New Request Has been Received from Website",
                                 'start_datetime':date_alert.strftime('%Y-%m-%d %H:%M:%S') ,
                                 'stop_datetime':date_fin.strftime('%Y-%m-%d %H:%M:%S') ,
                                 'alarm_ids':[(6,0, alarm_ids)],
                                 'partner_ids':[(6,0,user_ids)],
                                 'is_notif':True,
                                }
        event_id = event_obj.create(cr, uid,event_values, context=context )
        return request.website.render("website_property.thanksForSell", {})

    @http.route(['/property/inquire'
                 ], type='http', auth="public", website=True)
    def inquire_property(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        partner = pool.get('res.users').browse(
            cr, SUPERUSER_ID, request.uid, context).partner_id
        

        name = post.pop('name', '')
        phone = post.pop('phone', '')
        email = post.pop('email', '')
        description = post.pop('description', '')
        product_id = post.pop('product_id', False)
        prod = pool.get('product.template').browse(
            cr, SUPERUSER_ID, int(product_id), context)
       
        
       
        lead_val = {
            'contact_name': name,
            'name': name,
            'phone': phone,
            'email_from': email,
            'property': product_id,
            'reference_property':prod.reference_property,
            'property_ids': 2,
            'description': description,
            'type':'lead',
            'category':prod.category.id,
            'service_ids':prod.service_type,
            'medium_id2': request.registry['ir.model.data'].xmlid_to_res_id(request.cr, SUPERUSER_ID, 'crm.crm_medium_website'),
            'section_id': request.registry['ir.model.data'].xmlid_to_res_id(request.cr, SUPERUSER_ID, 'mt_crm_brokerage_manager.crm_case_section1'),
            'active': True,
        }
        res = pool.get('crm.lead').on_change_property(cr, uid, [], int(product_id), context=None)
        if res and len(res)>0 : 
            lead_val.update(res.get('value',{}))
        request.registry['crm.lead'].create(
            cr, SUPERUSER_ID, lead_val, context=dict(context, mail_create_nosubscribe=True))
        #create notif
        user_ids=[]
        alarm_ids=[]
        event_obj = pool.get('calendar.event')
        models_data = pool.get('ir.model.data')
        alarm_notif_id = models_data.get_object_reference(cr,uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
        manager = pool.get('res.users').search(cr, uid, [('login', 'ilike', 'karim.fahim@era-egypt.com')], context=context)
        manager = pool.get('res.users').browse(cr, uid, manager[0], context=context)
        user_ids.append(manager.partner_id.id)
        engy_manager_ids = pool.get('res.users').search(cr, uid, [('login', 'ilike', 'engy.ashraf@era-egypt.com')], context=context)
        engy_manager = pool.get('res.users').browse(cr, uid, engy_manager_ids[0], context=context)
        user_ids.append(engy_manager.partner_id.id)
        alarm_ids.append(alarm_notif_id)
        date_alert = datetime.datetime.now()+relativedelta(minutes=+ 1)
        date_fin = datetime.datetime.now()+relativedelta(minutes=+ 20)
        event_values = {
                                 'name' :"New lead Has been Received from website",
                                 'start_datetime':date_alert.strftime('%Y-%m-%d %H:%M:%S') ,
                                 'stop_datetime':date_fin.strftime('%Y-%m-%d %H:%M:%S') ,
                                 'alarm_ids':[(6,0, alarm_ids)],
                                 'partner_ids':[(6,0,user_ids)],
                                 'is_notif':True,
                                }
        event_id = event_obj.create(cr, uid,event_values, context=context )
        return request.website.render("website_property.inquiry",  {'successmsg':_('Your Inquiry has been sent successfully.')})


    @http.route(['/customer/care'
                 ], type='http', auth="public", website=True)
    def create_crm_claim(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID

        name = post.pop('name', '')
        name_claim = post.pop('name_claim', '')
        phone = post.pop('phone', '')
        email = post.pop('email', '')
        description = post.pop('description', '')
        categ= post.pop('categ', '')
        search_ids=pool.get('crm.case.categ').search(cr,uid,[('name','=',categ)])
        search_partner_ids=pool.get('res.partner').search(cr,uid,[('phone','=',phone)])
        categ_id =  search_ids and  search_ids[0] or False
        partner_id = search_partner_ids and search_partner_ids[0] or False
            
        claim_val = {
            'name': name,
            'name_claim':name_claim,
            'partner_phone': phone,
            'email_from': email,
            'description': description,
            'categ_id': categ_id,
            'partner_id': partner_id,
        }
        request.registry['crm.claim'].create(
            cr, SUPERUSER_ID, claim_val, context=context)
        #create notif
        user_ids=[]
        alarm_ids=[]
        event_obj = pool.get('calendar.event')
        models_data = pool.get('ir.model.data')
        alarm_notif_id = models_data.get_object_reference(cr,uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
        manager = pool.get('res.users').search(cr, uid, [('login', 'ilike', 'karim.fahim@era-egypt.com')], context=context)
        manager = pool.get('res.users').browse(cr, uid, manager[0], context=context)
        user_ids.append(manager.partner_id.id)
        engy_manager_ids = pool.get('res.users').search(cr, uid, [('login', 'ilike', 'engy.ashraf@era-egypt.com')], context=context)
        engy_manager = pool.get('res.users').browse(cr, uid, engy_manager_ids[0], context=context)
        user_ids.append(engy_manager.partner_id.id)
        alarm_ids.append(alarm_notif_id)
        date_alert = datetime.datetime.now()+relativedelta(minutes=+ 1)
        date_fin = datetime.datetime.now()+relativedelta(minutes=+ 20)
        event_values = {
                                 'name' :"New Claim Has been Submitted from Website",
                                 'start_datetime':date_alert.strftime('%Y-%m-%d %H:%M:%S') ,
                                 'stop_datetime':date_fin.strftime('%Y-%m-%d %H:%M:%S') ,
                                 'alarm_ids':[(6,0, alarm_ids)],
                                 'partner_ids':[(6,0,user_ids)],
                                 'is_notif':True,
                                }
        event_id = event_obj.create(cr, uid,event_values, context=context )
        return request.website.render("website_property.customer_care",  {'successmsg':_('Your message has been sent successfully.')})
    
    
    @http.route(['/page/website_property.group_buynig'
                 ], type='http', auth="public", website=True)
    def page_group_buynig(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        values = {}
        orm_state = pool.get('res.country.state')
        orm_product = pool.get('product.template')
        orm_district = pool.get('mt.city')
        orm_city = pool.get('mt.city')
        orm_property_type = pool.get('mt.property.type')
        orm_finishing_type = pool.get('mt.finsishing')
        orm_category = pool.get('mt.case.categ')
        orm_currency = pool.get('res.currency')
        orm_type_area = pool.get('product.uom')


        state_ids = orm_state.search( cr, SUPERUSER_ID, [], context=context)
        states = orm_state.browse( cr, SUPERUSER_ID, state_ids, context)
        values.update({'states': states})
        #TODO:All projects ?
        projects_ids = orm_product.search(cr, SUPERUSER_ID, [('product_type','=','project')], context=context)
        projects = orm_product.browse(cr, SUPERUSER_ID, projects_ids, context)
        values.update({'projects': projects})
        #district
        district_ids = orm_district.search(cr, SUPERUSER_ID, [], context=context)
        districts = orm_district.browse(cr, SUPERUSER_ID, district_ids, context)
        values.update({'districts': districts})
        #citys
        city_id = orm_city.search(cr, SUPERUSER_ID, [], context=context)
        citys = orm_city.browse(cr, SUPERUSER_ID, city_id, context)
        values.update({'citys': citys})
        #property
        property_type_ids = orm_property_type.search( cr, SUPERUSER_ID, [], context=context)
        property_types = orm_property_type.browse( cr, SUPERUSER_ID, property_type_ids, context)
        values.update({'property_types': property_types})
        #finishing
        finishing_type_ids = orm_finishing_type.search( cr, SUPERUSER_ID, [], context=context)
        finishing_types = orm_finishing_type.browse( cr, SUPERUSER_ID, finishing_type_ids, context)
        values.update({'finishing_types': finishing_types})
        #category
        category_ids = orm_category.search( cr, SUPERUSER_ID, [], context=context)
        categories = orm_category.browse( cr, SUPERUSER_ID, category_ids, context)       
        values.update({'categories': categories})
        #currency
        currency_ids = orm_currency.search( cr, SUPERUSER_ID, [], context=context)
        currencys = orm_currency.browse( cr, SUPERUSER_ID, currency_ids, context)       
        values.update({'currencys': currencys})
        #type_area
        type_area_id = orm_type_area.search(cr, SUPERUSER_ID, [('category_id.name', '=','Unit')], context=context)
        type_areas = orm_type_area.browse(cr, SUPERUSER_ID, type_area_id, context)
        values.update({'type_areas': type_areas})
        return request.website.render("website_property.group_buynig", values)
    
    @http.route(['/group_buynig/add'
                 ], type='http', auth="public", website=True)
    def add_group_buynig(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        partner = pool.get('res.users').browse(
            cr, SUPERUSER_ID, request.uid, context).partner_id

        name = post.pop('name')
        phone = post.pop('phone')
        email = post.pop('email')
        area = post.pop('area')
        budget = post.pop('price_min')
        budget2 = post.pop('price_max')
        state_id = post.pop('state_id', False)
        project_id = post.pop('project_id', False)
        district_ids = post.pop('district_ids',False)
        city_id = post.pop('city_id',False)
        
        property_type_id = post.pop('property_type_id', False)
        finishing_id = post.pop('finishing_id', False)
        category_id = post.pop('category_id', False)
        currency_id = post.pop('currency_id', False)
        type_area_id = post.pop('type_area_id',False)
       
        lead_val = {
            'contact_name': name,
            'name': name,
            'phone': phone,
            'email_from': email,
            'area_sqm': area,
            'product_unit_id8':type_area_id,
            'state_id': state_id,
            'project_id': project_id,
            'budget': budget,
            'budget2': budget2,
            'property_ids': 2,#TODO:not static
            'service_ids': 'buy',
            'type':'opportunity',
            'medium_id2': request.registry['ir.model.data'].xmlid_to_res_id(request.cr, SUPERUSER_ID, 'crm.crm_medium_website'),
            'section_id': request.registry['ir.model.data'].xmlid_to_res_id(request.cr, SUPERUSER_ID, 'website.salesteam_website_sales'),
            'active': True,
            'district_ids': district_ids,
            'city_id' : city_id,
            'property_ids': int(property_type_id),
            'finsishing_id':finishing_id,
            'category' : category_id,
            'currency_id': currency_id,
        }
        
        
        res = pool.get('crm.lead').onchange_client_phone(cr, uid, [], int(phone), context=None)
        if res and len(res)>0 : 
                lead_val.update(res.get('value',{}))
        request.registry['crm.lead'].create(
            cr, SUPERUSER_ID, lead_val, context=dict(context, mail_create_nosubscribe=True))

        return request.website.render("website_property.group_buynig",  {'successmsg':_('Your request has been sent successfully.')})
    
    
    @http.route(['/page/website_property.job_add'
                 ], type='http', auth="public", website=True)
    def page_add_job(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        values = {}
 
        return request.website.render("website_property.job_add", values)

    @http.route(['/job/add'
                 ], type='http', auth="public", website=True)
    def job_add(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID

        name = post.pop('name', '')
        phone = post.pop('phone', '')
        email = post.pop('email', '')
        description = post.pop('description', '')
        
        
        
        lead_val = {
#             'contact_name': name,
            'name': name,
            'phone': phone,
            'email_from': email ,
            'description': description,
        }
        
        #create notif
        user_ids=[]
        alarm_ids=[]
        event_obj = pool.get('calendar.event')
        models_data = pool.get('ir.model.data')
        alarm_notif_id = models_data.get_object_reference(cr,uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
        manager = pool.get('res.users').search(cr, uid, [('login', 'ilike', 'karim.fahim@era-egypt.com')], context=context)
        manager = pool.get('res.users').browse(cr, uid, manager[0], context=context)
        user_ids.append(manager.partner_id.id)
        engy_manager_ids = pool.get('res.users').search(cr, uid, [('login', 'ilike', 'engy.ashraf@era-egypt.com')], context=context)
        engy_manager = pool.get('res.users').browse(cr, uid, engy_manager_ids[0], context=context)
        user_ids.append(engy_manager.partner_id.id)
        alarm_ids.append(alarm_notif_id)
        date_alert = datetime.datetime.now()+relativedelta(minutes=+ 1)
        date_fin = datetime.datetime.now()+relativedelta(minutes=+ 20)
        event_values = {
                                 'name' :"New CV Has been received from Website",
                                 'start_datetime':date_alert.strftime('%Y-%m-%d %H:%M:%S') ,
                                 'stop_datetime':date_fin.strftime('%Y-%m-%d %H:%M:%S') ,
                                 'alarm_ids':[(6,0, alarm_ids)],
                                 'partner_ids':[(6,0,user_ids)],
                                 'is_notif':True,
                                }
        event_id = event_obj.create(cr, uid,event_values, context=context )
        
        job_id = request.registry['hr.applicant'].create(
            cr, SUPERUSER_ID, lead_val, context)
        
        if post['image1']:
            image1= base64.encodestring(post['image1'].read())
            request.registry['ir.attachment'].create(cr, SUPERUSER_ID, {'datas':image1 , 'res_id':job_id ,'res_model':'hr.applicant','name':'aaaaaaaa'}, context)
#         if post['image2']:
#             image2= base64.encodestring(post['image2'].read())
#             request.registry['product.image'].create(cr, SUPERUSER_ID, {'image':image2, 'lead_id':crm_id,'name':name}, context=dict(context, mail_create_nosubscribe=True))
#         if post['image3']:
#             image3= base64.encodestring(post['image3'].read())
#             request.registry['product.image'].create(cr, SUPERUSER_ID, {'image':image3, 'lead_id':crm_id,'name':name}, context=dict(context, mail_create_nosubscribe=True))
            
 
        return request.website.render("website_property.thanksForSelljob", {})
    
    

    @http.route(['/page/website_property.advacend_search','/advacend_search/search'
                 ], type='http', auth="public", website=True)
    def page_advacend_search(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        uid = SUPERUSER_ID
        values = {}
        
        
        orm_district = pool.get('mt.city')
#         orm_city = pool.get('mt.city')
        orm_property_type = pool.get('mt.property.type')
        
        orm_bedrooms = pool.get('mt.bedrooms')
        orm_bathrooms = pool.get('mt.bathrooms')
        
        orm_product = pool.get('product.template')
        orm_division = pool.get('mt.case.categ')
        orm_currency = pool.get('res.currency')
         
        #district
        district_ids = orm_district.search(cr, SUPERUSER_ID, [], context=context)
        districts = orm_district.browse(cr, SUPERUSER_ID, district_ids, context)
        values.update({'districts': districts})
        #citys
#         city_id = orm_city.search(cr, SUPERUSER_ID, [('search_home','=',True)], context=context)
#         citys = orm_city.browse(cr, SUPERUSER_ID, city_id, context)
#         values.update({'citys': citys})
        #property
        property_type_ids = orm_property_type.search( cr, SUPERUSER_ID, [], context=context)
        property_types = orm_property_type.browse( cr, SUPERUSER_ID, property_type_ids, context)
        values.update({'property_types': property_types})
        #bedrooms
        bedrooms_ids = orm_bedrooms.search( cr, SUPERUSER_ID, [], context=context)
        bedrooms = orm_bedrooms.browse( cr, SUPERUSER_ID, bedrooms_ids, context)
        values.update({'bedrooms': bedrooms})       
        #bathrooms
        bathrooms_ids = orm_bathrooms.search( cr, SUPERUSER_ID, [], context=context)
        bathrooms = orm_bathrooms.browse( cr, SUPERUSER_ID, bathrooms_ids, context)
        values.update({'bathrooms': bathrooms})    
        #division
        division_ids = orm_division.search(cr,SUPERUSER_ID,[],context=context)
        divisions = orm_division.browse(cr,SUPERUSER_ID,division_ids,context)       
        values.update({'divisions': divisions}) 
        #currency 
        currency_ids = orm_currency.search(cr,SUPERUSER_ID,[],context=context)
        currency = orm_currency.browse(cr,SUPERUSER_ID,currency_ids,context)       
        values.update({'currency': currency}) 
        
        domain=[('product_type','=','property'),('website_published','=', True)]
        domain_search=[]
        if post.get('district_ids', False) and int(post['district_ids']) !=0 :
            district_id_retain=int(post['district_ids'])
            values.update({'district_id_retain': district_id_retain})
            domain_search.append(('district_ids','=',district_id_retain))

        if post.get('property_type_id', False) and int(post['property_type_id']) !=0 :
            property_type_retain=int(post['property_type_id'])
            values.update({'property_type_retain': property_type_retain})
            domain_search.append(('property_type_id','=',property_type_retain))

        if post.get('service_type', False)  :
            service_type_retain=post.get('service_type')
            values.update({'service_type_retain': service_type_retain })
            domain_search.append(('service_type','=',service_type_retain))
            
        if post.get('min_budget', False) and  post.get('min_budget', False) != ''  :
            min_budget=post.get('min_budget')
            values.update({'min_budget': min_budget })
            domain_search.append(('asking_price','>',float(min_budget.replace(',',''))))

        if post.get('max_budget', False) and  post.get('max_budget', False) != ''  :
            max_budget=post.get('max_budget')
            values.update({'max_budget': max_budget })
            domain_search.append(('asking_price','<',float(max_budget.replace(',',''))))
                           
        if post.get('min_area', False) and  post.get('min_area', False) != ''  :
            min_area=post.get('min_area')
            values.update({'min_area': min_area })
            domain_search.append(('area_sqm','>',float(min_area)))
            
        if post.get('max_area', False) and  post.get('max_area', False) != ''  :
            max_area=post.get('max_area')
            values.update({'max_area': max_area })
            domain_search.append(('area_sqm','<',float(max_area)))

        if post.get('bedroom_id', False) and int(post['bedroom_id']) !=0 :
            bedroom_retain=int(post['bedroom_id'])
            values.update({'bedroom_retain': bedroom_retain})
            domain_search.append(('bedrooms_no_ids','=',bedroom_retain))

        if post.get('bathroom_id', False) and int(post['bathroom_id']) !=0 :
            bathroom_retain=int(post['bathroom_id'])
            values.update({'bathroom_retain': bathroom_retain})
            domain_search.append(('bathrooms_no_ids','=',bathroom_retain))
            
        if post.get('property_id',False):
            domain_search.append(('reference_property','=',post['property_id']))
            values.update({'reference_property': post['property_id']})
        if post.get('division_id',False) and  int(post['division_id']) !=0:
            category_retain=int(post['division_id'])
            domain_search.append(('category','=',category_retain))                           
            values.update({'division_id': category_retain})
        
        if post.get('currency_id',False) and  int(post['currency_id']) !=0:
            currency_retain=int(post['currency_id'])
            domain_search.append(('currency_id','=',currency_retain))                           
            values.update({'currency_id': currency_retain})
 
                                                   
                
        if domain_search:             
            property_ids = orm_product.search( cr, SUPERUSER_ID, domain+domain_search, context=context)
            properties = orm_product.browse( cr, SUPERUSER_ID, property_ids, context)  
            values.update({'properties': properties,'flag_zika':False})
        else :
            values.update({'properties': False,'flag_zika':True})
                              
        return request.website.render("website_property.advacend_search", values)
        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
