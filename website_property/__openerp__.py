# -*- coding: utf-8 -*-

{
    'name': 'Website Property',
    'version': '1.0',
    'author': 'MT-Consulting',
    'depends': ['kingfisher', 'mt_crm_proprety', 'auth_signup', 'website_blog','website_hr_recruitment','crm_claim',],
    'description': '''
        Website  Property
    ''',
    'data': [
        'security/ir.model.access.csv',
        'security/website_property.xml',
        'data/data.xml',
        'view/mt_property.xml',
        'view/mt_project.xml',
        'view/mt_developer.xml',
        'view/mt_city_view.xml',
        'view/crm_lead_view.xml',
        'views/property_add.xml',
        'views/jobs.xml',
        'views/property_details.xml',
        'views/home_page.xml',
        "views/featured_header_page.xml",
        "views/featured_residential_project_page.xml",
        "views/featured_commercial_project_page.xml",
        "views/featured_residential_property_page.xml",
        "views/featured_commercial_property_page.xml",
        "views/featured_residential_investment_property_page.xml",
        "views/featured_commercial_investment_property_page.xml",
        'views/search_page.xml',
        'views/academy_page.xml',
        'views/service_page.xml',
        'views/aboutus_page.xml',
        'views/careers_page.xml',
        'views/customer_care_page.xml',
        'views/disclaimer_page.xml',
        'views/group_buynig_page.xml',
        'views/market_research_page.xml',
        'views/special_offers_page.xml',
        'views/auth_page.xml',
        'views/advacend_search_page.xml'
    ],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
