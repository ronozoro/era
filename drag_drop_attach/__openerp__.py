# -*- coding: utf-8 -*-
{
    "name": "Drag & Drop Multi Attachments", 
    "version": "1.0",
    "summary": "Drag & Drop multiple attachments ",
    "description": """
    """,
    "depends": ["document", "upload_multi_attach",'mt_crm_proprety'],
    'data': ["views/drag_drop_attach_view.xml"],
    'qweb': ['static/src/xml/*.xml'],
    "installable": True, 
    "auto_install": False
}
