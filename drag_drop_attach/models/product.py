# -*- coding: utf-8 -*-
from openerp import api, models


class Product(models.Model):
    _inherit = 'product.template'

    @api.model
    def create(self, vals):
        res = super(Product, self).create(vals)
        product_images = self.env['product.image'].search([('product_tmpl_id', '=', res.id),('is_profile_pic','=',True)],
                                                          order='sequence asc, id desc', limit=1)
        if product_images:
            res.write({'image_medium': product_images.image})
        else:
            res.write({'image_medium': res._get_default_image()})
        return res

    @api.multi
    def write(self, vals):
        res = super(Product, self).write(vals)
        for each in self:
            product_images = self.env['product.image'].search([('product_tmpl_id', '=', each.id),('is_profile_pic','=',True)],
                                                              order='sequence asc, id desc', limit=1)
            if product_images:
                if each.image_medium!=product_images.image:
                    self._cr.execute("update product_template set image_medium=%s where id=%s",
                               (product_images.image, each.id, ))
                    self._cr.execute("update product_template set image_small=%s where id=%s",
                               (product_images.image, each.id, ))
                    return res
                else:
                    return res
            else:
                self._cr.execute("update product_template set image_medium=%s where id=%s",
                                 (each._get_default_image(), each.id,))
        return res


Product()
