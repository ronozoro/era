# -*- coding: utf-8 -*-
{
    'name': "Client request enhancement",

    'summary': """
        Client Request Enhancement""",

    'description': """
        -Split client request to buyer request and seller request
        -Removed (property field from buyer request)
    """,

    'author': "Ahmed Tarek - IT Systems",
    'website': "www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'crm',
    'version': '1',

    # any module necessary for this one to work correctly
    'depends': ['base','crm','mt_crm_brokerage_manager','no_contact_person_handler'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'client_request.xml',
        'menus.xml',
    ],

}