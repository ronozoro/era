# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _

class Buyer(models.Model):
    #     _name = 'client_req_enhancement.client_req_enhancement'
    _inherit = 'crm.lead'

    is_buyer = fields.Boolean(default=False)

    @api.model
    @api.multi
    def open_closed_seller(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
        view_id_form = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')
        view_id_tree = self.env.ref('mt_crm_brokerage_manager.view_crm_lead_tree')
        search_view_id = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        context = {
                   'default_type': 'opportunity',
                   'default_request_type': 'seller',
                   'search_default_request_type': 'seller',
                   # 'default_is_buyer': 0,

                   }
        client_requests = self.search([('type', '=', 'opportunity'),('user_id' , 'in' ,lst)])

        print "#######################################", client_requests
        return {
            'name': _('Seller Request'),
            'type': 'ir.actions.act_window',
            'context': context,
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'crm.lead',
            'views': [(view_id_tree.id, 'tree'),(view_id_form.id, 'form'),(False, 'kanban')],
            'view_id' : view_id_form.id,

            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id','in',client_requests.ids),('is_buyer', '=', 0)]
        }

    @api.model
    @api.multi
    def open_closed_buyer(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
        view_id_form = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')
        view_id_tree = self.env.ref('mt_crm_brokerage_manager.view_crm_lead_tree')

        search_view_id = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')

        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)
        context = {
                   'default_type': 'opportunity',
                   'default_request_type': 'buyer',
                   'search_default_request_type': 'buyer',
                   'default_is_buyer': 1,

                   }
        client_requests = self.search([('type', '=', 'opportunity'),('user_id' , 'in' ,lst)])
        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&", client_requests
        return {
            'name': _('Buyer Request'),
            'type': 'ir.actions.act_window',
            'context': context,
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'crm.lead',
            'views': [(view_id_tree.id, 'tree'),(view_id_form.id, 'form'),(False, 'kanban')],
            'view_id' : view_id_form.id,
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id','in',client_requests.ids),('is_buyer', '=', 1)]
        }


