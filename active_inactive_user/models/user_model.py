# coding=utf-8
from openerp import models, fields, api, exceptions
from datetime import date, datetime, time, timedelta
from openerp.tools.translate import _


class user_active(models.TransientModel):
    _name = 'user.active'

    def _avaliable_lst(self):
        ava_lst=[]
        self.env.cr.execute(
            "select res_users.id as id ,res_partner.name as user_name from res_users inner join res_partner on res_users.partner_id =res_partner.id ")
        res = self.env.cr.dictfetchall()
        vals = []
        users = self.env['user.name'].search([(1, '=', 1)])
        for user in users:
            user.unlink()
        for r in res:
            vals = {'user_id': r['id'], 'name': r['user_name']}
            self.env['user.name'].create(vals)
        #for x in self.env['user.name'].search([(1, '=', 1)]):
            #ava_lst.append(x.id)
        team_leader=self.env['crm.case.section'].search([('user_id','=',self.env.uid)])
        for t in team_leader:
            self.env.cr.execute("select member_id as id from sale_member_rel where section_id=" + str(
                    t['id']))
            res_sql = self.env.cr.dictfetchall()
            for mem in res_sql:
                ava_lst.append(mem['id'])
        domain= "[('user_id', 'in'," + str(ava_lst) + ")]"
        return domain

    active = fields.Boolean(string="Active",  )
    in_active = fields.Boolean(string="Inactive", )
    users_ids = fields.Many2many(comodel_name="user.name", relation="users_rel", column1="user_id", column2="id", string="users", )
    user_id = fields.Many2one(comodel_name="res.users", string="user", required=False )
    user_name = fields.Many2one(comodel_name="user.name", string="user", required=False,domain=_avaliable_lst)

    @api.multi
    def ADD_users(self):
        self.env.cr.execute(
            "select res_users.id as id ,res_partner.name as user_name from res_users inner join res_partner on res_users.partner_id =res_partner.id ")
        res = self.env.cr.dictfetchall()
        vals = []
        users = self.env['user.name'].search([(1, '=', 1)])
        for user in users:
            user.unlink()
        for r in res:
            vals = {'user_id': r['id'], 'name': r['user_name']}
            self.env['user.name'].create(vals)
        self.state='confirm'
    @api.multi
    def apply(self):
        obj=self.env['res.users']
        for s in self:
                users=obj.search([('id','=',s.user_name.user_id)])
                if s.active == True:
                    where = "where id=" + str(s.user_name.user_id)
                    sql = "update res_users set  active='1' " + where
                    self.env.cr.execute(sql)
                elif s.in_active ==True:
                    for us in users:
                        us.active=False

    '''def apply(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = self.read(cr, uid, ids, context=context)[0]
        obj=self.pool.get('res.users')
        print 'bbbbbbbbbbbbb',data['users_ids'],data
        users=obj.browse(cr,uid,data['users_ids'],context=context)
        user= obj.browse(cr, uid, data['user_id'], context=context)
        if data['active']==True:
         print 'GGGGGGGGGGGGGGG',users,user
         for user in users:
            obj.write(cr,uid,user.id,{'active':True},context=context)
        elif data['in_active']==True:
                print 'HHHHHHHHHHHHH',users
                for u in users:
                   obj.write(cr,uid,u.id,{'active':False},context=context)'''

class username(models.Model):
    _name = 'user.name'
    _rec_name = 'name'
    name = fields.Char(string="name",)
    user_id = fields.Integer(string="id", required=False, )

    @api.model
    def default_get(self, fields_list):
        self.env.cr.execute(
            "select res_users.id as id ,res_partner.name as user_name from res_users inner join res_partner on res_users.partner_id =res_partner.id ")
        res = self.env.cr.dictfetchall()
        vals = []
        users = self.env['user.name'].search([(1, '=', 1)])
        for user in users:
            user.unlink()
        for r in res:
            vals = {'user_id': r['id'], 'name': r['user_name']}
            self.env['user.name'].create(vals)
        res = super(username, self).default_get(fields_list)
        return res
