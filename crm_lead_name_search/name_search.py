# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.addons.mt_crm_brokerage_manager.models.crm_lead import crm_lead

class crm_lead_name_search(models.Model):
    _inherit = 'crm.lead'

    def name_search(self, cr, uid, name='', args=None, operator='ilike', context=None, limit=100):
        if context and context.get('domain', False):
            list = []
            list2 = []
            list3 = []
            request_domain = context.get('domain')
            if type(request_domain) == str:
                list = request_domain.split('[')
                list2 = list[3].split(']')
                list3 = list2[0].split(',')
            else:
                list3 = request_domain[0][2]
            search_ids = self.search(cr, uid, [('id', 'in', list3)], context=context)
            return self.name_get(cr, uid, search_ids, context)
        return super(crm_lead, self).name_search(cr, uid, name=name, args=args, operator=operator, context=context, limit=limit)
