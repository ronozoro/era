# -*- coding: utf-8 -*-
{
    'name': "itsys_teamleader_leads",

    'summary': """
       this module make the team leader to see his properties and all members of his team""",

    'description': """
        this module make the team leader to see his properties and all members of his team
    """,

    'author': "itsys corporation - Ahmed Gaber",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base' , 'product','mt_crm_proprety','mt_crm_brokerage_manager' ,'crm', 'itsys_assign_to_sales_team'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}