# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _


class productemplateinherit(models.Model):
    _inherit = 'crm.lead'



    @api.model
    @api.multi
    def open_closed(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
        search_view_id = self.env.ref('mt_crm_brokerage_manager.view_crm_opportunity_brokerage_form_lead')
        view_id = self.env.ref('mt_crm_brokerage_manager.view_crm_opportunity_brokerage_form_lead')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        leads = self.search([('type', '=', 'lead'),('user_id' , 'in' ,lst)])
        print "yyyyyyyyyyyyyyyyyyyyyyyy",leads

        context = {'default_type': 'lead'}
        return {
            'name': _('Leads'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.lead',
            'views': [(False, 'tree'), (view_id.id, 'form')],
            'search_view_id': search_view_id.id,
            'target': 'current',
            'context':context ,
            # and here show only your records make sure it's not empty
            'domain': [('id', 'in', leads.ids)]
        }
# 'domain': [('id', 'in', leads.ids),('type','=','opportunity')]