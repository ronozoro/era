# -*- coding: utf-8 -*-
{
    'name': "Hide budget for seller leads",


    'description': """
        Hide budget for seller leads
    """,

    'author': "Ahmed Tarek - IT Systems",
    'website': "www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'crm',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['mt_crm_brokerage_manager','itsys_currency_updater'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'leads.xml',
    ],

}