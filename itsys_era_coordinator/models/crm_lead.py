# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.osv import osv
from openerp.tools.translate import _


class crm_case_section(osv.osv):
    _inherit = "crm.case.section"

    category_id = fields.Many2one('mt.case.categ',string="Division")




class CrmLeadInherit(models.Model):
    _inherit = 'crm.lead'

    @api.model
    @api.multi
    def open_closed_resediential(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('category_id.categ_type','=','residential')])
        search_view_id = self.env.ref('mt_crm_brokerage_manager.view_crm_opportunity_brokerage_form_lead')
        view_id = self.env.ref('mt_crm_brokerage_manager.view_crm_opportunity_brokerage_form_lead')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)


        leads = self.search([('type', '=', 'lead'),('user_id' , 'in' ,lst),('category.categ_type','=','residential')])


        context = {'default_type': 'lead'}
        return {
            'name': _('Leads'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.lead',
            'views': [(False, 'tree'), (view_id.id, 'form')],
            'search_view_id': search_view_id.id,
            'target': 'current',
            'context':context ,
            # and here show only your records make sure it's not empty
            'domain': [('id', 'in', leads.ids)]
        }

    @api.model
    @api.multi
    def open_closed_commercial(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('category_id.categ_type','=','commercial')])
        search_view_id = self.env.ref('mt_crm_brokerage_manager.view_crm_opportunity_brokerage_form_lead')
        view_id = self.env.ref('mt_crm_brokerage_manager.view_crm_opportunity_brokerage_form_lead')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)


        leads = self.search([('type', '=', 'lead'),('user_id' , 'in' ,lst),('category.categ_type','=','commercial')])

        context = {'default_type': 'lead'}
        return {
            'name': _('Leads'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.lead',
            'views': [(False, 'tree'), (view_id.id, 'form')],
            'search_view_id': search_view_id.id,
            'target': 'current',
            'context':context ,
            # and here show only your records make sure it's not empty
            'domain': [('id', 'in', leads.ids)]
        }

    @api.model
    @api.multi
    def open_closed_seller_resedential(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('category_id.categ_type','=','residential')])
        view_id_form = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')
        view_id_tree = self.env.ref('mt_crm_brokerage_manager.view_crm_lead_tree')
        search_view_id = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')
        lst = []
        lst2 = []
        for each in records:
            for each_user in each.member_ids:

                lst.append(each_user.id)

        context = {
            'default_type': 'opportunity',
            'default_request_type': 'seller',
            'search_default_request_type': 'seller',
            'default_is_buyer': 0,

        }
        client_requests = self.search([('type', '=', 'opportunity'), ('user_id', 'in', lst),('category.categ_type','=','residential')])

        # for rec in client_requests:
        #     if rec.is_buyer ==0:
        #         lst2.append(rec.id)
        print "#######################################", client_requests
        return {
            'name': _('Seller Request'),
            'type': 'ir.actions.act_window',
            'context': context,
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'crm.lead',
            'views': [(view_id_tree.id, 'tree'), (view_id_form.id, 'form'), (False, 'kanban')],
            'view_id': view_id_form.id,

            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', client_requests.ids), ('is_buyer', '=', 0)]
        }

    @api.model
    @api.multi
    def open_closed_buyer_resedential(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('category_id.categ_type','=','residential')])
        view_id_form = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')
        view_id_tree = self.env.ref('mt_crm_brokerage_manager.view_crm_lead_tree')

        search_view_id = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')

        lst = []
        lst2 = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)
        context = {
            'default_type': 'opportunity',
            'default_request_type': 'buyer',
            'search_default_request_type': 'buyer',
            'default_is_buyer': 1,

        }
        client_requests = self.search([('type', '=', 'opportunity'), ('user_id', 'in', lst),('category.categ_type','=','residential')])
        # for rec in client_requests:
        #     if rec.is_buyer ==1:
        #         lst2.append(rec.id)

        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&", client_requests
        return {
            'name': _('Buyer Request'),
            'type': 'ir.actions.act_window',
            'context': context,
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'crm.lead',
            'views': [(view_id_tree.id, 'tree'), (view_id_form.id, 'form'), (False, 'kanban')],
            'view_id': view_id_form.id,
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', client_requests.ids), ('is_buyer', '=', 1)]
        }

    @api.model
    @api.multi
    def open_closed_seller_commercial(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
        view_id_form = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')
        view_id_tree = self.env.ref('mt_crm_brokerage_manager.view_crm_lead_tree')
        search_view_id = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')
        lst = []
        lst2 = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        context = {
            'default_type': 'opportunity',
            'default_request_type': 'seller',
            'search_default_request_type': 'seller',
            'default_is_buyer': 0,

        }
        client_requests = self.search(
            [('type', '=', 'opportunity'), ('user_id', 'in', lst), ('category.categ_type', '=', 'commercial')])

        # for rec in client_requests:
        #     if rec.is_buyer ==0:
        #         lst2.append(rec.id)
        print "#######################################", client_requests
        return {
            'name': _('Seller Request'),
            'type': 'ir.actions.act_window',
            'context': context,
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'crm.lead',
            'views': [(view_id_tree.id, 'tree'), (view_id_form.id, 'form'), (False, 'kanban')],
            'view_id': view_id_form.id,

            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', client_requests.ids), ('is_buyer', '=', 0)]
        }

    @api.model
    @api.multi
    def open_closed_buyer_commercial(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
        view_id_form = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')
        view_id_tree = self.env.ref('mt_crm_brokerage_manager.view_crm_lead_tree')

        search_view_id = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')

        lst = []
        lst2 = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)
        context = {
            'default_type': 'opportunity',
            'default_request_type': 'buyer',
            'search_default_request_type': 'buyer',
            'default_is_buyer': 1,

        }
        client_requests = self.search(
            [('type', '=', 'opportunity'), ('user_id', 'in', lst), ('category.categ_type', '=', 'commercial')])
        # for rec in client_requests:
        #     if rec.is_buyer ==1:
        #         lst2.append(rec.id)

        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&", client_requests
        return {
            'name': _('Buyer Request'),
            'type': 'ir.actions.act_window',
            'context': context,
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'crm.lead',
            'views': [(view_id_tree.id, 'tree'), (view_id_form.id, 'form'), (False, 'kanban')],
            'view_id': view_id_form.id,
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', client_requests.ids), ('is_buyer', '=', 1)]
        }



    @api.model
    @api.multi
    def open_closed_lead_ceo(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
        search_view_id = self.env.ref('mt_crm_brokerage_manager.view_crm_opportunity_brokerage_form_lead')
        view_id = self.env.ref('mt_crm_brokerage_manager.view_crm_opportunity_brokerage_form_lead')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)


        leads = self.search([('type', '=', 'lead'),('user_id' , 'in' ,lst)])


        context = {'default_type': 'lead'}
        return {
            'name': _('Leads'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.lead',
            'views': [(False, 'tree'), (view_id.id, 'form')],
            'search_view_id': search_view_id.id,
            'target': 'current',
            'context':context ,
            # and here show only your records make sure it's not empty
            'domain': [('id', 'in', leads.ids)]
        }




    @api.model
    @api.multi
    def open_closed_seller_ceo(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
        view_id_form = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')
        view_id_tree = self.env.ref('mt_crm_brokerage_manager.view_crm_lead_tree')
        search_view_id = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')
        lst = []
        lst2 = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        context = {
            'default_type': 'opportunity',
            'default_request_type': 'seller',
            'search_default_request_type': 'seller',
            'default_is_buyer': 0,

        }
        client_requests = self.search([('type', '=', 'opportunity'), ('user_id', 'in', lst)])

        # for rec in client_requests:
        #     if rec.is_buyer ==0:
        #         lst2.append(rec.id)
        print "#######################################", client_requests
        return {
            'name': _('Seller Request'),
            'type': 'ir.actions.act_window',
            'context': context,
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'crm.lead',
            'views': [(view_id_tree.id, 'tree'), (view_id_form.id, 'form'), (False, 'kanban')],
            'view_id': view_id_form.id,

            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', client_requests.ids), ('is_buyer', '=', 0)]
        }

    @api.model
    @api.multi
    def open_closed_buyer_ceo(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
        view_id_form = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')
        view_id_tree = self.env.ref('mt_crm_brokerage_manager.view_crm_lead_tree')

        search_view_id = self.env.ref('client_req_enhancement.view_crm_opportunity_brokerage_form_buyer_new')

        lst = []
        lst2 = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)
        context = {
            'default_type': 'opportunity',
            'default_request_type': 'buyer',
            'search_default_request_type': 'buyer',
            'default_is_buyer': 1,

        }
        client_requests = self.search(
            [('type', '=', 'opportunity'), ('user_id', 'in', lst)])
        # for rec in client_requests:
        #     if rec.is_buyer ==1:
        #         lst2.append(rec.id)

        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&", client_requests
        return {
            'name': _('Buyer Request'),
            'type': 'ir.actions.act_window',
            'context': context,
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'crm.lead',
            'views': [(view_id_tree.id, 'tree'), (view_id_form.id, 'form'), (False, 'kanban')],
            'view_id': view_id_form.id,
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', client_requests.ids), ('is_buyer', '=', 1)]
        }


