# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.osv import osv
from openerp.tools.translate import _

class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.model
    @api.multi
    def open_closed_residential(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('category_id.categ_type','=','residential')])
        search_view_id = self.env.ref('base.view_partner_tree')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        partns = self.search([('user_id', 'in', lst)])
        return {
            'name': _('Clients'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,kanban',
            'res_model': 'res.partner',
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', partns.ids)]
        }

    @api.model
    @api.multi
    def open_closed_commercial(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
        search_view_id = self.env.ref('base.view_partner_tree')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        partns = self.search([('user_id', 'in', lst)])
        return {
            'name': _('Clients'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,kanban',
            'res_model': 'res.partner',
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', partns.ids)]
        }

    @api.model
    @api.multi
    def open_closed_client_ceo(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
        search_view_id = self.env.ref('base.view_partner_tree')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        partns = self.search([('customer','=',True),('user_id', 'in', lst)])
        return {
            'name': _('Clients'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,kanban',
            'res_model': 'res.partner',
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', partns.ids)]
        }
