# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.osv import osv
from openerp.tools.translate import _


class productemplateinherit(models.Model):
    _inherit = 'product.template'

    @api.model
    @api.multi
    def open_closed_resediential(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')])
        search_view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
        view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        proprs = self.search([('agent_id.id', 'in', lst),('category.categ_type', '=', 'residential')])
        print "RRRRRRRRRRRRRRRRRRRRRRRRRRRR",proprs

        return {
            'name': _('Properties'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,kanban',
            'res_model': 'product.template',
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', proprs.ids)]
        }



    @api.model
    @api.multi
    def open_closed_commercial(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
        search_view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
        view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        proprs = self.search([('agent_id.id', 'in', lst),('category.categ_type', '=', 'commercial')])
        print "RRRRRRRRRRRRRRRRRRRRRRRRRRRR",proprs

        return {
            'name': _('Properties'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,kanban',
            'res_model': 'product.template',
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', proprs.ids)]
        }

    @api.model
    @api.multi
    def open_closed_properties_ceo(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
        search_view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
        view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        proprs = self.search([('agent_id.id', 'in', lst), ('product_type','=','property')])
        print "RRRRRRRRRRRRRRRRRRRRRRRRRRRR", proprs

        return {
            'name': _('Properties'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,kanban',
            'res_model': 'product.template',
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', proprs.ids)]
        }

    @api.model
    @api.multi
    def open_closed_developers_ceo(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search(
            [('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
        search_view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
        view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        proprs = self.search([('agent_id.id', 'in', lst), ('product_type', '=', 'developer')])
        print "RRRRRRRRRRRRRRRRRRRRRRRRRRRR", proprs

        return {
            'name': _('Properties'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,kanban',
            'res_model': 'product.template',
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', proprs.ids)]
        }



    @api.model
    @api.multi
    def open_projects_ceo(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
        search_view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
        view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        proprs = self.search([('agent_id.id', 'in', lst), ('product_type', '=', 'project')])
        print "RRRRRRRRRRRRRRRRRRRRRRRRRRRR", proprs

        return {
            'name': _('Properties'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,kanban',
            'res_model': 'product.template',
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', proprs.ids)]
        }

    # @api.model
    # @api.multi
    # def open_closed_projects_ceo(self):
    #     # here you can filter you records as you want
    #     records = self.env['crm.case.section'].search([('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
    #     search_view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
    #     view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
    #     lst = []
    #     for each in records:
    #         for each_user in each.member_ids:
    #             lst.append(each_user.id)
    #
    #     proprs = self.search([('agent_id.id', 'in', lst), ('product_type', '=', 'developer')])
    #     print "RRRRRRRRRRRRRRRRRRRRRRRRRRRR", proprs
    #
    #     return {
    #         'name': _('Properties'),
    #         'type': 'ir.actions.act_window',
    #         'view_type': 'form',
    #         'view_mode': 'tree,form,kanban',
    #         'res_model': 'product.template',
    #         'search_view_id': search_view_id.id,
    #         'target': 'current',
    #         'domain': [('id', 'in', proprs.ids)]
    #     }
    #
    #





