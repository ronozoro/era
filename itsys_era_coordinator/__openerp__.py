# -*- coding: utf-8 -*-
{
    'name': "itsys_era_coordinator",

    'summary': """
        coordinator privilages""",

    'description': """
        coordinator privilages
    """,

    'author': "Itsys- Ahmed Gaber ",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base',
                'sales_team',
                'product',
                'calendar',
                'mt_crm_brokerage_manager',
                'mt_crm_proprety',
                'portal',
                'crm_claim',
                'active_inactive_user'],

    # always loaded
    'data': [
        'security/coordinator_groups_view.xml',
        'security/ir.model.access.csv',
        'views/product_template_view.xml',
        'views/crm_lead_view.xml',
        'views/res_partner_view.xml',
    ],

}