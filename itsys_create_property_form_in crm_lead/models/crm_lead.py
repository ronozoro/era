import datetime

# import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv
from openerp import api
# from openerp.tools.translate import _
# import os
# from openerp.tools import image_resize_image
# from openerp import SUPERUSER_ID, tools
# from openerp.modules.module import get_module_resource
# from openerp import tools, api, addons, SUPERUSER_ID
import openerp.addons.decimal_precision as dp
import pprint
from pprint import pformat
import pprint


class lead_image(osv.Model):
    _name = 'lead.image'

    _columns = {
        'name': fields.char('Name'),
        'description': fields.text('Description'),
        'image_alt': fields.text('Image Label'),
        'image': fields.binary('Image'),
        'image_small': fields.binary('Small Image'),
        'lead_id': fields.many2one('crm.lead', 'Lead'),
        'sequence': fields.integer(string='Sequence'),
        'is_profile_pic': fields.boolean(string='For Profile'),

    }


class crm_lead_Seller(osv.osv):
    _inherit = "crm.lead"

    def create(self, cr, uid, vals, context=None):
        res = super(crm_lead_Seller, self).create(cr, uid, vals, context=context)
        production_ids = []
        prod_obj = self.pool.get('product.template')
        # lead_obj = self.pool.get('crm.lead').search(cr, uid, res, context=context)
        leads = self.pool.get('crm.lead').browse(cr, uid, [res])
        # pprint.pformat(vals)
        # print"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",vals
        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&", leads
        for each in leads:
            if each.prod_name and (each.is_buyer != True):
                print "KKKKKKKKKKKKKKKKKKKKKKK", each.images
                curr_product = {
                    'name': each.prod_name or ' ',
                    'partner_id': each.partner_id.id or False,  # each.product_id.id,
                    'category': each.category.id or False,
                    'property_sorting_id': each.property_sorting_id.id or False,
                    'property_type_id': each.property_ids.id or False,
                    'agent_id': each.user_id.id or False,
                    # 'requset_id': each.id,
                    'state_id': each.state_id.id or False,
                    'city_id': each.district_ids.id or False,
                    'agreement': each.agreement or False,
                    'start_date': each.agreement_start or False,
                    'end_date': each.agreement_end or False,
                    'district_ids': each.district_ids.id or False,
                    'neighborhood_ids': each.neighborhood_ids.id or False,
                    'bedrooms_no_ids': each.bedrooms_no_ids.id or False,
                    'asking_price': each.asking_price or False,
                    'currency_id': each.currency_id3.id or False,
                    'area_sqm': each.area_sqm or False,
                    'product_type': 'property' or False,
                    'default_states': 'available' or False,
                    'service_type': each.service_ids or False,
                    'licences_id': each.licence_id.id or False,
                    'bathrooms_no_ids': each.bathrooms_no_ids.id or False,
                    'product_unit_id8': each.product_unit_id8.id or False,
                    'bua_gross': each.bua_gross or False,
                    'bua_gross2': each.bua_gross2 or False,
                    'bua_net': each.bua_net or False,
                    'gardean_area': each.gardean_area or False,
                    'roof_area': each.roof_area or False,
                    'commer_fron': each.commer_fron or False,
                    'ceiling_hei': each.ceiling_hei or False,
                    'land_area': each.land_area or False,
                    'land_area2': each.land_area2 or False,
                    'product_unit_id': each.product_unit_id.id or False,
                    'product_unit_id1': each.product_unit_id1.id or False,
                    'product_unit_id2': each.product_unit_id2.id or False,
                    'product_unit_id3': each.product_unit_id3.id or False,
                    'product_unit_id5': each.product_unit_id5.id or False,
                    'product_unit_id131': each.product_unit_id131.id or False,
                    'product_unit_id6': each.product_unit_id6.id or False,
                    'product_unit_id7': each.product_unit_id7.id or False,
                    'is_company': each.client_code or False,
                    'client_type_id': each.client_type_id.id or False,
                    'sale_price': each.sale_price or False,
                    'payment': each.payment or False,
                    'agreement_serial': each.agreement_serial or False,
                    'request_id': each.id or False,
                    'domain': each.mailing_domain or False,
                    'hide_request': True or False,
                    'featured_residential_property': each.featured_residential_property or False,
                    'featured_commercial_property': each.featured_commercial_property or False,
                    'featured_residential_investement': each.featured_residential_investement or False,
                    'featured_commercial_investement': each.featured_commercial_investement or False,
                    'website_published': each.website_published or False,
                    'website_sequence': each.website_sequence or False,
                    'public_categ_ids': (0, 0, [each.public_categ_ids.ids]),
                    # 'images': [(6, 0, each.images.ids)],
                    # 'images': (0, 0, [each.images.ids]),
                    'description_sale': each.description_sale or False,
                }

                production_ids = prod_obj.create(cr, uid, curr_product)
                for img_id in each.images:
                    self.pool.get('product.image').create(cr,uid,{
                        'name': img_id.name,
                        'description': img_id.description,
                        'image_alt': img_id.image_alt,
                        'image': img_id.image,
                        'image_small': img_id.image_small,
                        'sequence': img_id.sequence,
                        'is_profile_pic': img_id.is_profile_pic,
                        'product_tmpl_id': production_ids,

                    })

                    # print(created,"UUUUUUUUUUUUUUUUUUUUUUUUUUU")


        leads.write({'property': production_ids,'city_id':leads.district_ids.id})

        return res

    def copy(self, cr, uid, id, default=None, context=None):
        res = super(crm_lead_Seller, self).copy(cr, uid, id, default=default, context=context)
        print(res)
        for img_id in self.browse(cr,uid,id).images:
            created = self.pool.get('lead.image').create(cr, uid, {
                'name': img_id.name,
                'description': img_id.description,
                'image_alt': img_id.image_alt,
                'image': img_id.image,
                'image_small': img_id.image_small,
                'sequence': img_id.sequence,

                'lead_id': res,

            })
            created = self.pool.get('product.image').create(cr, uid, {
                'name': img_id.name,
                'description': img_id.description,
                'image_alt': img_id.image_alt,
                'image': img_id.image,
                'image_small': img_id.image_small,
                'sequence': img_id.sequence,
                'is_profile_pic': img_id.is_profile_pic,
                'product_tmpl_id': self.browse(cr,uid,res).property.id,

            })

        print "@@@@@@@@@@@@@@@@@@@@@@@@@",id,"#############33",default,"PPPPPPPP",context
        return res

    def write(self, cr, uid, ids, vals, context=None):
        prod_obj = self.pool.get('product.template')
        print "################33", ids

        for each in self.pool.get('crm.lead').browse(cr, uid, ids):
            if vals.get('prod_name'):
                each.property.write({'name': vals.get('prod_name')})
            if vals.get('partner_id'):
                each.property.write({'partner_id': vals.get('partner_id')})
            if vals.get('category'):
                each.property.write({'category': vals.get('category')})
            if vals.get('property_ids'):
                each.property.write({'property_type_id': vals.get('property_ids')})

            if vals.get('property_sorting_id'):
                each.property.write({'property_sorting_id': vals.get('property_sorting_id')})

            if vals.get('user_id'):
                each.property.write({'agent_id': vals.get('user_id')})

            if vals.get('state_id'):
                each.property.write({'state_id': vals.get('state_id')})

            if vals.get('city_id'):
                each.property.write({'city_id': vals.get('district_ids')})

            if vals.get('agreement_start'):
                each.property.write({'start_date': vals.get('agreement_start')})

            if vals.get('district_ids'):
                each.property.write({'district_ids': vals.get('district_ids')})

            if vals.get('neighborhood_ids'):
                each.property.write({'neighborhood_ids': vals.get('neighborhood_ids')})

            if vals.get('asking_price'):
                each.property.write({'asking_price': vals.get('asking_price')})

            if vals.get('bedrooms_no_ids'):
                each.property.write({'bedrooms_no_ids': vals.get('bedrooms_no_ids')})

            if vals.get('currency_id3'):
                each.property.write({'currency_id': vals.get('currency_id3')})

            if vals.get('area_sqm'):
                each.property.write({'area_sqm': vals.get('area_sqm')})

            if vals.get('service_ids'):
                each.property.write({'service_type': vals.get('service_ids')})

            if vals.get('licences_id'):
                each.property.write({'licences_id': vals.get('licences_id')})

            if vals.get('bathrooms_no_ids'):
                each.property.write({'bathrooms_no_ids': vals.get('bathrooms_no_ids')})

            if vals.get('product_unit_id8'):
                each.property.write({'product_unit_id8': vals.get('product_unit_id8')})

            if vals.get('bua_gross'):
                each.property.write({'bua_gross': vals.get('bua_gross')})

            if vals.get('bua_gross2'):
                each.property.write({'bua_gross2': vals.get('bua_gross2')})

            if vals.get('bua_net'):
                each.property.write({'bua_net': vals.get('bua_net')})
            if vals.get('gardean_area'):
                each.property.write({'gardean_area': vals.get('gardean_area')})

            if vals.get('roof_area'):
                each.property.write({'roof_area': vals.get('roof_area')})
            if vals.get('ceiling_hei'):
                each.property.write({'ceiling_hei': vals.get('ceiling_hei')})

            if vals.get('commer_fron'):
                each.property.write({'commer_fron': vals.get('commer_fron')})

            if vals.get('land_area'):
                each.property.write({'land_area': vals.get('land_area')})

            if vals.get('land_area2'):
                each.property.write({'land_area2': vals.get('land_area2')})

            if vals.get('product_unit_id'):
                each.property.write({'product_unit_id': vals.get('product_unit_id')})

            if vals.get('product_unit_id1'):
                each.property.write({'product_unit_id1': vals.get('product_unit_id1')})

            if vals.get('product_unit_id2'):
                each.property.write({'product_unit_id2': vals.get('product_unit_id2')})

            if vals.get('product_unit_id3'):
                each.property.write({'product_unit_id3': vals.get('product_unit_id3')})

            if vals.get('product_unit_id5'):
                each.property.write({'product_unit_id5': vals.get('product_unit_id5')})

            if vals.get('product_unit_id131'):
                each.property.write({'product_unit_id131': vals.get('product_unit_id131')})

            if vals.get('product_unit_id6'):
                each.property.write({'product_unit_id6': vals.get('product_unit_id6')})

            if vals.get('product_unit_id7'):
                each.property.write({'product_unit_id7': vals.get('product_unit_id7')})

            if vals.get('client_code'):
                each.property.write({'is_company': vals.get('client_code')})

            if vals.get('client_type_id'):
                each.property.write({'client_type_id': vals.get('client_type_id')})

            if vals.get('sale_price'):
                each.property.write({'sale_price': vals.get('sale_price')})

            if vals.get('payment'):
                each.property.write({'payment': vals.get('payment')})

            if vals.get('agreement_serial'):
                each.property.write({'agreement_serial': vals.get('agreement_serial')})
            if vals.get('images'):
                each.property.write({'images': vals.get('images')})

            if vals.get('images'):
                each.property.write({'images': vals.get('images')})

            if vals.get('featured_residential_property'):
                each.property.write({'featured_residential_property': vals.get('featured_residential_property')})

            if vals.get('featured_commercial_property'):
                each.property.write({'featured_commercial_property': vals.get('featured_commercial_property')})

            if vals.get('featured_residential_investement'):
                each.property.write({'featured_residential_investement': vals.get('featured_residential_investement')})

            if vals.get('featured_commercial_investement'):
                each.property.write({'featured_commercial_investement': vals.get('featured_commercial_investement')})

            if vals.get('website_published'):
                each.property.write({'website_published': vals.get('website_published')})

            if vals.get('website_sequence'):
                each.property.write({'website_sequence': vals.get('website_sequence')})

            if vals.get('public_categ_ids'):
                each.property.write({'public_categ_ids': vals.get('public_categ_ids')})

            if vals.get('description_sale'):
                each.property.write({'description_sale': vals.get('description_sale')})



        #         'origine_id': each.id,
        #         'domain': each.mailing_domain,
        #         'kitchen_type_id':each.kitchen_type_id.id
        #         # 'reception_nbr':each.reception_nbr.id
        #
        #         # 'type':'opportunity',
        #
        #         # 'currency_id': rec.currency_id,
        #     }
        print "RRRRRRRRRRRRRRRRRRRRRR", vals
        #     production_ids = prod_obj.write(cr,uid,curr_product)
        # self.write({'property':production_ids})

        return super(crm_lead_Seller, self).write(cr, uid, ids, vals, context=context)

    @api.multi
    def request_url(self):
        local_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        # local_url = 'http://era-egypt.com'
        property_url = '/residential/property/%s' % (str(self.id))
        final_url = local_url + property_url
        base_url = "https://www.facebook.com/sharer/sharer.php?u=%s&t=%s menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600" % (
            final_url, self.prod_name)
        return base_url

    _columns = {
        'prod_name': fields.char('Property Name'),
        'property_sorting_id': fields.many2one('mt.property.sorting', 'Property Sorting',required=True),
        'hide_request': fields.boolean("Hide Request"),
        'images': fields.one2many('lead.image', 'lead_id',
                                  string='Images'),

        'featured_residential_property': fields.boolean('Featured Residential Property'),

        'featured_commercial_property': fields.boolean('Featured Commercial Property'),

        'featured_residential_investement': fields.boolean('Featured Residential Investement'),

        'featured_commercial_investement': fields.boolean('Featured Commercial Investement'),

        'website_published': fields.boolean('Available in the website', copy=False),

        'website_sequence': fields.integer('Sequence', help="Determine the display order in the Website E-commerce"),

        'public_categ_ids': fields.many2many('product.public.category', string='Public Category',
                                             help="Those categories are used to group similar products for e-commerce."),

        'description_sale': fields.text('Sale Description', translate=True,
                                        help="A description of the Product that you want to communicate to your customers. "
                                             "This description will be copied to every Sale Order, Delivery Order and Customer Invoice/Refund"),

    }
