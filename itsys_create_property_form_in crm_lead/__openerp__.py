

{
    'name': 'ITSYS Create Property Form In CRM Lead',
    'version': '1.1',
    'author': 'ITSYS Corporation - ahmed gaber',
    'summary': '',
    'description': """

    """,
    'website': 'www.it-syscorp.com',
    'depends': ['base','mt_crm_brokerage_manager','website_property','client_req_enhancement','mt_crm_proprety'],
    'category': 'Sales',
    'sequence': 16,
    'demo': [],
    'data': [
        'security/ir.model.access.csv',
        'views/product_view.xml',
        'views/lead_view.xml',



    ],
    'test': [
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'qweb': [],
}