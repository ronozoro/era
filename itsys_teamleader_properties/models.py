# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _


class productemplateinherit(models.Model):
    _inherit = 'product.template'

    #
    # @api.multi
    # def _get_current_user(self):
    #     self.curr_user_id = self.env.uid
    #     print "RRRRRRRRRRRRRRRRRRRRRRRRRRRr" , self.curr_user_id

    curr_user_id = fields.Many2one('res.users', 'Current User')
    prop_sale_team_id = fields.Many2one('crm.case.section', 'Sale Team')

    @api.model
    @api.multi
    def open_closed(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
        search_view_id = self.env.ref('mt_crm_proprety.mt_property_tree')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)
            # lst.append(each.user_id.id)

        # print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^666", lst, "%%%%%%%%%%%%%%%%%%%%%%5", records
        prods = self.search([('agent_id.id' , 'in' ,lst)])
        # print "%%%%%%%%%%%%%%%%%%%%%%%55555", prods
        return {
            'name': _('Properties'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,kanban',
            'res_model': 'product.template',
            'search_view_id': search_view_id.id,
            'target': 'current',
            # 'context': {'readonly_by_pass': True, 'check_domain': True},
            # and here show only your records make sure it's not empty
            'domain': [('id', 'in', prods.ids)]
        }
