#  -*- coding: utf-8 -*-

{
    "name": "Cities",
    "version": "0.1",
    "author": "MT-Consulting",
    "category": 'Cities',
    'complexity': "easy",
    "description": """
        Search for Tunisien zip codes and cities 
   
    """,
    'website': 'http://www.mtconsulting.odoo.com',
    'images': [],
    'init_xml': [],
     "depends": [],
    'data': [
                   "views/mt_city_view.xml",
   ],
    'demo_xml': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}
#  vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
