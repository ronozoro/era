# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import ValidationError


class DivisionConstrainLead(models.Model):
    _inherit = 'crm.lead'

    # def on_change_property_ids(self, cr, uid, ids, property_ids, context=None):
    #     print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
    #     lst = []
    #     if not property_ids:
    #         return {'domain': {'property_ids': [('id', '=', lst)]}}
    #
    #     type_prop = type_villa = '0'
    #     #         prop = self.browse(cr, uid, ids)
    #     if property_ids:
    #         #             ids = property_ids[0][2]
    #         #             if ids and len(ids) > 0:
    #
    #         for categ in self.pool.get('mt.property.type').browse(cr, uid, property_ids):
    #
    #             if categ.name in ('Ground floor with Garden', 'Villa'):
    #                 type_prop = '1'
    #             if categ.name == 'Villa':
    #                 type_villa = '1'
    #
    #     return {'value': {'type_prop': type_prop, 'type_villa': type_villa}}

    def on_change_category(self, cr, uid, ids, category, context=None):
        # -------------------------------------------------------------------------------
        res = super(DivisionConstrainLead, self).on_change_category(cr, uid, ids, category, context=context)

        user = self.pool.get('res.users').browse(cr, uid, context=context)

        if context is None:
            context = {}
        lst = []

        if category:
            categ_type = self.pool.get('mt.case.categ').browse(cr, uid, category, context=context)
            print "WWWWWWWWWWWWWWWWWWWWWWWWWWWWw",categ_type
            # cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', categ_type.ids)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context).ids
            if categ_type.name == 'Residential':
                res = {'domain': {'property_ids': [('id', 'in', propr)]}, 'value': {'type_lead': '0'}}

            elif categ_type.name == 'Commercial':
                res = {'domain': {'property_ids': [('id', 'in', propr)]}, 'value': {'type_lead': '1'}}

        else:
            # if user.has_group('base.group_system'):
            #     catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', category)],
            #                                                       context=context)
            #     propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
            #     if res.get('domain'):
            #         res['domain']['property_ids'] = [('id', 'in', propr.ids)]
            #         res['value']['type_lead'] = '1'

            if user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_ids'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '0'


                # res = {'domain': {'property_ids': [('id', 'in', propr.ids)]}, 'value': {'type_lead': '1'}}
                # if res['domain']
            if user.has_group('mt_crm_brokerage_manager.group_residential_seller_agent_id'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_ids'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '0'

            if user.has_group('mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_ids'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '0'

            if user.has_group('mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_ids'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '1'


            if user.has_group('mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_ids'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '1'

            if user.has_group('mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_ids'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '1'

            if user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group('itsys_era_coordinator.group_division_head_commercial'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_ids'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '1'


            if user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group('itsys_era_coordinator.group_division_head_resedential') :
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_ids'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '0'




        return res


class ProductTemplateInherit(models.Model):
    _inherit = 'product.template'

    def on_change_category(self, cr, uid, ids, category,service_type, context=None):

        # -------------------------------------------------------------------------------
        res = super(ProductTemplateInherit, self).on_change_category(cr, uid, ids, category,service_type, context=context)

        user = self.pool.get('res.users').browse(cr, uid, context=context)

        if context is None:
            context = {}
        lst = []

        if category:
            categ_type = self.pool.get('mt.case.categ').browse(cr, uid, category, context=context)
            # cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', categ_type.ids)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context).ids
            if categ_type.name == 'Residential':
                res = {'domain': {'property_type_id': [('id', 'in', propr)]}, 'value': {'type_lead': '0'}}

            elif categ_type.name == 'Commercial':
                res = {'domain': {'property_type_id': [('id', 'in', propr)]}, 'value': {'type_lead': '1'}}
        else:
            # if user.has_group('base.group_system'):
            #     print "JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJjj"
            #     categ_type = self.pool.get('mt.case.categ').search(cr, uid, [], context=context)
            #     print "categ_type",categ_type
            #     cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            #     print "cats2",cats2
            #     catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
            #                                                       context=context)
            #     print "catego",catego
            #     propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context).ids
            #     print "propr",propr
            #     if res.get('domain'):
            #         res['domain']['property_type_id'] = [('id', 'in', propr)]
            #         res['value']['type_lead'] = '1'

            if user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_type_id'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '0'


            if user.has_group('mt_crm_brokerage_manager.group_residential_seller_agent_id'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_type_id'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '0'

            if user.has_group('mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_type_id'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '0'

            if user.has_group('mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_type_id'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '1'


            if user.has_group('mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_type_id'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '1'

            if user.has_group('mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_type_id'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '1'


            if user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group('itsys_era_coordinator.group_division_head_resedential'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_type_id'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '0'


            if user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group('itsys_era_coordinator.group_division_head_commercial'):
                categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')],
                                                                   context=context)
                cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                  context=context)
                propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
                if res.get('domain'):
                    res['domain']['property_type_id'] = [('id', 'in', propr.ids)]
                    res['value']['type_lead'] = '1'

        return res