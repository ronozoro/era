# -*- coding: utf-8 -*-
{
    'name': "itsys_assign_to_sales_team",

    'summary': """
        This Module allow to assign members to sales team """,

    'description': """
        This Module allow to assign members to sales team
    """,

    'author': "Ahmed Gaber",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base' ,'sales_team','mt_crm_brokerage_manager','itsys_division_minimize','itsys_era_coordinator'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}