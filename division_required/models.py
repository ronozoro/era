# -*- coding: utf-8 -*-

from openerp import models, fields, api

class property_type_division_required(models.Model):
    _inherit = 'mt.property.type'

    def fields_get(self, cr, uid, fields=None, context=None, write_access=True):
        res = super(property_type_division_required, self).fields_get(cr, uid, fields, context)
        res['category_pro']['required'] = True
        return res



class property_sorting_division_required(models.Model):
    _inherit = 'mt.property.sorting'

    def fields_get(self, cr, uid, fields=None, context=None, write_access=True):
        res = super(property_sorting_division_required, self).fields_get(cr, uid, fields, context)
        res['category_id']['required'] = True
        return res


