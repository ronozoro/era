# -*- coding: utf-8 -*-
import datetime
from mako.runtime import _inherit_from

from openerp.addons.base_geolocalize.models.res_partner import geo_find, geo_query_address
from openerp.osv import fields
from openerp.osv import osv
from openerp.tools.translate import _


class marketing_campaign(osv.osv):
    _inherit = 'marketing.campaign'
    
    

    _columns = {
                'object_id': fields.many2one('ir.model', 'Resource',
                                      help="Choose the resource on which you want \
this campaign to be run"),
                'campaign_type_id': fields.many2one('mt.campaign_type', 'Campaign type'),
                'states': fields.selection([('planning', 'Planning'),
                                   ('active', ' Active'),
                                   ('completed', 'Completed'),
                                   ('canceled', 'Canceled')], "State"),
                'created_time': fields.datetime("Created Time",readonly=True),
                'launch_time': fields.datetime("Launch Time"),
                'end_date': fields.date('End Date'),
                'property_id': fields.many2one('product.template', string="Proprety"),
                'project_id': fields.many2one('product.template', string="Project"),
                'developer_id': fields.many2one('product.template', string="Developer"), 
#                 'fixed_cost': fields.char('Fixed Cost'),
                'expected_revenue': fields.char('Expected Revenue'),
    }
    
    _defaults = {
                 'created_time': datetime.date.today(),

    }
    
marketing_campaign()


#-------------------------------------------------------------------------------
# 1. campaign type
#-------------------------------------------------------------------------------
class campaign_type(osv.osv):
    _name = "mt.campaign_type"

    _columns = {
        'name': fields.char("Name", required=True),
    }



#-------------------------------------------------------------------------------
# marketing_campaign_activity
#-------------------------------------------------------------------------------
class marketing_campaign_activity(osv.osv):
    _inherit = 'marketing.campaign.activity'
    
    

    _columns = {
                
                'campaign_type_id': fields.many2one('mt.campaign_type', 'Type'),
                'start_date': fields.date('Start Date'),
                'end_date': fields.date('End Date'),
                'activity_type': fields.char('Activity Type'),
    }
    
    _defaults = {

    }
    
marketing_campaign_activity()


#-------------------------------------------------------------------------------
# marketing_campaign_segment
#-------------------------------------------------------------------------------
class marketing_campaign_segment(osv.osv):
    _inherit = 'marketing.campaign.segment'
    
    def on_change_campaign_id(self, cr, uid, ids, campaign_id):
        campaign = self.pool.get('marketing.campaign').browse(cr, uid, campaign_id)
        date_launch = campaign.launch_time
        return {'value': {'launch_time': date_launch, }}
    
    def on_change_target_segment_id(self, cr, uid, ids, target_segment_id):
        target = self.pool.get('mt.target_segment').browse(cr, uid, target_segment_id)
        name_target = target.name
        return {'value': {'name': name_target, }}

    _columns = {
                
                'target_segment_id': fields.many2one('mt.target_segment', 'Target Segment'),
                'currency_id': fields.many2one('mt.currency', 'Currency'),
                'launch_time': fields.datetime("Launch Time"),
                'actual_reach': fields.char('Actual Reach'),
                'target_reach': fields.char('Target Reach'),
                'sponsor_ship': fields.char('Sponsor Ship'),
    }
    
    _defaults = {

    }
    
marketing_campaign_segment()



#-------------------------------------------------------------------------------
# Target Segment
#-------------------------------------------------------------------------------
class target_segment(osv.osv):
    _name = "mt.target_segment"

    _columns = {
        'name': fields.char("Name", required=True),
    }
    
#-------------------------------------------------------------------------------
# Currency
#-------------------------------------------------------------------------------
class currency(osv.osv):
    _name = "mt.currency"

    _columns = {
        'name': fields.char("Name", required=True),
    }
