# -*- coding: utf-8 -*-
import datetime
from mako.runtime import _inherit_from

from openerp.addons.base_geolocalize.models.res_partner import geo_find, geo_query_address
from openerp.osv import fields
from openerp.osv import osv
from openerp.tools.translate import _
from dateutil.relativedelta import relativedelta


# def calendar_id2real_id(calendar_id=None, with_date=False):
#         """
#         Convert a "virtual/recurring event id" (type string) into a real event id (type int).
#         E.g. virtual/recurring event id is 4-20091201100000, so it will return 4.
#         @param calendar_id: id of calendar
#         @param with_date: if a value is passed to this param it will return dates based on value of withdate + calendar_id
#         @return: real event id
#         """
#         if calendar_id and isinstance(calendar_id, (basestring)):
#             res = calendar_id.split('-')
#             if len(res) >= 2:
#                 real_id = res[0]
#                 if with_date:
#                     real_date = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT, time.strptime(res[1], "%Y%m%d%H%M%S"))
#                     start = datetime.strptime(real_date, DEFAULT_SERVER_DATETIME_FORMAT)
#                     end = start + timedelta(hours=with_date)
#                     return (int(real_id), real_date, end.strftime(DEFAULT_SERVER_DATETIME_FORMAT))
#                 return int(real_id)
#         return calendar_id and int(calendar_id) or calendar_id
#     
class crm_claim(osv.osv):
    _inherit = 'crm.claim'
    
    def create(self, cr, uid, vals, context=None):
        #create notif
        if vals.get('user_id', False) :
                user_ids=[]
                alarm_ids=[]
                event_obj = self.pool.get('calendar.event')
                models_data = self.pool.get('ir.model.data')
                alarm_notif_id = models_data.get_object_reference(cr,uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
                manager_id = vals['user_id']
                manager = self.pool.get('res.users').browse(cr, uid, manager_id, context=context)
                manager_karim = self.pool.get('res.users').search(cr, uid, [('login', 'ilike', 'karim.fahim@era-egypt.com')], context=context)
                manager_karim_id = self.pool.get('res.users').browse(cr, uid, manager_karim[0], context=context)
                user_ids.append(manager_karim_id.partner_id.id)
                user_ids.append(manager.partner_id.id)
                alarm_ids.append(alarm_notif_id)
                date_alert = datetime.datetime.now()+relativedelta(minutes=+ 1)
                date_fin = datetime.datetime.now()+relativedelta(minutes=+ 20)
                event_values = {
                                    'name' :"there is a claim was assgned to you ",
                                    'start_datetime':date_alert.strftime('%Y-%m-%d %H:%M:%S') ,
                                    'stop_datetime':date_fin.strftime('%Y-%m-%d %H:%M:%S') ,
                                    'alarm_ids':[(6,0, alarm_ids)],
                                    'partner_ids':[(6,0,user_ids)],
                                    'is_notif':True,
                                    }
                event_id = event_obj.create(cr, uid,event_values, context=context )
        id = super(crm_claim, self).create(cr, uid, vals, context=context)
        return id
    
    def action_count(self, cr, uid, ids, field_name, arg, context=None):
        result = dict.fromkeys(ids, 0)
        lead = self.pool.get('crm.claim').browse(
            cr, uid, ids, context=context).partner_phone
        cr.execute(
            "select count (*) from crm_lead where phone ='%s'" % str(lead))
        value = cr.fetchone()[0] or 0
        result[ids[0]] = value
        return result

    _columns = {
        'name_claim':fields.char('Name'),
        'action_count': fields.function(action_count, type='integer'),
        'category': fields.many2one('mt.case.categ', 'Category List' ),
        'class': fields.selection([('public', 'Public'), ('private', 'Private'), ('confidential', 'Public for Employees')], 'Privacy'),
    }
    
    
        
#     def read(self, cr, uid, ids, fields=None, context=None, load='_classic_read'):
#         if context is None:
#             context = {}
#         fields2 = fields and fields[:] or None
#         EXTRAFIELDS = ('class', 'user_id', 'date')
#         for f in EXTRAFIELDS:
#             if fields and (f not in fields):
#                 fields2.append(f)
#         if isinstance(ids, (basestring, int, long)):
#             select = [ids]
#         else:
#             select = ids
#         select = map(lambda x: (x, calendar_id2real_id(x)), select)
#         result = []
#         real_data = super(crm_claim, self).read(cr, uid, [real_id for calendar_id, real_id in select], fields=fields2, context=context, load=load)
#         real_data = dict(zip([x['id'] for x in real_data], real_data))
# 
#         for calendar_id, real_id in select:
#             res = real_data[real_id].copy()
#             
#             res['id'] = calendar_id
#             result.append(res)
# 
#         for r in result:
# #             if r['user_id']:
# #                 user_id = type(r['user_id']) in (tuple, list) and r['user_id'][0] or r['user_id']
# #                 if user_id == uid:
# #                     continue
#             if r['class'] == 'private':
#                 for f in r.keys():
#                     if f not in ('id', 'allday', 'start', 'stop', 'duration', 'user_id', 'state', 'interval', 'count', 'recurrent_id_date', 'rrule'):
#                         if isinstance(r[f], list):
#                             r[f] = []
#                         else:
#                             r[f] = False
#                     if f == 'name':
#                         r[f] = _('Busy')
# 
#         for r in result:
#             for k in EXTRAFIELDS:
#                 if (k in r) and (fields and (k not in fields)):
#                     del r[k]
#         if isinstance(ids, (basestring, int, long)):
#             return result and result[0] or False
#         return result
#     
crm_claim()

