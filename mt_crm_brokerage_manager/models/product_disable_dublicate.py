# -*- coding: utf-8 -*-
from openerp import models, api
from openerp.exceptions import Warning
from openerp.tools.translate import _


class ProductTemplateDuplicate(models.Model):
    _inherit = 'product.template'

    @api.multi
    def copy(self,default=None):
        if self.env.user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            raise Warning(_('You do not have access to duplicate this record .. please create new one'))
        return super(ProductTemplateDuplicate, self).copy(default)


class ClientPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def unlink(self):
        if self.env.user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            raise Warning(_('You do not have access to delete this record ..  please contact admin.'))
        return super(ClientPartner, self).unlink()


ClientPartner()


class ClientActivity(models.Model):
    _inherit = 'calendar.event'

    @api.multi
    def unlink(self):
        if self.env.user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            raise Warning(_('You do not have access to delete this record .. please contact admin.'))
        return super(ClientActivity, self).unlink()


ClientActivity()
