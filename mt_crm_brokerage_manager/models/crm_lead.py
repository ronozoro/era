# -*- coding: utf-8 -*-
import datetime
from mako.runtime import _inherit_from

from openerp.addons.base_geolocalize.models.res_partner import geo_find, geo_query_address
from openerp.osv import fields
from openerp import api
from openerp.osv import osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from dateutil.relativedelta import relativedelta
from openerp import SUPERUSER_ID
from openerp.osv.orm import setup_modifiers
from lxml import etree
import webbrowser


class base_action_rule(osv.osv):
    _inherit = 'base.action.rule'

    def on_change_sale_team(self, cr, uid, ids, sales_team_id, context=None):
        res = {}
        list = []
        sales_team = self.pool.get('crm.case.section').browse(cr, uid, sales_team_id)
        for agent in sales_team.member_ids:
            list.append((6, 0, agent.id))
            res = {'value': {'act_followers': list}}
        return res

    _columns = {
        'sales_team_id': fields.many2one('crm.case.section', string="Sales Teams"),
    }


base_action_rule()


class mt_request_stage(osv.osv):
    _inherit = 'crm.case.stage'

    _columns = {
        'code': fields.char(string="Code"),
        'active': fields.boolean('Active'),
    }


mt_request_stage()


class res_users(osv.osv):
    _inherit = 'res.users'

    _columns = {
        'sale_team_id': fields.many2one('crm.case.section', 'Sale Team'),
    }


res_users()


class crm_case_section(osv.osv):
    _inherit = 'crm.case.section'

    def create(self, cr, uid, vals, context=None):
        id = super(crm_case_section, self).create(cr, uid, vals, context=context)
        if vals.get('user_id', False):
            team_leader_id = vals.get('user_id', False)
            self.pool.get('res.users').write(cr, uid, team_leader_id, {'sale_team_id': id}, context=context)
        return id

    def write(self, cr, uid, ids, vals, context=None):
        if vals.get('user_id', False):
            team_leader_id = vals.get('user_id', False)
            self.pool.get('res.users').write(cr, uid, team_leader_id, {'sale_team_id': ids[0]}, context=context)

        return super(crm_case_section, self).write(cr, uid, ids, vals, context=context)


class res_partner(osv.osv):
    _inherit = "res.partner"

    _columns = {
        'phone': fields.char('Phone', copy=False),
        'mobile': fields.char('Mobile', copy=False),
    }



    def create(self, cr, uid, vals, context=None):
        values = {}
        mod_obj = self.pool.get('ir.model.data')
        if vals.get('phone', False):
            phone = vals.get('phone', False)
            teams = vals.get('section_id', False)
            user = vals.get('user_id', False)
            if teams:
                # fake_user = mod_obj.get_object_reference(cr, SUPERUSER_ID, 'mt_crm_brokerage_manager', 'fake_user')
                lead_ids = self.pool.get('res.partner').search(cr, uid,
                                                               [('phone', '=', phone), ('section_id', '=', teams),
                                                                ('user_id', '!=', user)], context=context)
                if lead_ids and len(lead_ids) > 0:
                    old_lead = self.pool.get('res.partner').browse(cr, uid, lead_ids[0], context)
                    raise osv.except_osv(_(u'Warning'),
                                         _(u'you have a request %s assigned  to %s. in the same departement') % (
                                             old_lead.name, old_lead.user_id.name))

            if len(phone) != 11:
                raise osv.except_osv(_('Warning!'), _("Please you must enter 11 digits in phone case !"))
            partner_ids = self.search(cr, uid, ['|', ('phone', '=', phone), ('mobile', '=', phone)], context=context)
            if partner_ids:
                partner = self.browse(cr, uid, partner_ids[0], context=context).user_id.name
                raise osv.except_osv(_('Warning!'), _("this mobile 1 number is assigned to %s") % partner)
        if vals.get('mobile', False):
            mobile = vals.get('mobile', False)
            partner_ids = self.search(cr, uid, ['|', ('phone', '=', mobile), ('mobile', '=', mobile)], context=context)
            if partner_ids:
                partner = self.browse(cr, uid, partner_ids[0], context=context).user_id.name
                raise osv.except_osv(_('Warning!'), _("this mobile 2 number is assigned to %s") % partner)
        id = super(res_partner, self).create(cr, uid, vals, context=context)
        return id

    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}
        if vals.get('phone', False):
            data = vals.get('phone', False)
            if len(data) != 11:
                raise osv.except_osv(_('Warning!'), _("Please you must enter 11 digits in phone case !"))
            partner_ids = self.search(cr, uid, ['|', ('phone', '=', data), ('mobile', '=', data)], context=context)
            if partner_ids:
                partner = self.browse(cr, uid, partner_ids[0], context=context).name
                raise osv.except_osv(_('Warning!'), _("this mobile 1 number is assigned to %s") % partner)
        if vals.get('mobile', False):
            mobile = vals.get('mobile', False)
            partner_ids = self.search(cr, uid, ['|', ('phone', '=', mobile), ('mobile', '=', mobile)], context=context)
            if partner_ids:
                partner = self.browse(cr, uid, partner_ids[0], context=context).name
                raise osv.except_osv(_('Warning!'), _("this mobile 2 number is assigned to %s") % partner)

        return super(res_partner, self).write(cr, uid, ids, vals, context=context)

    _columns = {
        'name_arabic': fields.char("Name (In Arabic)", size=64),
        'work': fields.char("Work", size=64),
        'client_type_id': fields.many2one('mt.client.type', 'Client Type'),
    }

    _defaults = {
        'user_id': lambda self, cr, uid, context: uid,

    }


#     _sql_constraints = [
#         ('uniq_phone', 'unique(phone)',
#          "A phone 1 already exists with this name . phone's number must be unique!"),
#         ('uniq_mobile', 'unique(mobile)',
#          "A mobile 1 already exists with this name . mobile's number must be unique!"),
#     ]

res_partner()


class crm_lead(osv.osv):
    _inherit = "crm.lead"
    _order = "create_date desc"

    def action_count(self, cr, uid, ids, field_name, arg, context=None):
        result = dict.fromkeys(ids, 0)
        lead = self.pool.get('crm.lead').browse(
            cr, uid, ids, context=context).phone
        cr.execute(
            "select count (*) from crm_lead where type='opportunity' and phone ='%s'" % str(lead))
        value = cr.fetchone()[0] or 0
        result[ids[0]] = value - 1
        return result

    def action_count_lead(self, cr, uid, ids, field_name, arg, context=None):
        result = dict.fromkeys(ids, 0)
        lead = self.pool.get('crm.lead').browse(
            cr, uid, ids, context=context).phone
        cr.execute(
            "select count (*) from crm_lead where type='lead' and phone ='%s'" % str(lead))
        value = cr.fetchone()[0] or 0
        result[ids[0]] = value - 1
        return result

    #     def on_change_nom(self, cr, uid, ids, service_ids,category):
    #         res = {}
    #         if service_ids in ('sale', 'rent', 'share'):
    #             return {'value': {'request_type': 'seller',
    #                               }}
    #         else:
    # #             categ = self.pool.get('mt.case.categ').browse(cr, uid, category)
    # #             obj2 = self.pool.get('ir.model.data').get_object_reference(
    # #             cr, uid, 'mt_crm_brokerage_manager', 'categ_oppor20')
    # #             sale_team = self.pool.get('ir.model.data').get_object_reference(
    # #             cr, uid, 'mt_crm_brokerage_manager', 'crm_case_section7')
    # #             if categ and obj2 and categ.id == obj2[1]:
    #                 if service_ids == 'buy':
    #                     return {'value': {'request_type': 'buyer',
    #                                       'section_id': sale_team[1]
    #                                       }}
    #         return res

    def on_change_section_id(self, cr, uid, ids, section_id, is_buyer=None,is_client=None):
        curre_user = self.pool.get('res.users').browse(cr, uid, uid)
        res = {}
        users = []
        users2 = []
        pars = []
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", curre_user,"is_client",is_client

        mod_obj = self.pool.get('crm.lead').browse(cr, uid, ids)
        if section_id:
            print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
            sale_team = self.pool.get('crm.case.section').browse(cr, uid, section_id)
            users_ids = sale_team.member_ids.ids
            if curre_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                users.append(curre_user.id)

            if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
                users.append(curre_user.id)

            if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
                users.append(curre_user.id)

            for user in sale_team.member_ids:
                if is_buyer is not None:
                    if is_buyer == 1:
                        # print "1111111111111111111111111111111111111"
                        if curre_user.id != sale_team.user_id.id and curre_user.has_group(
                                'mt_crm_brokerage_manager.group_residential_buyer_agent_id') or curre_user.has_group(
                            'mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
                            print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ,buyer"
                            print "MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM ,buyer"
                            users.append(user.id)
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', '=', uid)])

                        if curre_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                            if user.has_group(
                                    'mt_crm_brokerage_manager.group_commercial_buyer_agent_id') and curre_user.has_group(
                                'mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
                                print
                                "33333333333333333333"
                                # break
                                users.append(user.id)
                            elif user.has_group(
                                    'mt_crm_brokerage_manager.group_residential_buyer_agent_id') and curre_user.has_group(
                                'mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):

                                # break
                                users.append(user.id)
                            print
                            "GGGGGGGGGGGGGGGGGGGGGggggg", users
                            # for rec in section_id.member_ids:
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])
                            print
                            "VVVVVVVVVVVVVVVVVVVVVVVVVVVV", pars

                        if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
                            if user.has_group('mt_crm_brokerage_manager.group_commercial_buyer_agent_id') and curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
                                users.append(user.id)
                                # print "GGGGGGGGGGGGGGGGGGGGGggggg", users
                            elif user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                                users.append(user.id)
                            print "GGGGGGGGGGGGGGGGGGGGGggggg", users
                            # for rec in section_id.member_ids:
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])
                            print "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN", pars
                            print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^66", mod_obj.is_client

                        if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
                            if user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id') and curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
                                users.append(user.id)
                                # print "GGGGGGGGGGGGGGGGGGGGGggggg", users
                            elif user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                                users.append(user.id)
                            print "GGGGGGGGGGGGGGGGGGGGGggggg", users

                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])
                            print "VVVVVVVVVVVVVVVVVVVVVVVVVVVV", pars
                            print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^66", mod_obj.is_client

                        if curre_user.has_group('base.group_system'):
                            if user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id') and curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
                                users.append(user.id)
                                # print "GGGGGGGGGGGGGGGGGGGGGggggg", users
                            elif user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                                users.append(user.id)
                            print "GGGGGGGGGGGGGGGGGGGGGggggg", users

                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])
                            print "VVVVVVVVVVVVVVVVVVVVVVVVVVVV", pars
                            print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^66", mod_obj.is_client

                    else:
                        if curre_user.id != sale_team.user_id.id and curre_user.has_group(
                                'mt_crm_brokerage_manager.group_residential_seller_agent_id') or curre_user.has_group(
                            'mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
                            # print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ,seller"
                            users.append(user.id)
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', '=', uid)])
                        if curre_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                            if user.has_group(
                                    'mt_crm_brokerage_manager.group_commercial_seller_agent_id') and curre_user.has_group(
                                'mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
                                print "33333333333333333333"
                                # break
                                users.append(user.id)
                            elif user.has_group(
                                    'mt_crm_brokerage_manager.group_residential_seller_agent_id') and curre_user.has_group(
                                'mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
                                print
                                "33333333333333333333"
                                # break
                                users.append(user.id)

                            print "OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO", users
                            # for rec in section_id.member_ids:
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])
                            print "999999999999999999999", pars

                        if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
                            if user.has_group('mt_crm_brokerage_manager.group_commercial_seller_agent_id') and curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
                                users.append(user.id)
                            elif user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                                users.append(user.id)
                            print "GGGGGGGGGGGGGGGGGGGGGggggg", users

                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])
                            print "VVVVVVVVVVVVVVVVVVVVVVVVVVVV", pars
                            print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^66", mod_obj.is_client

                        if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
                            if user.has_group('mt_crm_brokerage_manager.group_residential_seller_agent_id') and curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
                                users.append(user.id)
                                # print "GGGGGGGGGGGGGGGGGGGGGggggg", users
                            elif user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                                users.append(user.id)
                            print "GGGGGGGGGGGGGGGGGGGGGggggg", users
                            # for rec in section_id.member_ids:
                            print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^66",mod_obj.is_client
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])
                            print "VVVVVVVVVVVVVVVVVVVVVVVVVVVV", pars


            manager_id = sale_team.user_id.id
            res['value'] = {'manager_id': manager_id}
        if curre_user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            if is_client == 'client':
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('user_id', '=', uid)]}
            if is_client == 'org':
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', True),('user_id', '=', uid)]}
        if curre_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            if is_client == 'client':
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('user_id', 'in', curre_user.sale_team_id.member_ids.ids)]}
            if is_client == 'org':
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', True),
                                                ('user_id', 'in', curre_user.sale_team_id.member_ids.ids)]}


        if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
            if is_client =='client':
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', False),('customer', '=', True),
                                                ('id', 'in', pars)]}
            if is_client =='org':
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', True),
                                                ('id', 'in', pars)]}
        if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
            if is_client =='client':
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', False),('customer', '=', True),
                                                ('id', 'in', pars)]}
            if is_client =='org':
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', True),
                                                ('id', 'in', pars)]}

        if curre_user.has_group('base.group_system'):
            lst1 =[]
            sales_team = self.pool.get('crm.case.section').search(cr, uid, [])
            for rec in self.pool.get('crm.case.section').browse(cr, uid, sales_team):
                for user in rec.member_ids:
                    lst1.append(user.id)

            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', lst1)])

            if is_client == 'client':
                res['domain'] = {'user_id': [('id', 'in', lst1)],'origin_id': [('id', 'in', lst1)],
                                 'assign': [('id', 'in', lst1)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('id', 'in', pars)]}
            if is_client == 'org':
                res['domain'] = {'user_id': [('id', 'in', lst1)],'origin_id': [('id', 'in', lst1)],
                                 'assign': [('id', 'in', lst1)],
                                 'partner_id': [('is_company', '=', True),
                                                ('id', 'in', pars)]}

        if curre_user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
            lst1 =[]
            sales_team = self.pool.get('crm.case.section').browse(cr, uid, section_id)
            for rec in sales_team:
                for user in rec.member_ids:
                    lst1.append(user.id)

            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', lst1)])

            if is_client == 'client':
                res['domain'] = {'user_id': [('id', 'in', lst1)],'origin_id': [('id', 'in', lst1)],
                                 'assign': [('id', 'in', lst1)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('id', 'in', pars)]}
            if is_client == 'org':
                res['domain'] = {'user_id': [('id', 'in', lst1)],'origin_id': [('id', 'in', lst1)],
                                 'assign': [('id', 'in', lst1)],
                                 'partner_id': [('is_company', '=', True),
                                                ('id', 'in', pars)]}
        sale_team = self.pool.get('crm.case.section').browse(cr, uid, section_id)
        if sale_team:
            if res['domain']:
                  res['domain'].update({'manager_id': [('id', 'in', [sale_team.user_id.id])]})
            else:
                res['domain']={'manager_id': [('id', 'in', [sale_team.user_id.id])]}
        return res





    def on_change_section(self, cr, uid, ids, section_id, service_ids, context=None):
        curre_user = self.pool.get('res.users').browse(cr, uid, uid)
        res = {}
        users = []
        users2 = []
        pars = []
        users_ids = []
        if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
            users.append(curre_user.id)

        if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
            users.append(curre_user.id)

        if context.get('service_ids', False):
            context.update({'default_order': self.service_ids})
        print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$4"
        print section_id, service_ids, context
        if section_id and service_ids:
            # print "saaaaaaaaaa3id "
            sale_team = self.pool.get('crm.case.section').browse(cr, uid, section_id)
            # leads = self.pool.get('crm.lead').search(cr, uid, [('section_id','=',section_id)])
            # leads2 = self.pool.get('crm.lead').browse(cr, uid, ids)
            # print leads
            print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%",
            if curre_user.has_group('base.group_system'):
                users_ids = sale_team.member_ids.ids
                pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users_ids)])

            if curre_user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
                users_ids = sale_team.member_ids.ids
                pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users_ids)])


            for user in sale_team.member_ids:

                if service_ids in ('buyer_rent', 'share_buyer', 'buy'):
                    print "1111111111111111111111111111111111111"
                    if user.has_group(
                            'mt_crm_brokerage_manager.group_residential_buyer_agent_id') or user.has_group(
                        'mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
                        print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ,buyer"
                        users.append(user.id)
                        pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                    if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id') and service_ids in (
                            'buyer_rent', 'share_buyer', 'buy'):
                        # users = sale_team.member_ids.ids
                        users.append(user.id)
                        pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                    if user.has_group('itsys_era_coordinator.group_coordinator_commercial') and service_ids in (
                            'buyer_rent', 'share_buyer', 'buy'):
                        # users = sale_team.member_ids.ids
                        users.append(user.id)
                        pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                    if user.has_group('itsys_era_coordinator.group_coordinator_resedential') and service_ids in (
                            'buyer_rent', 'share_buyer', 'buy'):
                        # users = sale_team.member_ids.ids
                        users.append(user.id)
                        pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                    # if user.has_group('mt_crm_brokerage_manager.group_ceo_id') and service_ids in (
                    #         'buyer_rent', 'share_buyer', 'buy'):
                    #
                    #     # users = sale_team.member_ids.ids
                    #     users.append(user.id)
                    #     pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                elif service_ids in ['seller_rent', 'share_seller', 'sale']:
                    if user.has_group(
                            'mt_crm_brokerage_manager.group_residential_seller_agent_id') or user.has_group(
                        'mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
                        print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ,seller"
                        users.append(user.id)
                        pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                    if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id') and service_ids in [
                        'seller_rent', 'share_seller', 'sale']:
                        # for each in sale_team.member_ids:
                        #     if service_ids in ['seller_rent', 'share_seller', 'sale']:
                        users.append(user.id)
                        # users = sale_team.member_ids.ids
                        pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                    if user.has_group('itsys_era_coordinator.group_coordinator_commercial') and service_ids in [
                        'seller_rent', 'share_seller', 'sale']:
                        # users = sale_team.member_ids.ids
                        users.append(user.id)
                        pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                    if user.has_group('itsys_era_coordinator.group_coordinator_resedential') and service_ids in [
                        'seller_rent', 'share_seller', 'sale']:
                        # users = sale_team.member_ids.ids
                        users.append(user.id)
                        pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                    # if user.has_group('mt_crm_brokerage_manager.group_ceo_id') and service_ids in [
                    #     'seller_rent', 'share_seller', 'sale']:
                    #     # users = sale_team.member_ids.ids
                    #     users.append(user.id)
                    #
                    #     pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

            manager_id = sale_team.user_id.id
            res['value'] = {'manager_id': manager_id}
            if curre_user.has_group('mt_crm_brokerage_manager.group_agent_id'):
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('user_id', '=', uid)]}
            if curre_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('user_id', 'in', curre_user.sale_team_id.member_ids.ids)]}

            if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('id', 'in', pars)]}

            if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('id', 'in', pars)]}
            if curre_user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
                res['domain'] = {'user_id': [('id', 'in', users_ids)],'origin_id': [('id', 'in', users_ids)],
                                 'assign': [('id', 'in', users_ids)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('id', 'in', pars)]}

            if curre_user.has_group('base.group_system'):
                res['domain'] = {'user_id': [('id', 'in', users_ids)],'origin_id': [('id', 'in', users_ids)],
                                 'assign': [('id', 'in', users_ids)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('id', 'in', pars)]}
        else:
            sale_team = self.pool.get('crm.case.section').browse(cr, uid, section_id)
            user_record_set = self.pool.get('res.users').browse(cr, uid, uid)
            users_ids = sale_team.member_ids.ids

            if user_record_set.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users_ids)])

            if user_record_set.has_group('itsys_era_coordinator.group_coordinator_resedential'):
                pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users_ids)])

            if user_record_set.has_group('itsys_era_coordinator.group_coordinator_commercial'):
                pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users_ids)])

            if user_record_set.has_group('base.group_system'):
                # users_ids = sale_team.member_ids.ids
                pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users_ids)])



            elif user_record_set.id != sale_team.user_id.id and (user_record_set.has_group(
                    'mt_crm_brokerage_manager.group_residential_buyer_agent_id') or user_record_set.has_group(
                'mt_crm_brokerage_manager.group_commercial_buyer_agent_id')) or (user_record_set.has_group(
                'mt_crm_brokerage_manager.group_residential_seller_agent_id') or user_record_set.has_group(
                'mt_crm_brokerage_manager.group_commercial_seller_agent_id')):
                pars = self.pool.get('res.partner').search(cr, uid,
                                                           [('user_id', '=', uid)])

            if curre_user.has_group('mt_crm_brokerage_manager.group_agent_id'):
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('user_id', '=', uid)]}
            if curre_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('user_id', 'in', curre_user.sale_team_id.member_ids.ids)]}

            if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('user_id', 'in', pars)]}

            if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
                res['domain'] = {'user_id': [('id', 'in', users)],'origin_id': [('id', 'in', users)],
                                 'assign': [('id', 'in', users)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('user_id', 'in', pars)]}

            if curre_user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
                res['domain'] = {'user_id': [('id', 'in', users_ids)], 'origin_id': [('id', 'in', users_ids)],
                                 'assign': [('id', 'in', users_ids)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('id', 'in', pars)]}

            if curre_user.has_group('base.group_system'):
                res['domain'] = {'user_id': [('id', 'in', users_ids)], 'origin_id': [('id', 'in', users_ids)],
                                 'assign': [('id', 'in', users_ids)],
                                 'partner_id': [('is_company', '=', False), ('customer', '=', True),
                                                ('id', 'in', pars)]}



        return res

    def on_change_service_ids(self, cr, uid, ids, service_ids, section_id, context=None):
        res = {}
        users = []
        # print "RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRrrr"
        print  service_ids
        if service_ids and section_id:
            # print "saaaaaaaaaa3id "
            sale_team = self.pool.get('crm.case.section').browse(cr, uid, section_id)
            # leads = self.pool.get('crm.lead').search(cr, uid, [('section_id','=',section_id)])
            leads2 = self.pool.get('crm.lead').browse(cr, uid, ids)
            # print leads
            # print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%", leads2, leads2.id
            users_ids = sale_team.member_ids.ids
            for user in sale_team.member_ids:

                if service_ids in ('buyer_rent', 'share_buyer', 'buy'):
                    # print "1111111111111111111111111111111111111"
                    if user != sale_team.user_id.id and user.has_group(
                            'mt_crm_brokerage_manager.group_residential_buyer_agent_id') or user.has_group(
                        'mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
                        # print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ,buyer"
                        users.append(user.id)
                elif service_ids in ['seller_rent', 'share_seller', 'sale']:
                    if user != sale_team.user_id.id and user.has_group(
                            'mt_crm_brokerage_manager.group_residential_seller_agent_id') or user.has_group(
                        'mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
                        # print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ,seller"
                        users.append(user.id)

                # if user != sale_team.user_id.id:
                #     users.append(user)

            res['domain'] = {'user_id': [('id', 'in', users)]}
        else:
            res['domain'] = {'user_id': [('id', 'in', users)]}
        return res

    def on_change_user_id(self, cr, uid, ids, user_id):

        curre_user = self.pool.get('res.users').browse(cr, uid, uid)
        res = {}
        users = []
        users2 = []
        pars = []
        print
        ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", curre_user

        mod_obj = self.pool.get('crm.lead').browse(cr, uid, ids)
        if mod_obj.section_id:
            print
            ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
            sale_team = self.pool.get('crm.case.section').browse(cr, uid, mod_obj.section_id.id)
            users_ids = sale_team.member_ids.ids
            if curre_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                users.append(curre_user.id)
            for user in sale_team.member_ids:
                if mod_obj.is_buyer is not None:
                    if mod_obj.is_buyer == 1:
                        # print "1111111111111111111111111111111111111"
                        if curre_user.id != sale_team.user_id.id and curre_user.has_group(
                                'mt_crm_brokerage_manager.group_residential_buyer_agent_id') or curre_user.has_group(
                            'mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
                            print
                            "$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ,buyer"
                            print
                            "MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM ,buyer"
                            users.append(user.id)
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', '=', uid)])

                        if curre_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                            if user.has_group(
                                    'mt_crm_brokerage_manager.group_commercial_buyer_agent_id') and curre_user.has_group(
                                'mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
                                print
                                "33333333333333333333"
                                # break
                                users.append(user.id)
                            elif user.has_group(
                                    'mt_crm_brokerage_manager.group_residential_buyer_agent_id') and curre_user.has_group(
                                'mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
                                print
                                "33333333333333333333"
                                # break
                                users.append(user.id)
                            print
                            "GGGGGGGGGGGGGGGGGGGGGggggg", users
                            # for rec in section_id.member_ids:
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                        if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
                            # if user.has_group(
                            #         'mt_crm_brokerage_manager.group_commercial_buyer_agent_id') and curre_user.has_group(
                            #     'mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
                            #     print
                            #     "33333333333333333333"
                            #     # break
                            users.append(user.id)
                            # elif user.has_group(
                            #         'mt_crm_brokerage_manager.group_residential_buyer_agent_id') and curre_user.has_group(
                            #     'mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
                            #     print
                            #     "33333333333333333333"
                            #     # break
                            #     users.append(user.id)
                            # print "GGGGGGGGGGGGGGGGGGGGGggggg", users
                            # for rec in section_id.member_ids:
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                        if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
                            # if user.has_group(
                            #         'mt_crm_brokerage_manager.group_commercial_buyer_agent_id') and curre_user.has_group(
                            #     'mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
                            #     print
                            #     "33333333333333333333"
                            #     # break
                            users.append(user.id)
                            # elif user.has_group(
                            #         'mt_crm_brokerage_manager.group_residential_buyer_agent_id') and curre_user.has_group(
                            #     'mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
                            #     print
                            #     "33333333333333333333"
                            #     # break
                            #     users.append(user.id)
                            # print "GGGGGGGGGGGGGGGGGGGGGggggg", users
                            # for rec in section_id.member_ids:
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])


                    else:
                        if curre_user.id != sale_team.user_id.id and curre_user.has_group(
                                'mt_crm_brokerage_manager.group_residential_seller_agent_id') or curre_user.has_group(
                            'mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
                            # print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ,seller"
                            users.append(user.id)
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', '=', uid)])
                        if curre_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                            if user.has_group(
                                    'mt_crm_brokerage_manager.group_commercial_seller_agent_id') and curre_user.has_group(
                                'mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
                                print
                                "33333333333333333333"
                                # break
                                users.append(user.id)
                            elif user.has_group(
                                    'mt_crm_brokerage_manager.group_residential_seller_agent_id') and curre_user.has_group(
                                'mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
                                print
                                "33333333333333333333"
                                # break
                                users.append(user.id)

                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                        if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
                            # if user.has_group(
                            #         'mt_crm_brokerage_manager.group_commercial_buyer_agent_id') and curre_user.has_group(
                            #     'mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
                            #     print
                            #     "33333333333333333333"
                            #     # break
                            users.append(user.id)
                            # elif user.has_group(
                            #         'mt_crm_brokerage_manager.group_residential_buyer_agent_id') and curre_user.has_group(
                            #     'mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
                            #     print
                            #     "33333333333333333333"
                            #     # break
                            #     users.append(user.id)
                            # print "GGGGGGGGGGGGGGGGGGGGGggggg", users
                            # # for rec in section_id.member_ids:
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

                        if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
                            # if user.has_group(
                            #         'mt_crm_brokerage_manager.group_commercial_buyer_agent_id') and curre_user.has_group(
                            #     'mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
                            #     print
                            #     "33333333333333333333"
                            #     # break
                            users.append(user.id)
                            # elif user.has_group(
                            #         'mt_crm_brokerage_manager.group_residential_buyer_agent_id') and curre_user.has_group(
                            #     'mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
                            #     print
                            #     "33333333333333333333"
                            #     # break
                            #     users.append(user.id)
                            # print "GGGGGGGGGGGGGGGGGGGGGggggg", users
                            # # for rec in section_id.member_ids:
                            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', users)])

            manager_id = sale_team.user_id.id
            res['value'] = {'manager_id': manager_id}
        res['domain'] = {'user_id': [('id', 'in', users)],
                         'partner_id': [('id', 'in', pars), ('is_company', '=', False), ('customer', '=', True),
                                        ('user_id', '=', uid)]}

        if user_id not in users:
            raise osv.except_osv(_('Error!'), _('You cannot change to this user in assign to'))

        user2 = self.pool.get('res.users').browse(cr, uid, user_id)
        comp_id = user2.company_id.id
        # return {'value': {'company_id': comp_id, }}
        res['value'] = {'company_id': comp_id, }

        return res

    def cron_recurring_notification_new(self, cr, uid, context=None):
        return self._cron_recurring_notification_inprogress(cr, uid, [], automatic=True, context=context)

    def _cron_recurring_notification_new(self, cr, uid, ids, automatic=False, context=None):

        # create notif
        mod_obj = self.pool.get('ir.model.data')
        event_obj = self.pool.get('calendar.event')
        models_data = self.pool.get('ir.model.data')
        satge_ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead1')
        date_alert = datetime.datetime.now() - relativedelta(hours=+ 24)
        leads_ids = self.search(cr, uid, [('date_assign', '=', date_alert), ('stage_id', '=', satge_ref[1])],
                                context=context)
        for rec in self.browse(cr, uid, leads_ids, context):
            if rec.manager_id:
                user = 'user'
                if rec.user_id:
                    user = self.pool.get('res.users').browse(cr, uid, rec.user_id.id, context=context)
                user_ids = []
                alarm_ids = []

                alarm_notif_id = \
                    models_data.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
                manager = self.pool.get('res.users').browse(cr, uid, rec.manager_id.id, context=context)
                manager_karim = self.pool.get('res.users').search(cr, uid,
                                                                  [('login', 'ilike', 'karim.fahim@era-egypt.com')],
                                                                  context=context)
                manager_karim_id = self.pool.get('res.users').browse(cr, uid, manager_karim[0], context=context)
                user_ids.append(manager_karim_id.partner_id.id)
                user_ids.append(manager.partner_id.id)
                alarm_ids.append(alarm_notif_id)
                date_alert = datetime.datetime.now() + relativedelta(minutes=+ 1)
                date_fin = datetime.datetime.now() + relativedelta(minutes=+ 20)
                event_values = {
                    'name': "the request assigned to " + user.name + ' Not Change to assigned to for 24 Hours.',
                    'start_datetime': date_alert.strftime('%Y-%m-%d %H:%M:%S'),
                    'stop_datetime': date_fin.strftime('%Y-%m-%d %H:%M:%S'),
                    'alarm_ids': [(6, 0, alarm_ids)],
                    'partner_ids': [(6, 0, user_ids)],
                    'is_notif': True,
                }
                event_id = event_obj.create(cr, uid, event_values, context=context)

        return

    def cron_recurring_notification_assigned(self, cr, uid, context=None):
        return self._cron_recurring_notification_inprogress(cr, uid, [], automatic=True, context=context)

    def _cron_recurring_notification_assigned(self, cr, uid, ids, automatic=False, context=None):

        # create notif
        mod_obj = self.pool.get('ir.model.data')
        satge_ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead2')
        date_alert = datetime.datetime.now() - relativedelta(hours=+ 24)
        leads_ids = self.search(cr, uid, [('date_assign', '=', date_alert), ('stage_id', '=', satge_ref[1])],
                                context=context)
        for rec in self.browse(cr, uid, leads_ids, context):
            if rec.manager_id:
                user = 'user'
                if rec.user_id:
                    user = self.pool.get('res.users').browse(cr, uid, rec.user_id.id, context=context)
                user_ids = []
                alarm_ids = []
                event_obj = self.pool.get('calendar.event')
                models_data = self.pool.get('ir.model.data')
                alarm_notif_id = \
                    models_data.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
                manager = self.pool.get('res.users').browse(cr, uid, rec.manager_id.id, context=context)
                manager_karim = self.pool.get('res.users').search(cr, uid,
                                                                  [('login', 'ilike', 'karim.fahim@era-egypt.com')],
                                                                  context=context)
                manager_karim_id = self.pool.get('res.users').browse(cr, uid, manager_karim[0], context=context)
                user_ids.append(manager_karim_id.partner_id.id)
                user_ids.append(manager.partner_id.id)
                alarm_ids.append(alarm_notif_id)
                date_alert = datetime.datetime.now() + relativedelta(minutes=+ 1)
                date_fin = datetime.datetime.now() + relativedelta(minutes=+ 20)
                event_values = {
                    'name': "the request assigned to " + user.name + ' Not in progress from 24 hours ago',
                    'start_datetime': date_alert.strftime('%Y-%m-%d %H:%M:%S'),
                    'stop_datetime': date_fin.strftime('%Y-%m-%d %H:%M:%S'),
                    'alarm_ids': [(6, 0, alarm_ids)],
                    'partner_ids': [(6, 0, user_ids)],
                    'is_notif': True,
                }
                event_id = event_obj.create(cr, uid, event_values, context=context)

        return

    def cron_recurring_notification_inprogress(self, cr, uid, context=None):
        return self._cron_recurring_notification_assigned(cr, uid, [], automatic=True, context=context)

    def _cron_recurring_notification_inprogress(self, cr, uid, ids, automatic=False, context=None):

        # create notif
        mod_obj = self.pool.get('ir.model.data')
        satge_ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead3')
        date_alert = datetime.datetime.now() - relativedelta(hours=+ 168)
        leads_ids = self.search(cr, uid, [('date_inprogress', '=', date_alert), ('stage_id', '=', satge_ref[1])],
                                context=context)
        for rec in self.browse(cr, uid, leads_ids, context):
            if rec.manager_id:
                user = 'user'
                if rec.user_id:
                    user = self.pool.get('res.users').browse(cr, uid, rec.user_id.id, context=context)
                user_ids = []
                alarm_ids = []
                event_obj = self.pool.get('calendar.event')
                models_data = self.pool.get('ir.model.data')
                alarm_notif_id = \
                    models_data.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
                manager = self.pool.get('res.users').browse(cr, uid, rec.manager_id.id, context=context)
                manager_karim = self.pool.get('res.users').search(cr, uid,
                                                                  [('login', 'ilike', 'karim.fahim@era-egypt.com')],
                                                                  context=context)
                manager_karim_id = self.pool.get('res.users').browse(cr, uid, manager_karim[0], context=context)
                user_ids.append(manager_karim_id.partner_id.id)
                user_ids.append(manager.partner_id.id)
                alarm_ids.append(alarm_notif_id)
                date_alert = datetime.datetime.now() + relativedelta(minutes=+ 1)
                date_fin = datetime.datetime.now() + relativedelta(minutes=+ 20)
                event_values = {
                    'name': "the request assigned to " + user.name + ' still in progress for a week, the agent have to put a comment',
                    'start_datetime': date_alert.strftime('%Y-%m-%d %H:%M:%S'),
                    'stop_datetime': date_fin.strftime('%Y-%m-%d %H:%M:%S'),
                    'alarm_ids': [(6, 0, alarm_ids)],
                    'partner_ids': [(6, 0, user_ids)],
                    'is_notif': True,
                }
                event_id = event_obj.create(cr, uid, event_values, context=context)

        return

    def case_mark_lost(self, cr, uid, ids, context=None):
        """ Mark the case as lost: state=cancel and probability=0
        """
        mod_obj = self.pool.get('ir.model.data')
        lead_obj = self.pool.get('crm.lead').browse(cr, uid, ids)
        stage_lost = mod_obj.get_object_reference(
            cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead8')
        if stage_lost:
            data = stage_lost[1]
            lead_obj.write({'stage_id': data})
        # # create notif
        # for rec in self.browse(cr, uid, ids, context):
        #     if rec.manager_id:
        #         user = 'user'
        #         if rec.user_id:
        #             user = self.pool.get('res.users').browse(cr, uid, rec.user_id.id, context=context)
        #         user_ids = []
        #         alarm_ids = []
        #         event_obj = self.pool.get('calendar.event')
        #         models_data = self.pool.get('ir.model.data')
        #         alarm_notif_id = \
        #             models_data.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
        #         manager = self.pool.get('res.users').browse(cr, uid, rec.manager_id.id, context=context)
        #         manager_karim = self.pool.get('res.users').search(cr, uid,
        #                                                           [('login', 'ilike', 'karim.fahim@era-egypt.com')],
        #                                                           context=context)
        #         manager_karim_id = self.pool.get('res.users').browse(cr, uid, manager_karim[0], context=context)
        #         user_ids.append(manager_karim_id.partner_id.id)
        #         user_ids.append(manager.partner_id.id)
        #         alarm_ids.append(alarm_notif_id)
        #         date_alert = datetime.datetime.now() + relativedelta(minutes=+ 1)
        #         date_fin = datetime.datetime.now() + relativedelta(minutes=+ 20)
        #         event_values = {
        #             'name': "the request assigned to " + user.name + ' have marked lost',
        #             'start_datetime': date_alert.strftime('%Y-%m-%d %H:%M:%S'),
        #             'stop_datetime': date_fin.strftime('%Y-%m-%d %H:%M:%S'),
        #             'alarm_ids': [(6, 0, alarm_ids)],
        #             'partner_ids': [(6, 0, user_ids)],
        #             'is_notif': True,
        #         }
        #         event_id = event_obj.create(cr, uid, event_values, context=context)
        return True

    def case_mark_won(self, cr, uid, ids, context=None):
        """ Mark the case as lost: state=cancel and probability=0
        """
        mod_obj = self.pool.get('ir.model.data')
        lead_obj = self.pool.get('crm.lead').browse(cr,uid,ids)
        stage_lost = mod_obj.get_object_reference(
            cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead7')
        print "@@@@@@@@@@@@@@@@@@@@@@@@@2",stage_lost,ids,lead_obj
        if stage_lost:

            print stage_lost
            data = stage_lost[1]
            print "<<<<<<<<<<<<<<<<<<<<<",data
            lead_obj.write({'stage_id': data, 'request_code': 'Wo'})
        # # create notif
        # for rec in self.browse(cr, uid, ids, context):
        #     if rec.manager_id:
        #         user = 'user'
        #         if rec.user_id:
        #             user = self.pool.get('res.users').browse(cr, uid, rec.user_id.id, context=context)
        #         user_ids = []
        #         alarm_ids = []
        #         event_obj = self.pool.get('calendar.event')
        #         models_data = self.pool.get('ir.model.data')
        #         alarm_notif_id = \
        #             models_data.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
        #         manager = self.pool.get('res.users').browse(cr, uid, rec.manager_id.id, context=context)
        #         user_ids.append(manager.partner_id.id)
                # manager_karim = self.pool.get('res.users').search(cr, uid,
                #                                                   [('login', 'ilike', 'karim.fahim@era-egypt.com')],
                #                                                   context=context)
                # manager_karim_id = self.pool.get('res.users').browse(cr, uid, manager_karim[0], context=context)
                # user_ids.append(manager_karim_id.partner_id.id)
                # alarm_ids.append(alarm_notif_id)
                # date_alert = datetime.datetime.now() + relativedelta(minutes=+ 1)
                # date_fin = datetime.datetime.now() + relativedelta(minutes=+ 20)
                # event_values = {
                #     'name': "the request assigned to " + user.name + ' have marked won',
                #     'start_datetime': date_alert.strftime('%Y-%m-%d %H:%M:%S'),
                #     'stop_datetime': date_fin.strftime('%Y-%m-%d %H:%M:%S'),
                #     'alarm_ids': [(6, 0, alarm_ids)],
                #     'partner_ids': [(6, 0, user_ids)],
                #     'is_notif': True,
                # }
                # event_id = event_obj.create(cr, uid, event_values, context=context)
        return True

    #     def on_change_category(self, cr, uid, ids, category,service_ids, context=None):
    #         res = {'value': {'type_lead': "0"}}
    #         categ = self.pool.get('mt.case.categ').browse(cr, uid, category)
    #         obj = self.pool.get('ir.model.data').get_object_reference(
    #             cr, uid, 'mt_crm_brokerage_manager', 'categ_oppor21')
    #         if categ and obj and categ.id == obj[1]:
    #             res = {'value': {'type_lead': "1"}}
    #
    #         obj2 = self.pool.get('ir.model.data').get_object_reference(
    #             cr, uid, 'mt_crm_brokerage_manager', 'categ_oppor20')
    #         sale_team = self.pool.get('ir.model.data').get_object_reference(
    #             cr, uid, 'mt_crm_brokerage_manager', 'crm_case_section7')
    #         if categ and obj2 and categ.id == obj2[1]:
    #             if service_ids == 'buy' :
    #                 res = {'value': {'section_id': sale_team[1]}}
    #
    #         return res
    #
    # def on_change_category(self, cr, uid, ids, category, context=None):
    #     # -------------------------------------------------------------------------------
    #     if context is None:
    #         context = {}
    #     res = {'value': {'type_lead': '0'}}
    #
    #     if category:
    #         categ = self.pool.get('mt.case.categ').browse(cr, uid, category)
    #         obj = self.pool.get('ir.model.data').get_object_reference(
    #             cr, uid, 'mt_crm_brokerage_manager', 'categ_oppor21')
    #         if categ and obj and categ.id == obj[1]:
    #             res['value'] = {'type_lead': "1"}
    #
    #     return res

    def onchange_country_id(self, cr, uid, ids, country_id, context=None):

        if country_id:
            data_obj = self.pool.get('ir.model.data')
            country_ref = data_obj.get_object_reference(cr, uid, 'base', 'eg')
            country_egypt_code = self.pool.get('res.country').browse(cr, uid, country_ref[1], context=context).code
            country = self.pool.get('res.country').browse(cr, uid, country_id, context=context)
            country_code2 = country.code
            if country_egypt_code != country_code2:
                return {'value': {'phone': country.phone_code}}
            else:
                return {'value': {'phone': False}}

    # def on_change_property_ids(self, cr, uid, ids, property_ids, context=None):
    #
    #     print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    #     type_prop = type_villa = '0'
    #     #         prop = self.browse(cr, uid, ids)
    #     if property_ids:
    #         #             ids = property_ids[0][2]
    #         #             if ids and len(ids) > 0:
    #
    #         for categ in self.pool.get('mt.property.type').browse(cr, uid, property_ids):
    #
    #             if categ.name in ('Ground floor with Garden', 'Villa'):
    #                 type_prop = '1'
    #             if categ.name == 'Villa':
    #                 type_villa = '1'
    #
    #     return {'value': {'type_prop': type_prop, 'type_villa': type_villa}}

    def on_change_user(self, cr, uid, ids, user_id, context=None):
        """ When changing the user, also set a section_id or restrict section id
            to the ones user_id is member of. """
        leads = self.browse(cr, uid, ids)
        user = self.pool.get('res.users').browse(cr, uid, user_id)
        comp_id = user.company_id.id
        section_id = self._get_default_section_id(
            cr, uid, user_id=user_id, context=context) or False
        mod_obj = self.pool.get('ir.model.data')
        stage = mod_obj.get_object_reference(
            cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead2')
        data = stage[1]
        if user_id and not section_id:
            section_ids = self.pool.get('crm.case.section').search(cr, uid, [
                '|', ('user_id', '=', user_id), ('member_ids', '=', user_id)], context=context)
            if section_ids:
                section_id = section_ids[0]
            mod_obj = self.pool.get('ir.model.data')
            stage = mod_obj.get_object_reference(
                cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead2')
            data = stage[1]
            return {'value': {'section_id': section_id,
                              'company_id': comp_id,
                              'stage_id': data,
                              'origin_id': leads.user_id.id}}
        if user_id:
            return {'value': {'section_id': section_id,
                              'company_id': comp_id,
                              'stage_id': data,
                              'origin_id': leads.user_id.id}}

    def on_change_partner_id(self, cr, uid, ids, partner_id, is_client, context=None):
        child_ids = []
        partner = self.pool.get('res.partner').browse(cr, uid, partner_id)
        if partner:
            title_name = ""
            if partner.title:
                title_name = self.pool.get('res.partner.title').browse(
                    cr, uid, partner.title.id).name + " " + partner.name
            else:
                title_name = partner.name
            print is_client
            if is_client == 'org':
                for child in partner.child_ids:
                    child_ids.append(child.id)
                our_contact = self.pool.get('res.partner').browse(cr, uid, child_ids[0])
                contact = our_contact.name
                print contact
            else:
                contact = partner.name

            return {'value': {
                'name': title_name,
                'street': partner.street,
                'city': partner.city,
                'city_id': partner.city_id.id,
                'zip': partner.zip,
                'country_id': partner.country_id,
                'phone': partner.phone,
                'fax': partner.fax,
                'email_from': partner.email,
                'user_id': partner.user_id.id,
                'name_arabic': partner.name_arabic,
                #                 'organisation_id': partner.parent_id.id,
                'title': partner.title.id,
                'client_type_id': partner.client_type_id.id,
                'contact_name': contact,
            }
            }

    def on_change_partner_id_client(self, cr, uid, ids, partner_id, is_client, context=None):
        fields = self.on_change_partner_id(
            cr, uid, id, partner_id, is_client, context=context)
        return fields

    def on_change_partner_id_org(self, cr, uid, ids, partner_id, is_client, context=None):
        fields = self.on_change_partner_id(
            cr, uid, id, partner_id, is_client, context=context)
        return fields

    def onchange_client_phone(self, cr, uid, ids, phone, context=None):
        partner_ids = False
        if phone:
            partner_ids = self.pool.get('res.partner').search(
                cr, uid, [('phone', '=', phone)])
        #
        if partner_ids:
            partner = self.pool.get('res.partner').browse(
                cr, uid, partner_ids[0], context=context)
            return {'value': {'name': partner.name,
                              #                                 'contact_name':partner.name,
                              'name_arabic': partner.name_arabic,
                              'street': partner.street,
                              'street2': partner.street2,
                              'state_id': partner.state_id.id,
                              'city_id': partner.city_id.id,
                              'zip': partner.zip,
                              'country_id': partner.country_id.id,
                              'mobile': partner.mobile,
                              'fax': partner.fax,
                              'email_from': partner.email,
                              'title': partner.title.id,
                              'user_id': partner.user_id.id,
                              #                              'organisation_id': partner.parent_id.id,
                              'partner_id': partner.id,
                              'client_type_id': partner.client_type_id.id
                              }}

    def onchange_employe_id(self, cr, uid, ids, employee):
        if not employee:
            return {}

        user_id = self.pool.get('hr.employee').browse(
            cr, uid, employee).user_id

        return {'value': {'user_id': user_id}}

    def _check_digital_number_landline(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        check = True
        # print course.description==course.name
        if obj.phone3 and len(obj.phone3) != 10:
            check = False
        return check

    def _check_digital_number_fax(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        check = True
        # print course.description==course.name
        if obj.fax and len(obj.fax) != 11:
            check = False
        return check

    def _check_digital_number_mobile2(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        check = True
        # print course.description==course.name
        if obj.mobile and len(obj.mobile) != 11:
            check = False
        return check

    def _client_count(self, cr, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, 0)
        #         partner_ids = self.pool.get('crm.lead').search(cr, uid, [('partner_id', '=',partner_id)])
        action_ids = self.pool.get('crm.lead').search(
            cr, uid, [('partner_id', 'in', ids)])
        #                 for cmp  in self.browse(cr, uid, ids, context=context):

        for client in self.pool.get('crm.lead').browse(cr, uid, action_ids, context):
            res[client.partner_id.id] += 1
        return res

    def on_change_request_stage(self, cr, uid, ids, request_stage_id, context=None):
        result = {}
        if request_stage_id:
            part = self.pool.get('crm.case.stage').browse(
                cr, uid, request_stage_id, context=context)
            result['request_code'] = part.code or False
        return {'value': result}

    #             result['stage_id'] = part.name or False
    #         return {'value': result}

    #     def on_change_stage(self, cr, uid, ids, context=None):
    #
    #         result = {}
    #         if ids:
    #             part = self.pool.get('crm.lead').browse(cr, uid, ids, context=context)
    #             result['old_stage'] = part.stage_id.id or False
    #             result['new_stage'] = part.stage_id.id or False
    #         return {'value': result}

    def create(self, cr, uid, vals, context=None):
        id = super(crm_lead, self).create(cr, uid, vals, context=context)
        values = {}

        values['req_id'] = id
        if vals.get('reference_request', '/') == '/':
            values['reference_request'] = self.pool.get('ir.sequence').get(cr, uid, 'crm.lead') or '/'
        if vals.get('stage_id', False):
            values['old_stage'] = vals['stage_id']
            values['new_stage'] = vals['stage_id']
            values['request_stage_id'] = vals['stage_id']
        if vals.get('service_ids', False) in ('sale', 'seller_rent', 'share_seller'):
            values.update({'request_type': 'seller'})
        else:
            if vals.get('service_ids', False) == 'buy':
                values.update({'request_type': 'buyer'})

        if vals.get('partner_id', False):
            partner_id = vals['partner_id']
            lead_ids = self.pool.get('crm.lead').search(cr, uid,
                                                        [('partner_id', '=', partner_id), ('type', '=', 'opportunity')],
                                                        context=context)
            count = 0
            value_partner = {}
            if lead_ids and len(lead_ids) > 0:
                mod_obj = self.pool.get('ir.model.data')
                if len(lead_ids) == 1:
                    client_type_new = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager',
                                                                   'client_type_id1')
                    value_partner = {
                        'client_type_id': client_type_new[1]
                    }
                    self.pool.get('res.partner').write(cr, uid, partner_id, {'client_type_id': client_type_new[1]})
                    values.update({'client_type_id': client_type_new[1]})

                if len(lead_ids) > 1:
                    client_type_exist = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager',
                                                                     'client_type_id5')
                    value_partner = {
                        'client_type_id': client_type_exist[1]
                    }
                    self.pool.get('res.partner').write(cr, uid, partner_id, {'client_type_id': client_type_exist[1]})
                    vals.update({'client_type_id': client_type_exist[1]})

                    cr.execute('select count(*) from crm_lead c inner join crm_case_stage cc on cc.id = c.stage_id \
                                                         where   cc.name = \'Won\' and c.id in' + str(tuple(lead_ids)))
                    count = cr.fetchone()[0] or 0
                if len(lead_ids) == 0:
                    client_type_new = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager',
                                                                   'client_type_id1')
                    value_partner = {
                        'client_type_id': client_type_new[1]
                    }
                    self.pool.get('res.partner').write(cr, uid, partner_id, {'client_type_id': client_type_new[1]})
                    vals.update({'client_type_id': client_type_new[1]})
            if count > 0:
                client_type_contracted = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager',
                                                                      'client_type_id2')
                if client_type_contracted[1]:
                    value_partner.update({'client_type_id': client_type_contracted[1]})
                    vals.update({'client_type_id': client_type_contracted[1]})
            if value_partner and len(value_partner) > 0:
                self.pool.get('res.partner').write(cr, uid, partner_id, value_partner)

                # create notif
        if vals.get('manager_id', False):
            #                 user = 'user'
            if vals.get('user_id', False):
                user_id = vals['user_id']
            else:
                user_id = uid
            user = self.pool.get('res.users').browse(cr, uid, user_id, context=context)
            user_ids = []
            alarm_ids = []
            event_obj = self.pool.get('calendar.event')
            models_data = self.pool.get('ir.model.data')
            alarm_notif_id = \
                models_data.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
            manager_id = vals['manager_id']
            manager = self.pool.get('res.users').browse(cr, uid, manager_id, context=context)
            user_ids.append(manager.partner_id.id)
            # manager_karim = self.pool.get('res.users').search(cr, uid,
            #                                                   [('login', 'ilike', 'karim.fahim@era-egypt.com')],
            #                                                   context=context)
            # manager_karim_id = self.pool.get('res.users').browse(cr, uid, manager_karim[0], context=context)
            # user_ids.append(manager_karim_id.partner_id.id)
            alarm_ids.append(alarm_notif_id)
            date_alert = datetime.datetime.now() + relativedelta(minutes=+ 1)
            date_fin = datetime.datetime.now() + relativedelta(minutes=+ 20)
            print'---user------', user
            print'---user--name----', user.name
            event_values = {
                'name': "the user " + user.name + ' have create a new request',
                'start_datetime': date_alert.strftime('%Y-%m-%d %H:%M:%S'),
                'stop_datetime': date_fin.strftime('%Y-%m-%d %H:%M:%S'),
                'alarm_ids': [(6, 0, alarm_ids)],
                'partner_ids': [(6, 0, user_ids)],
                'is_notif': True,
            }
            event_id = event_obj.create(cr, uid, event_values, context=context)
        super(crm_lead, self).write(cr, uid, id, values, context=context)
        return id

    def write(self, cr, uid, ids, vals, context=None):
        # stage change: update date_last_stage_update
        #         id = super(crm_lead, self).write(cr, uid, ids, vals, context=context)
        if not ids:
            return True
        if isinstance(ids, (int, long)):
            ids = [ids]
        current = self.browse(cr, uid, ids, context=context)
        product_template_obj = self.pool.get('product.template')
        mod_obj = self.pool.get('ir.model.data')
        stage_case_obj = self.pool.get('crm.case.stage')
        res_partner_obj = self.pool.get('res.partner')
        if vals.get('stage_id', False):
            part = self.browse(cr, uid, ids, context=context).new_stage.id
            vals.update({'old_stage': part})
        if vals.get('stage_id', False):
            vals.update({'new_stage': vals['stage_id']})
            vals.update({'request_stage_id': vals['stage_id']})
        if vals.get('service_ids', False) in ('sale', 'seller_rent', 'share_seller'):
            vals.update({'request_type': 'seller'})
        else:
            if vals.get('service_ids', False) == 'buy':
                vals.update({'request_type': 'buyer'})
        lead = self.browse(cr, uid, ids[0], context=context)
        if lead.comment == False and lead.request_code == 'Rej':
            raise osv.except_osv(_('Error!'), _('Please you must write the reason of reject.'))
        partner_id = self.browse(cr, uid, ids[0], context=context).partner_id.id
        lead_ids = self.search(cr, uid, [('partner_id', '=', partner_id), ('type', '=', 'opportunity')],
                               context=context)
        count = 0
        value_partner = {}
        if lead_ids and len(lead_ids) > 0:
            if len(lead_ids) == 1:
                cr.execute('select count(*) from crm_lead c inner join crm_case_stage cc on cc.id = c.stage_id \
                  where   cc.name = \'Won\' and c.id  =%s' % lead_ids[0])
                count = cr.fetchone()[0] or 0
            if len(lead_ids) > 1:
                client_type_exist = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'client_type_id5')
                value_partner = {'client_type_id': client_type_exist[1]}
                res_partner_obj.write(cr, uid, partner_id, {'client_type_id': client_type_exist[1]})
                vals.update({'client_type_id': client_type_exist[1]})
                cr.execute('select count(*) from crm_lead c inner join crm_case_stage cc on cc.id = c.stage_id \
                                                     where   cc.name = \'Won\' and c.id in' + str(tuple(lead_ids)))
                count = cr.fetchone()[0] or 0
        if count > 0:
            client_type_contracted = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager',
                                                                  'client_type_id2')
            if client_type_contracted[1]:
                value_partner.update({'client_type_id': client_type_contracted[1]})
                vals.update({'client_type_id': client_type_contracted[1]})
        if value_partner and len(value_partner) > 0:
            res_partner_obj.write(cr, uid, partner_id, value_partner)
        if vals.get('stage_id', False):
            #             stage = self.pool.get('crm.lead').browse(
            #                 cr, uid, ids, context=context).new_stage.code
            req_type = self.browse(cr, uid, ids, context=context).request_type
            stage = vals['stage_id']
            stage_code = stage_case_obj.browse(
                cr, uid, stage, context=context).code
            prop_id = self.browse(cr, uid, ids, context=context).property.id
            prop = self.browse(cr, uid, ids, context=context).property
            if stage_code in ('Lo', 'Rej') and req_type == 'seller':
                if prop_id:
                    product_template_obj.write(cr, uid, prop_id, {'website_published': False, 'states': 'removed'})
            if stage_code == 'Wo':
                if prop_id:
                    product_template_obj.write(cr, uid, prop_id, {'states': 'sold'})
            satge_ref = mod_obj.get_object_reference(
                cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead7')
            if vals.get('stage_id', False):
                stage = vals.get('stage_id', False)
                if stage == satge_ref[1]:
                    crm_id = self.browse(cr, uid, ids, context=context)
                    crm_leads = self.search(cr, uid, [('request_type', '=', 'buyer'), ('property', '=', prop_id),
                                                      ('id', '!=', crm_id.id)], context=context)
                    if crm_leads and len(crm_leads) > 0:
                        if prop_id:
                            self.write(cr, uid, crm_leads, {'property': False})
                            for lead in crm_leads:
                                self.message_post(cr, uid, lead, body=_("property is Sold"), context=context)

                if prop and prop.origine_id:
                    osv.osv.write(self, cr, SUPERUSER_ID, prop.origine_id.id, {'stage_id': satge_ref[1]})
                    # fake_user = mod_obj.get_object_reference(
                    #          cr, uid, 'mt_crm_brokerage_manager', 'fake_user')
            if vals.get('stage_id', False):
                stage = vals.get('stage_id', False)
                stage_obj = stage_case_obj.browse(
                    cr, uid, stage, context=context)
                stage_code_ref = stage_obj.code
                vals.update({'state': stage_obj.code})
                # new
                if stage_code_ref == 'Ne':
                    satge_ref = mod_obj.get_object_reference(
                        cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead1')
                    stage_code = stage_case_obj.browse(
                        cr, uid, satge_ref[1], context=context).code
                    if stage == satge_ref[1]:
                        vals.update({'request_code': stage_code})
                # Assigned
                if stage_code_ref == 'Ass':
                    satge_ref = mod_obj.get_object_reference(
                        cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead2')
                    stage_code = stage_case_obj.browse(
                        cr, uid, satge_ref[1], context=context).code
                    if stage == satge_ref[1]:
                        vals.update({'request_code': stage_code})
                # In Progress
                if stage_code_ref == 'InPro':
                    satge_ref = mod_obj.get_object_reference(
                        cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead3')
                    stage_code = stage_case_obj.browse(
                        cr, uid, satge_ref[1], context=context).code
                    if stage == satge_ref[1]:
                        vals.update({'request_code': stage_code})
                # Won
                if stage_code_ref == 'Wo':
                    satge_ref = mod_obj.get_object_reference(
                        cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead7')
                    stage_code = stage_case_obj.browse(
                        cr, uid, satge_ref[1], context=context).code
                    if stage == satge_ref[1]:
                        vals.update({'request_code': stage_code})

                # Lost
                if stage_code_ref == 'Lo':
                    satge_ref = mod_obj.get_object_reference(
                        cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead8')
                    stage_code = stage_case_obj.browse(
                        cr, uid, satge_ref[1], context=context).code
                    if stage == satge_ref[1]:
                        vals.update({'request_code': stage_code})
                # Rejected
                if stage_code_ref == 'Rej':
                    satge_ref = mod_obj.get_object_reference(
                        cr, SUPERUSER_ID, 'mt_crm_brokerage_manager', 'crm_stage_lead4')
                    # fake_user = mod_obj.get_object_reference(
                    #             cr, uid, 'mt_crm_brokerage_manager', 'fake_user')
                    # if fake_user:
                    #     vals.update({'user_id': fake_user[1]})
                    stage_code = stage_case_obj.browse(
                        cr, uid, satge_ref[1], context=context).code
                    if stage == satge_ref[1]:
                        vals.update({'request_code': stage_code})
                    vals.update({'origin_id': lead.user_id.id})

        if vals.get('property', False):
            prop_id = self.browse(cr, uid, ids, context=context).property.id
            if prop_id:
                props = product_template_obj.search(cr, uid, [('id', '=', prop_id)], context=context)
                product_template_obj.message_post(cr, uid, props, body=_("properties is consulted"), context=context)
        # print'---ids---', ids
        # super(crm_lead, self).write(cr, uid, ids, vals, context=context)
        for record in self.browse(cr, uid, ids):

            data_country = record.country_id.id
            country_code2 = self.pool.get('res.country').browse(
                cr, uid, data_country, context=context).lenght
            data = record.phone
            # if data and data !=False :
            #     if len(data) != country_code2:
            #                 raise osv.except_osv(_('Warning!'), _("the length of phone is not correct !!!!!!!"))
            phone = record.phone
            teams = record.section_id and record.section_id.id or False
            user = record.user_id.id
            if teams:
                # fake_user = mod_obj.get_object_reference( cr, SUPERUSER_ID, 'mt_crm_brokerage_manager', 'fake_user')
                lead_ids = self.pool.get('crm.lead').search(cr, uid, [('phone', '=', phone), ('section_id', '=', teams),
                                                                      ('user_id', '!=', user), ('id', '!=', record.id)],
                                                            context=context)
                if lead_ids and len(lead_ids) > 0:
                    old_lead = self.pool.get('crm.lead').browse(cr, uid, lead_ids[0], context)
                    raise osv.except_osv(_(u'Warning'),
                                         _(u'you have a request %s assigned  to %s. in the same departement') % (
                                             old_lead.name, old_lead.user_id.name))
                lead_sec_ids = self.pool.get('crm.lead').search(cr, uid,
                                                                [('phone', '=', phone), ('section_id', '!=', teams)],
                                                                context=context)
                if lead_sec_ids and len(lead_sec_ids) > 0:
                    old_lead = self.pool.get('crm.lead').browse(cr, uid, lead_sec_ids[0], context)
                    self.message_post(cr, uid, record.id, body=_(
                        "You have a request belong other team %s with other user %s" % (
                            old_lead.section_id.name, old_lead.user_id.name)), context=context)
                if (record.referred_by == 'inside' and record.referred_inside) or (
                        record.referred_by == 'outside' and record.referred_outside):
                    vals.update({'lock_referred_by': True})
        res=super(crm_lead, self).write(cr, uid, ids, vals, context=context)
        current = self.browse(cr, uid, ids, context=context)
        if current.stage_id.id == mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead7')[1] and current.request_type == 'buyer':
            lead_sell=False
            if current.property.id:
                self.pool.get('product.template').write(cr, uid, [current.property.id], {'states': 'sold'})
                lead_sell = self.pool.get('crm.lead').search(cr, uid, [('request_type', '=', 'seller'),
                                                                       ('property', '=', current.property.id)],
                                                             context=context)
            if lead_sell:
                self.pool.get('crm.lead').write(cr, uid, [lead_sell[0]], {
                    'stage_id': mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead7')[1]})
        return res

    #         return super(crm_lead, self).write(cr, uid, ids, vals, context=context)

    def _check_stage(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        check = True
        # print course.description==course.name
        if obj.old_stage.name == 'Buyer Request' and obj.new_stage.name == 'Seller Request':
            check = False
        if obj.old_stage.code == 'Rej':
            check = False
        if obj.old_stage.code == 'Lo':
            check = False
        return check

    # def onchange_client_code_id(self, cr, uid, ids, is_client):
    #     if not is_client:
    #         return {}
    #
    #     #       client=self.browse(cr, uid, ids).is_client
    #     if is_client == 'client':
    #         return {'value': {'client_code': False}}
    #     else:
    #         return {'value': {'client_code': True}}

    def on_change_model_and_list(self, cr, uid, ids, state_model, context=None):
        value = {}
        if state_model == 'available':

            data_obj = self.pool.get('ir.model.data')
            #         id1 = data_obj._get_id(cr, uid, 'mt_crm_proprety', 'mt_property_kanban')
            #         if id1:
            #                 id1 = data_obj.browse(cr, uid, id1, context=context).res_id
            id2 = data_obj._get_id(cr, uid, 'mt_crm_proprety', 'product_template_form_view_b2b')
            if id2:
                id2 = data_obj.browse(cr, uid, id2, context=context).res_id
            id3 = data_obj._get_id(cr, uid, 'mt_crm_proprety', 'mt_developer_tree')
            if id3:
                id3 = data_obj.browse(cr, uid, id3, context=context).res_id

            value = {

                'context': context,
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'product.template',
                'view_id': False,
                'views': [(id3, 'tree'), (id2, 'form')],
                #                 'domain': [['category', '!=', False]],
                'type': 'ir.actions.act_window',
            }

            return {'value': {'mailing_domain': value}}

    #             mailing_list_ids = set()
    #             for item in list_ids:
    #                 if isinstance(item, (int, long)):
    #                     mailing_list_ids.add(item)
    #                 elif len(item) == 3:
    #                     mailing_list_ids |= set(item[2])
    #             if mailing_list_ids:
    #                 value['mailing_domain'] = "[('list_id', 'in', %s)]" % list(mailing_list_ids)
    #             else:
    #                 value['mailing_domain'] = "[('list_id', '=', False)]"
    #         else:
    #             value['mailing_domain'] = False
    #         return {'value': value}

    def onchange_assign_id(self, cr, uid, ids, assign):
        res = {}
        sales_team = []
        if assign:
            cr.execute(
                "select section_id from sale_member_rel where member_id ='%s'" % str(assign))
            value = cr.fetchall()
            if value:
                for id in value:
                    sales_team.append(id[0])
                res['domain'] = {'section_id': [('id', 'in', sales_team)]}
                return res
            else:
                res['domain'] = {'section_id': False}
                return res

    def action_cil_send(self, cr, uid, ids, context=None):
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        '''
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        ir_model_data = self.pool.get('ir.model.data')
        try:
            template_id = ir_model_data.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'cil_template_form')[
                1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form')[
                1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'crm.lead',
            'default_res_id': ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    def onchange_district_id(self, cr, uid, ids, state_prop_id):
        res = {}
        if state_prop_id:
            city = self.pool.get('mt.city').browse(cr, uid, state_prop_id)
            res['domain'] = {'neighborhood_ids': [('id', 'in', city.neighborhood_ids.ids)]}
        return res

    def on_change_nom(self, cr, uid, ids, service_ids, view_id=None, view_type='form', context=None, toolbar=False,
                      submenu=False):
        res = {}
        if service_ids:
            if service_ids in ('sale', 'seller_rent', 'share_seller'):
                return {'value': {'request_type': 'seller',
                                  }}
            else:
                if service_ids in ('buy', 'buyer_rent', 'share_buyer'):
                    return {'value': {'request_type': 'buyer',
                                      }}
        return {'value': {'request_type': None,
                          }}

    def _search_budget_search(self, cr, uid, obj, name, args, context):
        form = args[0][2] - (20 * args[0][2] / 100)
        to = args[0][2] + (20 * args[0][2] / 100)
        x = [('budget', '>=', form), ('budget2', '<=', to)]
        res = self.search(cr, uid, x, context=context)
        return [('id', 'in', res)]

    _columns = {
        # search prop
        'mailing_domain': fields.char('Select Properties', oldname='domain'),
        'req_id': fields.many2one("crm.lead", 'request'),
        #
        'budget_search': fields.function(lambda self: self, string='Budget', type='float',
                                         fnct_search=_search_budget_search),
        'name': fields.char('Subject', select=1),
        'client_count': fields.function(_client_count, type='integer', string="Clients"),
        'medium_id2': fields.many2one('crm.tracking.medium', 'Channel',
                                      help="This is the method of delivery. Ex: Postcard, Email, or Banner Ad",
                                      oldname='channel_id'),
        'reference_request': fields.char("Request ID", readonly=True),
        'code': fields.char("code"),
        'comment': fields.text("Comment", track_visibility='onchange'),
        'name_arabic': fields.char("Name (In Arabic)", size=64),
        'budget': fields.float("Budget", ),
        'budget2': fields.float("Budget", ),
        'payment': fields.selection([('cash', 'Cash'), ('installments', 'Installments')], 'Payment type'),
        #         'organisation_id': fields.many2one('res.partner', 'Organisation', domain="[('is_company','=',1)]"),
        'area': fields.char("Area", size=64),
        'assign': fields.many2one("res.users", track_visibility='onchange'),
        'phone3': fields.char("Home", size=64),
        'campaign_id': fields.many2one("marketing.campaign", 'Campaign'),
        'bua_gross': fields.float('BUA Gross (Sqm)'),
        'bua_gross2': fields.float('BUA Gross (Sqm)'),
        'bua_net': fields.float('BUA Net (Sqm)'),
        'gardean_area': fields.float('Garden Area (Sqm)'),
        'area_sqm': fields.float('Garden Area (Sqm)', digits_compute=dp.get_precision('Account')),
        'product_unit_id8': fields.many2one('product.uom', 'unit of mesure'),
        'roof_area': fields.float('Roof Area (Sqm)'),
        'city_id': fields.many2one('mt.city', 'City Search'),
        'property_ids': fields.many2one('mt.property.type', 'Property Type'),
        'service_ids': fields.selection(
            [('buy', 'Buy'), ('sale', 'Sale'), ('buyer_rent', 'Rent (Buyer)'), ('seller_rent', 'Rent (Seller)'),
             ('share_buyer', 'Share (Buyer)'), ('share_seller', 'Share (Seller)')], 'Service Type', translate=True),
        'neighborhood_ids': fields.many2one('mt.neighborhood', string='Neighborhood'),
        'neighborhood1_ids': fields.many2one('mt.neighborhood', string='Neighborhood 2'),
        'district_ids': fields.many2one('mt.city', string='District'),
        'district1_ids': fields.many2one('mt.city', string='District 2'),
        'state_prop_id': fields.many2one('res.country.state', string='City'),
        'bedrooms_no_ids': fields.many2one('mt.bedrooms', string='No. of Bedrooms'),
        'bathrooms_no_ids': fields.many2one('mt.bathrooms', string='No. of Bathrooms'),
        'property': fields.many2one('product.template', string="Proprety"),
        'project_id': fields.many2one('product.template', string="Project"),
        'nom': fields.char('Name categ'),
        'category': fields.many2one('mt.case.categ', 'Category List'),
        'type_lead': fields.char('Type'),
        'type_prop': fields.char('Type'),
        'type_villa': fields.char('Type'),
        'category_name': fields.char("name of category"),
        'is_reffered': fields.boolean(string="Is reffered"),
        'fax': fields.char('Work'),
        'currency_id': fields.many2one('res.currency', string='Currency'),
        'currency_id2': fields.many2one('res.currency', string='Currency'),
        'currency_id3': fields.many2one('res.currency', string='Currency'),
        'currency_id4': fields.many2one('res.currency', string='Currency'),
        'currency_id5': fields.many2one('res.currency', string='Currency'),
        'product_unit_id': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id4': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id1': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id2': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id3': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id12': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id13': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id131': fields.many2one('product.uom', 'unit of mesure'),
        'commer_fron': fields.float('Commercial Frontage M'),
        'ceiling_hei': fields.float('Ceiling Height M'),
        'product_unit_id5': fields.many2one('product.uom', 'unit of mesure'),
        'product_unit_id6': fields.many2one('product.uom', 'unit of mesure'),
        'land_area': fields.float('Land Area from'),
        'land_area2': fields.float('To'),
        'product_unit_id7': fields.many2one('product.uom', 'unit of mesure'),
        'request_stage_id': fields.many2one('crm.case.stage', 'Request stage', track_visibility='onchange'),
        'request_code': fields.char('Request Code'),
        'user_id': fields.many2one('res.users', 'Assign To', select=True, track_visibility='onchange'),
        'licence_id': fields.many2one('mt.licenses', 'Licence', select=True, track_visibility='onchange'),
        'old_stage': fields.many2one('crm.case.stage', 'Old stage'),
        'new_stage': fields.many2one('crm.case.stage', 'New stage'),
        'progress_id': fields.many2one('mt.progress.action', 'Progress Action'),
        'reject_id': fields.many2one('mt.reject.reason', 'Reason', track_visibility='onchange'),
        'is_client': fields.selection([('client', 'Client'), ('org', 'Organisation')], 'Type'),
        'referred_by': fields.selection([('inside', 'Inside'), ('outside', 'Outside')], 'Referred by'),
        'referred_outside': fields.many2one('res.partner', 'Partner'),
        'referred_inside': fields.many2one('res.users', 'Users'),
        'lock_referred_by': fields.boolean('Referred'),
        'state': fields.char('Code stage'),
        'budget_from': fields.float("Budget Min", digits_compute=dp.get_precision('Account')),
        'budget_to': fields.float("Budget Max", digits_compute=dp.get_precision('Account')),
        'origin_id': fields.many2one('res.users', 'Origin', select=True, track_visibility='onchange'),
        'admin_id': fields.many2one('res.users', 'Admin'),
        'project_interest_id': fields.many2many('product.template', 'lead_project_interrest_rel', 'project_id',
                                                'lead_id', string="Projects of Interest"),
        'agreement_start': fields.date('Agreement Start Date'),
        'agreement_end': fields.date('Agreement End Date'),
        'date_assign': fields.date('Date of assign'),
        'date_inprogress': fields.date('Date of progress'),
        'commission_percent': fields.float('Comission Percent'),
        'product_unit_id20': fields.many2one('product.uom', 'Per'),
        'product_unit_id21': fields.many2one('product.uom', 'Per'),
        'product_unit_id22': fields.many2one('product.uom', 'Per'),
        'service_price': fields.float('Service Price', digits_compute=dp.get_precision('Account')),
        'reference_property': fields.char("Property ID", readonly=True),

        'client_code': fields.char('Client Code'),
        'action_count': fields.function(action_count, type='integer'),
        'action_count_lead': fields.function(action_count_lead, type='integer'),
        'request_type': fields.selection([('buyer', 'Buyer'), ('seller', 'Seller')], 'Request Type'),
        'agreement': fields.selection(
            [('none', 'None'), ('normal', 'Normal'), ('exclusive', 'Exclusive'), ('standard', ' Standard')],
            'Agreement'),
        'agreement_serial': fields.char('Agreement Serial'),
        'client_type_id': fields.many2one('mt.client.type', 'Client Type'),
        'query_reason_id': fields.many2one('mt.query.reason', 'Query reason'),
        'blaconies_nbr': fields.selection([('un', '1'),
                                           ('deux', ' 2'),
                                           ('trois', '3'),
                                           ('quatre', '4'),
                                           ('cinq', '5'),
                                           ('six', '6'),
                                           ('sept', '7')], "No. of Balconies"),
        'reception_nbr': fields.selection([('un', '1'),
                                           ('deux', ' 2'),
                                           ('trois', '3'),
                                           ('quatre', '4'),
                                           ('cinq', '5'),
                                           ('six', '6'),
                                           ('sept', '7')], "No. of Receptions"),
        # currency_id = fields.Many2one('res.currency', string='Currency',
        #         required=True, readonly=True, states={'draft': [('readonly', False)]},
        'asking_price': fields.float('Asking Price', digits_compute=dp.get_precision('Account')),
        'sale_price': fields.float('Sale Price', digits_compute=dp.get_precision('Account')),
        'manager_id': fields.many2one('res.users', string='Team Leader', related='section_id.user_id', store=True,
                                      readonly=True),
        #         'manager_id': fields.related('section_id', 'user_id', type='many2one', relation="res.users", string='Manager' , store=True),
        #         'manager_id': fields.many2one('res.users', 'Team leader', select=True, track_visibility='onchange'),
        # Commercial inforamtion
        'total_space_erea': fields.float('Total space area'),
        'number_floors': fields.integer('Number of floors'),
        'floor_area': fields.float('Floor area'),
        'available_floors': fields.integer('Available floors'),

    }

    def act_view_property(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        list = []
        list2 = []
        list3 = []
        if context and context.get('domain', False):
            mailing_domain = context.get('domain')
            if type(mailing_domain) == str:
                list = mailing_domain.split('[')
                list2 = list[3].split(']')
                list3 = list2[0].split(',')
            else:
                list3 = mailing_domain[0][2]

            data_obj = self.pool.get('ir.model.data')
            id1 = data_obj._get_id(cr, uid, 'mt_crm_proprety', 'mt_property_tree')
            if id1:
                id1 = data_obj.browse(cr, uid, id1, context=context).res_id
            id2 = data_obj._get_id(cr, uid, 'product', 'product_template_only_form_view')
            if id2:
                id2 = data_obj.browse(cr, uid, id2, context=context).res_id

            value = {

                'context': context,
                'view_type': 'form',
                'view_mode': 'tree,form ',
                'res_model': 'product.template',
                'view_id': False,
                'views': [(id1, 'tree'), (id2, 'form')],
                'domain': [('id', 'in', list3)],
                'type': 'ir.actions.act_window',
            }

            return value

    def act_view_property22(self, cr, uid, ids, context=None):
        url = 'http://www.python.org/'
        url = "http://localhost:8045/web#id=137&view_type=form&model=product.template"

        return webbrowser.open_new_tab(url)

    # def name_search(self, cr, uid, name='', args=None, operator='ilike', context=None, limit=100):
    #     if context and context.get('domain', False):
    #         list = []
    #         list2 = []
    #         list3 = []
    #         request_domain = context.get('domain')
    #         if type(request_domain) == str:
    #             list = request_domain.split('[')
    #             list2 = list[3].split(']')
    #             list3 = list2[0].split(',')
    #         else:
    #             list3 = request_domain[0][2]
    #         search_ids = self.search(cr, uid, [('id', 'in', list3)], context=context)
    #         return self.name_get(cr, uid, search_ids, context)
    #     print "TTTTTTTTTTTTTTTTTTTTTTTTt",name
    #     return super(crm_lead, self).name_search(cr, uid, [name], args, operator, context, limit)

    def get_new_stage(self, cr, uid):
        mod_obj = self.pool.get('ir.model.data')
        stage = mod_obj.get_object_reference(
            cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead1')
        if stage:
            return stage[1]
        return False

    def on_change_property(self, cr, uid, ids, property, context=None):
        prop = self.pool.get('product.template').browse(cr, uid, property)
        if property:
            return {'value': {
                #                        'partner_id':prop.partner_id.id,
                'property_ids': prop.property_type_id.id,
                #                        'service_ids':prop.service_type,
                #                        'street':prop.street,
                #                        'street2':prop.street2,
                #                        'state_id':prop.state_id.id,
                #                         'city_id':prop.city_id.id,
                #                        'zip':prop.zip,
                #                        'country_id':prop.country_id.id,
                #
                'agreement': prop.agreement,
                'agreement_serial': prop.agreement_serial,
                'neighborhood_ids': prop.neighborhood_ids.id,
                'category': prop.category.id,
                #                        'neighborhood1_ids':[(6,0,prop.neighborhood1_ids.ids)],
                'district_ids': prop.district_ids.id,
                #                        'district1_ids':[(6,0,prop.district1_ids.ids)],
                'bedrooms_no_ids': prop.bedrooms_no_ids.id,
                'bathrooms_no_ids': prop.bathrooms_no_ids.id,
                'area_sqm': prop.area_sqm,
                'product_unit_id8': prop.product_unit_id8.id,
                'bua_gross': prop.bua_gross,
                'bua_gross2': prop.bua_gross2,
                'bua_net': prop.bua_net,
                'gardean_area': prop.gardean_area,
                'roof_area': prop.roof_area,
                'commer_fron': prop.commer_fron,
                'ceiling_hei': prop.ceiling_hei,
                'land_area': prop.land_area,
                'land_area2': prop.land_area_to,
                'product_unit_id': prop.product_unit_id.id,
                'product_unit_id1': prop.product_unit_id1.id,
                'product_unit_id2': prop.product_unit_id2.id,
                'product_unit_id3': prop.product_unit_id3.id,
                'product_unit_id5': prop.product_unit_id5.id,
                'product_unit_id6': prop.product_unit_id6.id,
                'product_unit_id7': prop.product_unit_id7.id,
                'asking_price': prop.asking_price,
                'sale_price': prop.sale_price,
                'payment': prop.payment,
            }
            }

        #     def _check_digital_number_mobile(self, cr, uid, ids, context=None):

    #         obj = self.browse(cr, uid, ids[0], context=context)
    #         check = True
    #         # print course.description==course.name
    #         if obj.comment == False and len(obj.phone) != 11:
    #             check = False
    #         return check
    #
    #     _constraints = [
    #         (_check_digital_pourcentage, u'Please you must write the reason of reject', ['comment'])]

    def _check_digital_number_mobile(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids, context=context)
        check = True
        print check
        for line in obj:
            print '........len(obj.phone)......', len(line.phone3)
            if line.phone.lenght != 8:
                check = False
        return check

    _constraints = [
        (_check_digital_number_mobile, 'Please you must enter 8 digits', ['phone3'])]
    _constraints = [
        (_check_digital_number_landline, 'Please you must enter 10 digits', ['phone3'])]
    _constraints = [
        (_check_digital_number_fax, 'Please you must enter 11 digits', ['fax'])]
    _constraints = [
        (_check_stage, 'you can\'t move to this state', ['stage_id'])]

    #     def _default_request_stage(self, cr, uid, context=None):
    #          if context is None:
    #              context = {}
    #          mod_obj = self.pool.get('ir.model.data')
    #          uom = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'request_stage_potential0')
    #          if uom[1]:
    #                return uom[1]
    #          return False

    def _default_SQM(self, cr, uid, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        uom = mod_obj.get_object_reference(
            cr, uid, 'mt_crm_brokerage_manager', 'product_uom_sqm0')
        if uom[1]:
            return uom[1]
        return False

    def _default_country_id(self, cr, uid, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        ref = mod_obj.get_object_reference(cr, uid, 'base', 'eg')
        if ref[1]:
            return ref[1]
        return False

    def _default_state_id(self, cr, uid, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        ref = mod_obj.get_object_reference(
            cr, uid, 'mt_egyptian_cities', 'res_country_state_eg_1')
        if ref[1]:
            return ref[1]
        return False

    def _default_currency(self, cr, uid, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        uom = mod_obj.get_object_reference(cr, uid, 'base', 'EGP')
        if uom[1]:
            return uom[1]
        return False

    def _default_user(self, cr, uid, context=None):
        if context is None:
            context = {}
        sales_team = self.pool.get('crm.case.section').search(cr, uid, [('user_id', '=', uid)], context=context)
        if not sales_team:
            return uid
        return False

    def _default_admin(self, cr, uid, context=None):
        manager_karim = self.pool.get('res.users').search(cr, uid, [('login', 'ilike', 'karim.fahim@era-egypt.com')],
                                                          context=context)
        if manager_karim:
            return manager_karim[0]
        return False

    def _default_sales_team(self, cr, uid, context=None):
        if context is None:
            context = {}

        cr.execute(
            "select section_id from sale_member_rel where member_id ='%s'" % str(uid))
        value = cr.fetchall()
        if value:
            for id in value:
                return id[0]
        return False

    _defaults = {
        #     'request_stage_id':_default_request_stage,
        'product_unit_id': _default_SQM,
        'product_unit_id1': _default_SQM,
        'product_unit_id2': _default_SQM,
        'product_unit_id3': _default_SQM,
        'product_unit_id4': _default_SQM,
        'product_unit_id5': _default_SQM,
        'product_unit_id6': _default_SQM,
        'product_unit_id7': _default_SQM,
        'product_unit_id8': _default_SQM,
        'product_unit_id12': _default_SQM,
        'product_unit_id13': _default_SQM,
        'product_unit_id131': _default_SQM,
        'product_unit_id20': _default_SQM,
        'product_unit_id21': _default_SQM,
        'product_unit_id22': _default_SQM,
        'reference_request': lambda obj, cr, uid, context: '/',
        'currency_id': _default_currency,
        'currency_id2': _default_currency,
        'currency_id3': _default_currency,
        'currency_id4': _default_currency,
        'currency_id5': _default_currency,
        'state_id': _default_state_id,
        'state_prop_id': _default_state_id,
        'country_id': _default_country_id,
        #         'organisation_id': lambda self, cr, uid, context: uid,
        'assign': lambda self, cr, uid, context: uid,
        'stage_id': lambda self, cr, uid, context: self.get_new_stage(cr, uid),
        'user_id': _default_user,
        'admin_id': _default_admin,
        'section_id': _default_sales_team,
        'is_client': 'client',
        'state': 'Ass',
        'date_assign': datetime.datetime.now(),
    }

    def _message_get_auto_subscribe_fields(self, cr, uid, updated_fields, auto_follow_fields=None, context=None):
        if auto_follow_fields is None:
            auto_follow_fields = ['user_id', 'assign', 'manager_id', 'admin_id']
        return super(crm_lead, self)._message_get_auto_subscribe_fields(cr, uid, updated_fields, auto_follow_fields,
                                                                        context=context)

    def onchange_city_id(self, cr, uid, ids, city_id):
        v = {}
        if city_id:
            city = self.pool.get('mt.city').browse(cr, uid, city_id)
            v['zip'] = city.zip
            v['city'] = city.long_name
            v['country_id'] = city.country_id.id
            v['state_id'] = city.state_id.id
            v['city_id'] = city.id

        print "##################", v
        return {'value': v}

    def onchange_name(self, cr, uid, ids, name, contact_name, title, is_client, context=None):
        """This function updates the name of lead with contact_name and title
        """
        res = {'value': {}}
        title_name = ""
        if is_client == 'client':
            if title:
                title_name = self.pool.get(
                    'res.partner.title').browse(cr, uid, title).name
            if contact_name:
                res['value']['name'] = title_name + " " + contact_name
            else:
                res['value']['name'] = title_name
        return res


class query_reason(osv.osv):
    _name = "mt.query.reason"

    _columns = {
        'name': fields.char("Query reason", required=True),
    }


class service_type(osv.osv):
    _name = "mt.service.type"

    _columns = {
        'name': fields.char("Service Type", required=True),
        'code': fields.char("Code", required=True)
    }


class old_stage(osv.osv):
    _name = "mt.old.stage"
    _columns = {
        'name': fields.char("Old stage", required=True),
    }


class new_stage(osv.osv):
    _name = "mt.new.stage"
    _columns = {
        'name': fields.char("New stage", required=True),
    }


class new_stage2(osv.osv):
    _name = "mt.client.type"
    _columns = {
        'name': fields.char("Type Client", required=True),
    }


class case_categ(osv.osv):
    _name = "mt.case.categ"
    _columns = {
        'name': fields.char("Category", required=True),
    }


class progress_action(osv.osv):
    _name = "mt.progress.action"
    _columns = {
        'name': fields.char("Progress action", required=True),
        #         'request_stage_id' : fields.many2one('crm.case.stage', 'Request stage'),
    }


class Reject_reason(osv.osv):
    _name = "mt.reject.reason"
    _columns = {
        'name': fields.char("Reason", required=True),
        'request_stage_id': fields.many2one('crm.case.stage', 'Request stage'),
    }


#     def _default_stage_id(self, cr, uid, context=None):
#          if context is None:
#              context = {}
#          mod_obj = self.pool.get('ir.model.data')
#          ref = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead4')
#          if ref[1]:
#                 return ref[1]
#          return False
#
#     _defaults = {
#                  'request_stage_id': _default_stage_id
#                  }


class mt_property_type(osv.osv):
    _name = "mt.property.type"
    _columns = {
        'name': fields.char("Property Type", required=True, translate=True),
        'category_pro': fields.many2one('mt.case.categ', 'Division'),
    }


class mt_neighborhood(osv.osv):
    _name = "mt.neighborhood"
    _description = "Neighborhood"

    _columns = {
        'name': fields.char('Name', size=64, translate=True),
    }


mt_neighborhood()


# -------------------------------------------------------------------------------
# mt_bedrooms
# -------------------------------------------------------------------------------
class mt_bedrooms(osv.osv):
    _name = "mt.bedrooms"

    _columns = {
        'name': fields.char("Bedrooms No", required=True),
    }


# -------------------------------------------------------------------------------
# mt_bathrooms
# -------------------------------------------------------------------------------
class mt_bathrooms(osv.osv):
    _name = "mt.bathrooms"

    _columns = {
        'name': fields.char("Bathrooms No", required=True),
    }


# -------------------------------------------------------------------------------
# crm_phone
# -------------------------------------------------------------------------------
class crm_phone(osv.osv):
    _inherit = "crm.phonecall"

    def on_change_client_request(self, cr, uid, ids, opportunity_id):
        if opportunity_id:
            obj = self.pool.get("crm.lead").browse(cr, uid, opportunity_id)
            return {'value': {
                'phone': obj.phone,
                'service_type': obj.service_ids,
            }}

    _columns = {
        'opportunity_id': fields.many2one('crm.lead', 'Lead/Client Request'),
        'phone': fields.char('Phone'),
        'service_type': fields.selection(
            [('buy', 'Buy'), ('sale', 'Sale'), ('buyer_rent', 'Rent (Buyer)'), ('seller_rent', 'Rent (Seller)'),
             ('share_buyer', 'Share (Buyer)'), ('share_seller', 'Share (Seller)')], 'Service Type', translate=True),
        'class': fields.selection(
            [('public', 'Public'), ('private', 'Private'), ('confidential', 'Public for Employees')], 'Privacy'),
    }


# -------------------------------------------------------------------------------
# activity_type
# -------------------------------------------------------------------------------
class activity_type(osv.osv):
    _name = "activity.type"

    _columns = {
        'name': fields.char("Type", required=True),
    }


# -------------------------------------------------------------------------------
# event
# -------------------------------------------------------------------------------
class event(osv.osv):
    _inherit = "calendar.event"

    def on_change_type(self, cr, uid, ids, type_id):
        obj = self.pool.get("activity.type").browse(cr, uid, type_id)
        return {'value': {'name': obj.name,
                          }}

    def on_change_client_request(self, cr, uid, ids, opportunity_id):
        if opportunity_id:
            obj = self.pool.get("crm.lead").browse(cr, uid, opportunity_id)
            return {'value': {'contact_name': obj.contact_name,
                              'phone': obj.phone,
                              'service_type': obj.service_ids,
                              'category': obj.category.id,
                              }}


    @api.multi
    def open_closed(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])

        view_id_form = self.env.ref('calendar.view_calendar_event_form')
        view_id_tree = self.env.ref('calendar.view_calendar_event_tree')

        search_view_id = self.env.ref('calendar.view_calendar_event_search')

        # search_view_id = self.env.ref('mt_crm_brokerage_manager.crm_event_tree_view_inherit')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        # partns = self.env['res.partner'].search([('user_id.partner_id', 'in', lst)])
        actvities = self.sudo().search([('user_id','in',lst)])
        print "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW", actvities ,lst
        return {
            'name': _('Client Activity'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,calendar',
            'res_model': 'calendar.event',
            'views': [(view_id_tree.id, 'tree'), (view_id_form.id, 'form'), (False, 'calendar')],
            'view_id': view_id_form.id,
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', actvities.ids)]
        }

    @api.model
    @api.multi
    def open_closed_resediential(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')])
        view_id_form = self.env.ref('calendar.view_calendar_event_form')
        view_id_tree = self.env.ref('calendar.view_calendar_event_tree')

        search_view_id = self.env.ref('calendar.view_calendar_event_search')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        partns = self.sudo().search([('user_id', 'in', lst)])
        print "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW", partns, records,lst
        return {
            'name': _('Client Activity'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,calendar',
            'res_model': 'calendar.event',
            'views': [(view_id_tree.id, 'tree'), (view_id_form.id, 'form'), (False, 'calendar')],
            'view_id': view_id_form.id,
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', partns.ids)]
        }

    @api.model
    @api.multi
    def open_closed_commercial(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
        view_id_form = self.env.ref('calendar.view_calendar_event_form')
        view_id_tree = self.env.ref('calendar.view_calendar_event_tree')

        search_view_id = self.env.ref('calendar.view_calendar_event_search')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        partns = self.sudo().search([('user_id', 'in', lst)])
        print "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW", partns, records,lst
        return {
            'name': _('Client Activity'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,calendar',
            'res_model': 'calendar.event',
            'views': [(view_id_tree.id, 'tree'), (view_id_form.id, 'form'), (False, 'calendar')],
            'view_id': view_id_form.id,
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', partns.ids)]
        }

    @api.model
    @api.multi
    def open_closed_ceo(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
        view_id_form = self.env.ref('calendar.view_calendar_event_form')
        view_id_tree = self.env.ref('calendar.view_calendar_event_tree')

        search_view_id = self.env.ref('calendar.view_calendar_event_search')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        partns = self.sudo().search([('user_id', 'in', lst)])
        print "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW", partns, records, lst
        return {
            'name': _('Client Activity'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,calendar',
            'res_model': 'calendar.event',
            'views': [(view_id_tree.id, 'tree'), (view_id_form.id, 'form'), (False, 'calendar')],
            'view_id': view_id_form.id,
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', partns.ids)]
        }

    # @api.multi
    # @api.onchange('partner_ids')
    def onchange_partner_ids(self, cr, uid, ids, value, context=None):
        print "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEeeee"
        lis = []
        pars = []
        cats2 =[]
        catego =[]
        propr =[]
        res = super(event, self).onchange_partner_ids(cr, uid, ids, value, context=context)
        curre_user = self.pool.get('res.users').browse(cr, uid, uid)
        if curre_user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            sale_team = self.pool.get('crm.case.section').search(cr, uid, [('member_ids', '=', uid)])

            for each in self.pool.get('crm.case.section').browse(cr, uid, sale_team):
                for user in each.member_ids:
                    if user.has_group(
                            'mt_crm_brokerage_manager.group_commercial_buyer_agent_id') and curre_user.has_group(
                            'mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
                        lis.append(user.id)
                        categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')],
                                                                           context=context)
                        cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                        catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                          context=context)
                        propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

                    elif user.has_group(
                            'mt_crm_brokerage_manager.group_commercial_seller_agent_id') and curre_user.has_group(
                            'mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
                        lis.append(user.id)
                        categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')],
                                                                           context=context)
                        cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                        catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                          context=context)
                        propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

                    elif user.has_group(
                            'mt_crm_brokerage_manager.group_residential_buyer_agent_id') and curre_user.has_group(
                            'mt_crm_brokerage_manager.group_residential_buyer_agent_id'):
                        lis.append(user.id)
                        categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')],
                                                                           context=context)
                        cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                        catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                          context=context)
                        propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

                    elif user.has_group(
                            'mt_crm_brokerage_manager.group_residential_seller_agent_id') and curre_user.has_group(
                            'mt_crm_brokerage_manager.group_residential_seller_agent_id'):
                        lis.append(user.id)
                        categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')],
                                                                           context=context)
                        cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
                        catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                                          context=context)
                        propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', lis)])
            leads = self.pool.get('crm.lead').search(cr, uid, [('user_id', 'in', lis)])
            print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$44", lis, pars ,leads
            res['domain'] = {'opportunity_id': [('id', 'in', leads)],
                             'partner_ids': [('id', 'in', pars)],
                             'category': [('id', 'in', cats2)]}

        if curre_user.has_group('mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
            sale_team = self.pool.get('crm.case.section').search(cr, uid, [('user_id', '=', uid)])
            for each in self.pool.get('crm.case.section').browse(cr, uid, sale_team):
                for user in each.member_ids:
                    lis.append(user.id)
            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', lis)])
            leads = self.pool.get('crm.lead').search(cr, uid, [('user_id', 'in', lis)])
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            print "SSSSSSSSSSS", lis, "AAAAAAAAAAAAAAAAAAAAAA", pars,leads
            res['domain'] = {'opportunity_id': [('id', 'in', leads)],
                             'partner_ids': [('id', 'in', pars)],
                             'category': [('id', 'in', cats2)]}

        if curre_user.has_group('mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
            sale_team = self.pool.get('crm.case.section').search(cr, uid, [('user_id', '=', uid)])
            for each in self.pool.get('crm.case.section').browse(cr, uid, sale_team):
                for user in each.member_ids:
                    lis.append(user.id)
            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', lis)])
            leads = self.pool.get('crm.lead').search(cr, uid, [('user_id', 'in', lis)])
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            print "SSSSSSSSSSS", lis, "AAAAAAAAAAAAAAAAAAAAAA", pars, leads
            res['domain'] = {'opportunity_id': [('id', 'in', leads)],
                             'partner_ids': [('id', 'in', pars)],
                             'category': [('id', 'in', cats2)]}

        if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential') or curre_user.has_group('itsys_era_coordinator.group_division_head_resedential'):
            sale_team = self.pool.get('crm.case.section').search(cr, uid,
                                                                 [('category_id.categ_type', '=', 'residential')])
            for each in self.pool.get('crm.case.section').browse(cr, uid, sale_team):
                for user in each.member_ids:
                    lis.append(user.id)

            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', lis)])
            leads = self.pool.get('crm.lead').search(cr, uid, [('user_id', 'in', lis)])

            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)


            print "SSSSSSSSSSSSSSSSSSS", lis, "AAAAAAAAAAAAAAAAAAAAAA", pars ,leads
            res['domain'] = {'opportunity_id': [('id', 'in', leads)],
                             'partner_ids': [('id', 'in', pars)],
                             'category': [('id', 'in', cats2)]}

        if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial') or curre_user.has_group('itsys_era_coordinator.group_division_head_commercial'):
            sale_team = self.pool.get('crm.case.section').search(cr, uid,
                                                                 [('category_id.categ_type', '=', 'commercial')])
            for each in self.pool.get('crm.case.section').browse(cr, uid, sale_team):
                for user in each.member_ids:
                    lis.append(user.id)

            pars = self.pool.get('res.partner').search(cr, uid, [('user_id', 'in', lis)])
            leads = self.pool.get('crm.lead').search(cr, uid, [('user_id', 'in', lis)])

            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)
            print "QQQQQQQQQQQQQQQQ", lis, "TTTTTTTTTTTTTTTTTTTTTTT", pars,leads
            res['domain'] = {'opportunity_id': [('id', 'in', leads)],
                             'partner_ids': [('id', 'in', pars)],
                             'category': [('id', 'in', cats2)]}

            print res
        return res



    _columns = {
        'type_id': fields.many2one('activity.type', 'Activity Type'),
        'contact_name': fields.char('Contact Name'),
        'category': fields.many2one('mt.case.categ', 'Category List'),
        'phone': fields.char('Phone'),
        'is_notif': fields.boolean('Notif'),
        'service_type': fields.selection(
            [('buy', 'Buy'), ('sale', 'Sale'), ('buyer_rent', 'Rent (Buyer)'), ('seller_rent', 'Rent (Seller)'),
             ('share_buyer', 'Share (Buyer)'), ('share_seller', 'Share (Seller)')], 'Service Type', translate=True),

    }

    _defaults = {
        'class': 'private'

    }


event()


# -------------------------------------------------------------------------------
# mt_licenses
# -------------------------------------------------------------------------------
class mt_licenses(osv.osv):
    _name = "mt.licenses"

    _columns = {
        'name': fields.char("License Name", required=True),
    }


# -------------------------------------------------------------------------
# ir_ui_menu
# -------------------------------------------------------------------------
class ir_ui_menu(osv.Model):
    _inherit = "ir.ui.menu"
    _columns = {
        'active': fields.boolean('Active'),
    }
    _defaults = {
        'active': True,
    }


ir_ui_menu()
