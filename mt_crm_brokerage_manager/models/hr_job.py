# -*- coding: utf-8 -*-
import datetime
from mako.runtime import _inherit_from

from openerp.addons.base_geolocalize.models.res_partner import geo_find, geo_query_address
from openerp.osv import fields
from openerp.osv import osv
from openerp.tools.translate import _


class hr_job(osv.osv):
    _inherit = 'hr.job'
    
    

    _columns = {
        'type_job': fields.selection([('careers', 'Careers'), ('internships', 'Internships')], 'Type'),
    }
    
    _defaults = {
                 'type_job': 'careers',

    }
    
hr_job()

