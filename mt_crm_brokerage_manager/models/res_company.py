# -*- coding: utf-8 -*-
import datetime
from mako.runtime import _inherit_from

from openerp.osv import fields
from openerp.osv import osv
from openerp.tools.translate import _


#-------------------------------------------------------------------------------
# res_country
#-------------------------------------------------------------------------------
class res_country(osv.osv):
    _inherit = 'res.country'   
   
    _columns = {
                'phone_code': fields.char('Phone Code'),
                'lenght': fields.integer('Length'),
    }

class mt_city(osv.osv):
    _inherit = 'mt.city'   
   
    _columns = {
                'neighborhood_ids': fields.many2many('mt.neighborhood','city_neigh_rel','city_id1','city_id2','Neighborhood'),
    }
    
  