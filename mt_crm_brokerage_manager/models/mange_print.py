from openerp import api, fields, models


class CrmLead(models.Model):
    _inherit = 'crm.lead'
    is_agent = fields.Boolean(compute='bool_get_agent')

    @api.one
    def bool_get_agent(self):
        for record in self:
            if self.env.user.has_group('mt_crm_brokerage_manager.group_agent_id') and record.stage_id.code != 'Wo':
                record.is_agent = True
            else:
                record.is_agent = False

    @api.multi
    def lead_print(self):
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.env['report'].get_action(self, 'mt_crm_brokerage_manager.report_leads')

    @api.multi
    def request_print(self):
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.env['report'].get_action(self, 'mt_crm_brokerage_manager.report_client_request')


CrmLead()
