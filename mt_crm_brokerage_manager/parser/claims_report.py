# -*- coding: utf-8 -*-
from openerp.osv import osv
from openerp.report import report_sxw


class claims(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        self.ids_to_print = context.get('active_ids', False)
        super(claims, self).__init__(cr, uid, name, context=context)
        active_id = context.get('active_ids', False)
        self.localcontext.update({
        'get_line_claims' : self._get_line_claims,
         
        })
   
    
    
    
    def _get_line_claims(self):
        claims_obj=self.pool.get('crm.claim')
        
        res=[]
        for claim in claims_obj.browse(self.cr,self.uid,self.ids_to_print) :
            line={
                'name':claim.name ,
                'partner_id': claim.partner_id.name,
                'partner_phone': claim.partner_phone,
                'category': claim.category.name,
#                 'class': activity.class,
                'ref': claim.ref.name,
                'user_id': claim.user_id.name,
                'date': claim.date,
                'stage_id': claim.stage_id.name,
                'date_action_next': claim.date_action_next,
                'action_next': claim.action_next,
                'categ_id': claim.categ_id.name,
                  }
            res.append(line)
        return   res
      
class report_leads(osv.AbstractModel):
    _name = 'report.mt_crm_brokerage_manager.report_claims'
    _inherit = 'report.abstract_report'
    _template = 'mt_crm_brokerage_manager.report_claims'
    _wrapped_report_class = claims