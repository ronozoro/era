# -*- coding: utf-8 -*-
from openerp.osv import osv
from openerp.report import report_sxw


class crm_leads(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        self.ids_to_print = context.get('active_ids', False)
        super(crm_leads, self).__init__(cr, uid, name, context=context)
        active_id = context.get('active_ids', False)
        self.localcontext.update({
        'get_line_leads' : self._get_line_leads,
         
        })
   
    
    
    
    def _get_line_leads(self):
        lead_obj=self.pool.get('crm.lead')
        
        res=[]
        for lead in lead_obj.browse(self.cr,self.uid,self.ids_to_print) :
            line={
                'contact_name':lead.contact_name ,
                'phone': lead.phone,
                'email_from': lead.email_from,
                'user_id': lead.user_id.name,
                'medium_id2': lead.medium_id2.name,
                'category': lead.category.name,
                'stage_id': lead.stage_id.name,
                'create_date': lead.create_date,
                  }
            res.append(line)
        return   res
      
class report_leads(osv.AbstractModel):
    _name = 'report.mt_crm_brokerage_manager.report_leads'
    _inherit = 'report.abstract_report'
    _template = 'mt_crm_brokerage_manager.report_leads'
    _wrapped_report_class = crm_leads  