# -*- coding: utf-8 -*-
from openerp.osv import osv
from openerp.report import report_sxw


class phonecalls(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        self.ids_to_print = context.get('active_ids', False)
        super(phonecalls, self).__init__(cr, uid, name, context=context)
        active_id = context.get('active_ids', False)
        self.localcontext.update({
        'get_line_phonecall' : self._get_line_phonecall,
         
        })
   
    
    
    
    def _get_line_phonecall(self):
        phonecall_obj=self.pool.get('crm.phonecall')
        
        res=[]
        for call in phonecall_obj.browse(self.cr,self.uid,self.ids_to_print) :
            line={
                'date':call.date ,
                'name': call.name,
                'partner_id': call.partner_id.name,
                'user_id': call.user_id.name,
                  }
            res.append(line)
        return   res
      
class report_phone_calls(osv.AbstractModel):
    _name = 'report.mt_crm_brokerage_manager.report_phonecall'
    _inherit = 'report.abstract_report'
    _template = 'mt_crm_brokerage_manager.report_phonecall'
    _wrapped_report_class = phonecalls