# -*- coding: utf-8 -*-
from openerp.osv import osv
from openerp.report import report_sxw


class crm_client_request(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        self.ids_to_print = context.get('active_ids', False)
        super(crm_client_request, self).__init__(cr, uid, name, context=context)
        active_id = context.get('active_ids', False)
        self.localcontext.update({
        'get_line_client_request' : self._get_line_client_request,
         
        })
   
    
    
    
    def _get_line_client_request(self):
        client_request_obj=self.pool.get('crm.lead')
        
        res=[]
        for lead in client_request_obj.browse(self.cr,self.uid,self.ids_to_print) :
            line={
                'create_date':lead.create_date ,
                'name': lead.name,
                'phone': lead.phone,
                'category': lead.category.name,
                'service_ids': lead.service_ids,
                'district_ids': lead.district_ids.name,
                'neighborhood_ids': lead.neighborhood_ids.name,
                'property_ids': lead.property_ids.name,
                'budget': lead.budget,
                'stage_id': lead.stage_id.name,
                'email_from': lead.email_from,
                'project_id': lead.project_id.name,
                'property': lead.property.name,
                'user_id': lead.user_id.name,
                  }
            res.append(line)
        return   res
      
class report_leads(osv.AbstractModel):
    _name = 'report.mt_crm_brokerage_manager.report_client_request'
    _inherit = 'report.abstract_report'
    _template = 'mt_crm_brokerage_manager.report_client_request'
    _wrapped_report_class = crm_client_request