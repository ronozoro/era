# -*- coding: utf-8 -*-
from openerp.osv import osv
from openerp.report import report_sxw


class client_activity(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        self.ids_to_print = context.get('active_ids', False)
        super(client_activity, self).__init__(cr, uid, name, context=context)
        active_id = context.get('active_ids', False)
        self.localcontext.update({
        'get_line_client_activity' : self._get_line_client_activity,
         
        })
   
    
    
    
    def _get_line_client_activity(self):
        client_activity_obj=self.pool.get('calendar.event')
        
        res=[]
        for activity in client_activity_obj.browse(self.cr,self.uid,self.ids_to_print) :
            line={
                'name':activity.name ,
                'contact_name': activity.contact_name,
                'phone': activity.phone,
                'service_type': activity.service_type,
                'start_date': activity.start_date,
                'user_id': activity.user_id.name,
                'location': activity.location,
                'show_as': activity.show_as,
#                 'class': activity.class,
                'duration': activity.duration,
                  }
            res.append(line)
        return   res
      
class report_leads(osv.AbstractModel):
    _name = 'report.mt_crm_brokerage_manager.report_client_activity'
    _inherit = 'report.abstract_report'
    _template = 'mt_crm_brokerage_manager.report_client_activity'
    _wrapped_report_class = client_activity