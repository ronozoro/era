# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (C) 2004-2012 OpenERP S.A. (<http://openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import openerp
from openerp.osv import fields, osv
from docutils.nodes import Invisible
from dateutil.relativedelta import relativedelta
import time
import datetime 

class crm_lead_appro(osv.osv_memory):
    _inherit = 'crm.lead2opportunity.partner'
    _description = 'Lead To Opportunity Partner'

    # crm_lead_id = fields.Many2one('crm.lead', string="leads")
    _columns = {
        'crm_lead_id': fields.many2one('crm.lead',string="leads"),
    }
    def action_apply(self, cr, uid, ids, context=None):
        """
        Convert lead to opportunity or merge lead and opportunity and open
        the freshly created opportunity view.
        """
       
#         res= super(crm_lead_appro,self).action_apply( cr, uid, ids, context)
        mod_obj = self.pool.get('ir.model.data')
        stage = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead2')
        if stage :data=stage[1]
        
        if context is None:
            context = {}
 
        lead_obj = self.pool.get('crm.lead')
        lead_ids = context.get('active_ids', [])
        print "contexttttttttttttttt",context ,"lead_idsssssssssssss",lead_ids
        lead_type = self.pool.get('crm.lead').browse(cr, uid, lead_ids, context=context)
        print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55",lead_type
        print'---lead_ids-----',lead_ids
        l = lead_obj.browse(cr,uid,lead_ids[0],context)
        print'---l.medium_id2-----',l.medium_id2
        print'---l.medium_id2.id-----',l.medium_id2.id
        w = self.browse(cr, uid, ids, context=context)[0]

        opp_ids = [o.id for o in w.opportunity_ids]
        if w.name == 'merge':
            lead_id = lead_obj.merge_opportunity(cr, uid, opp_ids, context=context)
            print'----lead_id-----',lead_id
            lead_ids = [lead_id]
            lead = lead_obj.read(cr, uid, lead_id, ['type', 'user_id'], context=context)
            if lead['type'] == "lead":
                if lead_type.category.name == 'Residential':
                    print "Residential"
                    lead_type.write({'type_lead': '0'})
                    context.update({'type_lead': '0'})
                    context = dict(context, active_ids=lead_ids)
                    self._convert_opportunity(cr, uid, ids, {'lead_ids': lead_ids, 'user_ids': [w.user_id.id], 'section_id': w.section_id.id}, context=context)
                elif lead_type.category.name == 'Commercial':
                    print "Commercial"
                    lead_type.write({'type_lead': '1'})

                    context.update({'type_lead': '1'})
                    context = dict(context, active_ids=lead_ids)
                    self._convert_opportunity(cr, uid, ids, {'lead_ids': lead_ids, 'user_ids': [w.user_id.id],'section_id': w.section_id.id}, context=context)


            elif not context.get('no_force_assignation') or not lead['user_id']:
                if lead_type.category.name == 'Residential':
                    print "Residential"
                    lead_type.write({'type_lead': '0'})
                    context.update({'type_lead': '0'})

                    lead_obj.write(cr, uid, lead_id, {'user_id': w.user_id.id,
                                                      'section_id': w.section_id.id,
                                                      }, context=context)

                elif lead_type.category.name == 'Commercial':
                    print "Commercial"
                    lead_type.write({'type_lead': '1'})

                    context.update({'type_lead': '1'})
                    lead_obj.write(cr, uid, lead_id, {'user_id': w.user_id.id,
                                                      'section_id': w.section_id.id
                                                      }, context=context)
        else:
            print "ahmmoooooooooooooooooooooooooooooos",lead_type.category.name
            if lead_type.category.name == 'Residential':
                print "Residential"
                lead_type.write({'type_lead': '0'})
                context.update({'type_lead': '0'})
                self._convert_opportunity(cr, uid, ids, {'lead_ids': lead_ids, 'user_ids': [w.user_id.id],
                                                         'section_id': w.section_id.id,
                                                         }, context=context)
                # res = {'domain': {'property_type_id': [('id', 'in', propr.ids)]}, 'value': {'type_lead': '0'}}

            elif lead_type.category.name == 'Commercial':
                print "Commercial"
                lead_type.write({'type_lead': '1'})

                context.update({'type_lead': '1'})
                # res = {'domain': {'property_type_id': [('id', 'in', propr.ids)]}, 'value': {'type_lead': '1'}}

                self._convert_opportunity(cr, uid, ids, {'lead_ids': lead_ids, 'user_ids': [w.user_id.id],
                                                         'section_id': w.section_id.id,
                                                         }, context=context)
            print "saaaaaaaaaaaaaaaa555555555555555555555555aaaaid", context
        
        lead = lead_obj.browse(cr,uid,lead_ids[0],context)
        if stage :
            data=stage[1]
            lead_obj.write(cr, uid, lead_ids, { 'stage_id':data,
                                                     }, context=context)
        
        
        #create notif
            for rec in self.pool.get('crm.lead').browse(cr,uid,lead_ids[0],context):
                if rec.manager_id :
#                     user = 'user'
#                     if rec.user_id:
#                         user = self.pool.get('res.users').browse(cr, uid, rec.user_id.id, context=context)
                    user_ids=[]
                    alarm_ids=[]
                    event_obj = self.pool.get('calendar.event')
                    models_data = self.pool.get('ir.model.data')
                    alarm_notif_id = models_data.get_object_reference(cr,uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
                    manager = self.pool.get('res.users').browse(cr, uid, rec.manager_id.id, context=context)
                    user_ids.append(manager.partner_id.id)
                    user_ids.append(w.user_id.partner_id.id)
                    # manager_karim = self.pool.get('res.users').search(cr, uid, [('login', 'ilike', 'karim.fahim@era-egypt.com')], context=context)
                    # manager_karim_id = self.pool.get('res.users').browse(cr, uid, manager_karim[0], context=context)
                    # user_ids.append(manager_karim_id.partner_id.id)
                    # alarm_ids.append(alarm_notif_id)
                    # date_alert = datetime.datetime.now()+relativedelta(minutes=+ 1)
                    # date_fin = datetime.datetime.now()+relativedelta(minutes=+ 20)
                    # event_values = {
                    #              'name' :"New request assigned to " + w.user_id.name,
                    #              'start_datetime':date_alert.strftime('%Y-%m-%d %H:%M:%S') ,
                    #              'stop_datetime':date_fin.strftime('%Y-%m-%d %H:%M:%S') ,
                    #              'alarm_ids':[(6,0, alarm_ids)],
                    #              'partner_ids':[(6,0,user_ids)],
                    #              'is_notif':True,
                    #             }
                    # event_id = event_obj.create(cr, uid,event_values, context=context )
        print "saaaaaaaaaaaaaaaaaaaaid",context
        
        return self.pool.get('crm.lead').redirect_opportunity_view(cr, uid, lead_ids[0], context=context)



#     _columns = {
#         'name': fields.selection([
#                 ('convert', 'Convert Convert to Client Request'),
#             ], 'Conversion Action', required=True),
#     }
    
    
#     _defaults = {
#         'name': 'convert',
#          
#     }
# 
#     
    
    
#     def on_change_type_facturation(self, cr, uid, ids, mt_type_forfait_regie, context=None):
#        # sol = self.pool.get('sale.order.line').browse(cr, uid, mt_type_forfait_regie,nbr_heure_exp, context=context)
#         mod_obj = self.pool.get('ir.model.data')
#         uom = mod_obj.get_object_reference(cr, uid, 'mt_offre_mission_audit', 'product_uom_forfait0')
#         print uom
#         if mt_type_forfait_regie == 'forfait':
#              return {'value': {
#                                'product_uom': uom[1],
#                                'product_uom_qty' : 1,
# 
#                           }}
#         else:
#              return {'value': {
#                               'product_uom': False,
#                                              
# 
# 
#                           }}
             
crm_lead_appro()
  