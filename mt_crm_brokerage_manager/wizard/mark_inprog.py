# -*- coding: utf-8 -*-


from openerp.osv import fields, osv
from openerp.tools.translate import _
import uuid
import time
import datetime 
import openerp.addons.decimal_precision as dp

class mt_mark_inprog(osv.osv_memory):
      
    _name = 'mt.mark_inprog'
     
     
    def inprog_button_ok(self, cr, uid, id, context=None):
        desc_obj = self.pool.get('crm.lead')
        message = self.browse(cr, uid, id, context=context)
        mod_obj = self.pool.get('ir.model.data')
        stage_lost = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead3')
        name1=message.name
#         if stage_lost :
#             data=stage_lost[1]        
#             self.write(cr, uid, ids, {'stage_id': data}, context=context)
#         stages = self.pool.get('crm.case.stage').search(cr,uid,[('code','=','InPro')])
        id = context.get('active_id',False)
        vals = {
                'stage_id':    stage_lost[1],
                 'progress_id': message.progress_id.id,
                 'request_code': 'InPro',
                 'comment':name1,
                 'date_inprogress':datetime.datetime.now(),
                }  
        if id :   
            desc_obj.write(cr,uid,id,vals)  
    
#   
    _columns ={
                
                
                'progress_id':fields.many2one('mt.progress.action', 'Reason'),
                'name': fields.text("Comment"),
               }
    
    
   
     
     
 
mt_mark_inprog()    
          