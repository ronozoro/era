# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from datetime import datetime

import time
import _strptime
import time 

DT_FMT = '%Y-%m-%d %H:%M:%S'

class edit_many_request(osv.osv):
#     _inherit='crm.lead'
    _name = 'edit.many_request'
     
    
    
    def create(self, cr, uid, vals, context=None):
        id = super(edit_many_request, self).create(cr, uid, vals, context=context)
        leads_ids = context.get('active_ids', [])
        print'----leads_ids--',leads_ids
        for lead in leads_ids :
            if vals.get('assign', False):
                assign = vals.get('assign', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'assign': assign}, context=context)
            if vals.get('user_id', False):
                user_id = vals.get('user_id', False)
                print'----user_id--',user_id
                print'----lead--',lead
                self.pool.get('crm.lead').write(cr, uid, lead, {'user_id': user_id}, context=context)
            if vals.get('section_id', False):
                section_id = vals.get('section_id', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'section_id': section_id}, context=context)
            if vals.get('medium_id2', False):
                medium_id2 = vals.get('medium_id2', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'medium_id2': medium_id2}, context=context)
            if vals.get('category', False):
                category = vals.get('category', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'category': category}, context=context)
            if vals.get('service_ids', False):
                service_ids = vals.get('service_ids', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'service_ids': service_ids}, context=context)
            if vals.get('project_id', False):
                project_id = vals.get('project_id', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'project_id': project_id}, context=context)
            if vals.get('property_ids', False):
                property_ids = vals.get('property_ids', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'property_ids': property_ids}, context=context)
            if vals.get('agreement', False):
                agreement = vals.get('agreement', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'agreement': agreement}, context=context)
            if vals.get('payment', False):
                payment = vals.get('payment', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'payment': payment}, context=context)
            if vals.get('district_ids', False):
                district_ids = vals.get('district_ids', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'district_ids': district_ids}, context=context)
            if vals.get('neighborhood_ids', False):
                neighborhood_ids = vals.get('neighborhood_ids', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'neighborhood_ids': neighborhood_ids}, context=context)
            if vals.get('bathrooms_no_ids', False):
                bathrooms_no_ids = vals.get('bathrooms_no_ids', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'bathrooms_no_ids': bathrooms_no_ids}, context=context)
            if vals.get('bedrooms_no_ids', False):
                bedrooms_no_ids = vals.get('bedrooms_no_ids', False)
                self.pool.get('crm.lead').write(cr, uid, lead, {'bedrooms_no_ids': bedrooms_no_ids}, context=context)
            
        return id
    
    
    
    def action_button_confirm(self, cr, uid, ids, context=None):
        return True
     
    
    _columns ={
               'user_id': fields.many2one('res.users', 'Assign To', select=True, ),
               'assign': fields.many2one("res.users",'Entered By'),
               'section_id': fields.many2one('crm.case.section', 'Sales Team',select=True),
               'medium_id2': fields.many2one('crm.tracking.medium', 'Lead Channel', help="This is the method of delivery. Ex: Postcard, Email, or Banner Ad", oldname='channel_id'),
               'category': fields.many2one('mt.case.categ', 'Division' ),
               'service_ids': fields.selection([ ('buy', 'Buy'), ('sale', 'Sale'),('buyer_rent', 'Rent (Buyer)'), ('seller_rent', 'Rent (Seller)'), ('share_buyer', 'Share (Buyer)'), ('share_seller', 'Share (Seller)')], 'Service Type' , translate=True),
               'project_id': fields.many2one('product.template', string="Project"),
               'property_ids': fields.many2one('mt.property.type', 'Property Type'),
               'agreement': fields.selection([('none', 'None'), ('normal', 'Normal'), ('exclusive', 'Exclusive'),('standard', ' Standard')], 'Agreement' ),
               'payment': fields.selection([('cash', 'Cash'), ('installments', 'Installments')], 'Payment type'),
               'district_ids': fields.many2one('mt.city', string='District'),
               'neighborhood_ids': fields.many2one('mt.neighborhood', string='Neighborhood'),
               'bedrooms_no_ids': fields.many2one('mt.bedrooms', string='No. of Bedrooms'),
               'bathrooms_no_ids': fields.many2one('mt.bathrooms', string='No. of Bathrooms'),
               }
     
     
 
          