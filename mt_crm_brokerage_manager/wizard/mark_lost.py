# -*- coding: utf-8 -*-


from openerp.osv import fields, osv
from openerp.tools.translate import _
import uuid
import time
import datetime 
import openerp.addons.decimal_precision as dp
from dateutil.relativedelta import relativedelta


class mt_mark_lost(osv.osv_memory):
      
    _name = 'mt.mark_lost'
     
     
    def lost_button_ok(self, cr, uid, id, context=None):
        desc_obj = self.pool.get('crm.lead')
        message = self.browse(cr, uid, id, context=context)
        stages = self.pool.get('crm.case.stage').search(cr,uid,[('code','=','Lo')])
        name1=message.name
        id = context.get('active_id',False)
        vals = {
                 'stage_id':    stages[0],
                 'reject_id': message.reject_id.id,
                 'request_code': 'Lo',
                 'comment':name1,
                }  
        if id :   
            #create notif
            for rec in self.pool.get('crm.lead').browse(cr,uid,id,context):
                if rec.manager_id :
                    user = 'user'
                    if rec.user_id:
                        user = self.pool.get('res.users').browse(cr, uid, rec.user_id.id, context=context)
                    user_ids=[]
                    alarm_ids=[]
                    event_obj = self.pool.get('calendar.event')
                    models_data = self.pool.get('ir.model.data')
                    alarm_notif_id = models_data.get_object_reference(cr,uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
                    manager = self.pool.get('res.users').browse(cr, uid, rec.manager_id.id, context=context)
                    user_ids.append(manager.partner_id.id)
                    manager_karim = self.pool.get('res.users').search(cr, uid, [('login', 'ilike', 'karim.fahim@era-egypt.com')], context=context)
                    # manager_karim_id = self.pool.get('res.users').browse(cr, uid, manager_karim[0], context=context)
                    # user_ids.append(manager_karim_id.partner_id.id)
                    # alarm_ids.append(alarm_notif_id)
                    # date_alert = datetime.datetime.now()+relativedelta(minutes=+ 1)
                    # date_fin = datetime.datetime.now()+relativedelta(minutes=+ 20)
                    # event_values = {
                    #              'name' :"the request assigned to " + user.name +' have marked lost',
                    #              'start_datetime':date_alert.strftime('%Y-%m-%d %H:%M:%S') ,
                    #              'stop_datetime':date_fin.strftime('%Y-%m-%d %H:%M:%S') ,
                    #              'alarm_ids':[(6,0, alarm_ids)],
                    #              'partner_ids':[(6,0,user_ids)],
                    #              'is_notif':True,
                    #             }
                    # event_id = event_obj.create(cr, uid,event_values, context=context )
            desc_obj.write(cr,uid,id,vals)  
    
#   
    _columns ={
                
                
                'reject_id':fields.many2one('mt.reject.reason', 'Reason'),
                'name': fields.text("Comment"),
                'stage_id' : fields.many2one('crm.case.stage', 'stage'),

               }
    
    
    def get_new_stage(self, cr, uid):       
        mod_obj = self.pool.get('ir.model.data')
        stage = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead8')
        if stage :
            return stage[1]        
        return False   
    
    
    _defaults = {
#     'request_stage_id':_default_request_stage,                  
    'stage_id':lambda self, cr, uid, context:self.get_new_stage(cr, uid),
} 
mt_mark_lost()    
          