# -*- coding: utf-8 -*-
##############################################################################

import openerp
from openerp.osv import fields, osv
from dateutil.relativedelta import relativedelta
import time
import datetime 

class lead_message(osv.osv_memory):
    _name = 'lead.message' 
    
    _columns = { 
        'name': fields.text("Comment" , required=True),
        'type': fields.selection([('won','Won'), ('lost','Lost')], 'Type'),
        'reject_id':fields.many2one('mt.reject.reason', 'Reason' ,required=True),
        'stage_id' : fields.many2one('crm.case.stage', 'stage'),
        
 }
    
    
    def get_new_stage(self, cr, uid):       
        mod_obj = self.pool.get('ir.model.data')
        stage = mod_obj.get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'crm_stage_lead4')
        if stage :
            return stage[1]        
        return False   
    
    
    _defaults = {
#     'request_stage_id':_default_request_stage,                  
    'stage_id':lambda self, cr, uid, context:self.get_new_stage(cr, uid),
} 
    def add_lead_message(self, cr, uid, ids, context=None):
        res = {}
        if context is None:
             context = {} 
        Composer = self.pool['mail.compose.message']
        for message in self.browse(cr, uid, ids, context=context):
            lead_id = context.get('active_id') or False
            stages = self.pool.get('crm.case.stage').search(cr,uid,[('name','=','Rejected')])
            name1=message.name
            #create notif
            for rec in self.pool.get('crm.lead').browse(cr,uid,lead_id,context):
                if rec.manager_id :
                    user = 'user'
                    if rec.user_id:
                        user = self.pool.get('res.users').browse(cr, uid, rec.user_id.id, context=context)
                    user_ids=[]
                    alarm_ids=[]
                    event_obj = self.pool.get('calendar.event')
                    models_data = self.pool.get('ir.model.data')
                    alarm_notif_id = models_data.get_object_reference(cr,uid, 'mt_crm_brokerage_manager', 'calendar_alarm_notif_id2')[1]
                    manager = self.pool.get('res.users').browse(cr, uid, rec.manager_id.id, context=context)
                    user_ids.append(manager.partner_id.id)
                    # manager_karim = self.pool.get('res.users').search(cr, uid, [('login', 'ilike', 'karim.fahim@era-egypt.com')], context=context)
                    # manager_karim_id = self.pool.get('res.users').browse(cr, uid, manager_karim, context=context)
                    # user_ids.append(manager_karim_id.partner_id.id)
                    # alarm_ids.append(alarm_notif_id)
                    # date_alert = datetime.datetime.now()+relativedelta(minutes=+ 1)
                    # date_fin = datetime.datetime.now()+relativedelta(minutes=+ 20)
                    # event_values = {
                    #              'name' :"the request assigned to " + user.name +' is rejected',
                    #              'start_datetime':date_alert.strftime('%Y-%m-%d %H:%M:%S') ,
                    #              'stop_datetime':date_fin.strftime('%Y-%m-%d %H:%M:%S') ,
                    #              'alarm_ids':[(6,0, alarm_ids)],
                    #              'partner_ids':[(6,0,user_ids)],
                    #              'is_notif':True,
                    #             }
                    # event_id = event_obj.create(cr, uid,event_values, context=context )
            lead_obj = self.pool['crm.lead']
            if stages:
                    self.pool.get('crm.lead').write(cr, uid, [lead_id], {'stage_id': stages[0],'comment':name1, 'request_code': 'Rej'})
                    template_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'email_template_lead_mail_rejected')[1]
                    composer_id = Composer.create(cr, uid, {
                        'model': 'crm.lead',
                        'res_id': lead_id,
                        'template_id': template_id,
                        'composition_mode': 'comment',
                    }, context=context)
                    template_values = Composer.onchange_template_id(
                        cr, uid, composer_id, template_id, 'comment', 'crm.lead', lead_id
                    )['value']
                    template_values['attachment_ids'] = [(4, id) for id in template_values.get('attachment_ids', [])]
                    Composer.write(cr, uid, [composer_id], template_values, context=context)
                    Composer.send_mail(cr, uid, [composer_id], context=context)
                    lead_obj.write(cr, uid, lead_id, {'reject_id': message.reject_id.id}, context=context)
        return {'type': 'ir.actions.act_window_close',}
    
    def add_lead_message_w_l(self, cr, uid, ids, context=None):
        res = {}
        if context is None:
             context = {} 
        Composer = self.pool['mail.compose.message']
        for message in self.browse(cr, uid, ids, context=context):
            lead_id = context.get('active_id') or False
            if message.type == 'won':
                stages = self.pool.get('crm.case.stage').search(cr,uid,[('name','=','Won')])
                if stages:
                        self.pool.get('crm.lead').write(cr, uid, [lead_id], {'stage_id': stages[0],'comment':message.name})
                        template_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'email_template_lead_mail_won')[1]
                        composer_id = Composer.create(cr, uid, {
                            'model': 'crm.lead',
                            'res_id': lead_id,
                            'template_id': template_id,
                            'composition_mode': 'comment',
                        }, context=context)
                        template_values = Composer.onchange_template_id(
                            cr, uid, composer_id, template_id, 'comment', 'crm.lead', lead_id
                        )['value']
                        template_values['attachment_ids'] = [(4, id) for id in template_values.get('attachment_ids', [])]
                        Composer.write(cr, uid, [composer_id], template_values, context=context)
                        Composer.send_mail(cr, uid, [composer_id], context=context)
            if message.type == 'lost': 
                stages = self.pool.get('crm.case.stage').search(cr,uid,[('name','=','Lost')])
                if stages:
                        self.pool.get('crm.lead').write(cr, uid, [lead_id], {'stage_id': stages[0],'comment':message.name})
                        template_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'mt_crm_brokerage_manager', 'email_template_lead_mail_lost')[1]
                        composer_id = Composer.create(cr, uid, {
                            'model': 'crm.lead',
                            'res_id': lead_id,
                            'template_id': template_id,
                            'composition_mode': 'comment',
                        }, context=context)
                        template_values = Composer.onchange_template_id(
                            cr, uid, composer_id, template_id, 'comment', 'crm.lead', lead_id
                        )['value']
                        template_values['attachment_ids'] = [(4, id) for id in template_values.get('attachment_ids', [])]
                        Composer.write(cr, uid, [composer_id], template_values, context=context)
                        Composer.send_mail(cr, uid, [composer_id], context=context)
        return {'type': 'ir.actions.act_window_close',}
 