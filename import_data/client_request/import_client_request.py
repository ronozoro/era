# -*- coding: utf-8 -*-
 
import base64
from openerp.osv import osv, fields
from openerp import tools,netsvc
from tempfile import TemporaryFile
import csv
from openerp.tools.translate import _


 

class import_client_request(osv.osv):
    _inherit = 'import.import'
    
    def import_client_request(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        this = self.browse(cr, uid, ids[0])   
        quotechar='"'
        delimiter=','
        fileobj = TemporaryFile('w+')
         
        fileobj.write((base64.decodestring(this.data)))   
        fileobj.seek(0)                                    
        reader = csv.DictReader(fileobj, quotechar=str(quotechar), delimiter=str(delimiter))         
 
        client_request_obj = self.pool.get('crm.lead') 
        partner_obj = self.pool.get('res.partner')        
        channel_obj = self.pool.get('crm.tracking.medium')        
        category_obj = self.pool.get('mt.case.categ')    
        property_type_obj = self.pool.get('mt.property.type')                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
 
        for row  in reader :  
           
            partner_ids = partner_obj.search(cr,uid,[('name','=',row['Customer Name'])])
            channel_ids = channel_obj.search(cr,uid,[('name','=',row['Channel'])])
            category_ids = category_obj.search(cr,uid,[('name','=',row['Division'])])
            property_type_ids = property_type_obj.search(cr,uid,[('name','=',row['Properity Type'])])
            values={
                              'type':'opportunity',
                              'name': row['Subject'],
                              'partner_id': partner_ids and partner_ids[0],
                              'phone': row['Mobile'], 
                              'medium_id2': channel_ids and channel_ids[0],
                              'category': category_ids and category_ids[0],
                              'property_ids': property_type_ids and property_type_ids[0],
                              'service_ids':row['Service Type'].lower(), 
                              'budget':row['Budget'],
                               
                             }
            client_request_obj.create(cr,uid,values)
        return True

