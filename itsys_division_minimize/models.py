# -*- coding: utf-8 -*-

from openerp import models, fields, api


class DivisionMinimzeRequests(models.Model):
    _inherit = 'crm.lead'

    def on_change_category(self, cr, uid, ids, category, context=None):
        user = self.pool.get('res.users').browse(cr, uid, context=context)
        aval_list = []
        if user.has_group('base.group_system'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)], context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context).ids

            return {'domain': {'category': [('id', 'in', cats2)], 'property_ids': [('id', 'in', propr)]},
                    'value': {'category': cats2, 'property_ids': propr}}


        if user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_ids': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('mt_crm_brokerage_manager.group_residential_seller_agent_id'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_ids': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_ids': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_ids': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_ids': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_ids': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}


        if user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_ids': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_ids': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}




class DivisionMinimzeProperties(models.Model):
    _inherit = 'product.template'

    def on_change_category(self, cr, uid, ids, category,service_type, context=None):
        user = self.pool.get('res.users').browse(cr, uid, context=context)
        aval_list = []
        if user.has_group('base.group_system'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context).ids

            return {'domain': {'category': [('id', 'in', cats2)], 'property_type_id': [('id', 'in', propr)]},
                    'value': {'category': [cats2], 'property_type_id': [propr]}}

        if user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_type_id': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('mt_crm_brokerage_manager.group_residential_seller_agent_id'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_type_id': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_type_id': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_type_id': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_type_id': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_type_id': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Residential')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_type_id': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

        if user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
            categ_type = self.pool.get('mt.case.categ').search(cr, uid, [('name', '=', 'Commercial')], context=context)
            cats2 = self.pool.get('mt.case.categ').browse(cr, uid, categ_type).ids
            catego = self.pool.get('mt.property.type').search(cr, uid, [('category_pro', 'in', cats2)],
                                                              context=context)
            propr = self.pool.get('mt.property.type').browse(cr, uid, catego, context=context)

            return {'domain': {'category': [('id', 'in', cats2)], 'property_type_id': [('id', 'in', propr.ids)]},
                    'value': {'category': cats2[0]}}

    #
    # @api.model
    # def _get_my_list(self):
    #     user = self.env['res.users'].browse(self.env.uid)
    #     aval_list = []
    #
    #     if user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id'):
    #         cats = self.env['mt.case.categ'].search([('name', '=', 'Residential')])
    #         # self.category = cats.category.id
    #         for c in cats:
    #             aval_list.append(c.id)
    #     elif user.has_group('mt_crm_brokerage_manager.group_residential_seller_agent_id'):
    #         cats = self.env['mt.case.categ'].search([('name', '=', 'Residential')])
    #         # self.category = cats.category.id
    #         for c in cats:
    #             aval_list.append(c.id)
    #
    #     elif user.has_group('mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
    #         cats = self.env['mt.case.categ'].search([('name', '=', 'Commercial')])
    #         # self.category = cats.category.id
    #         for c in cats:
    #             aval_list.append(c.id)
    #
    #     elif user.has_group('mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
    #         cats = self.env['mt.case.categ'].search([('name', '=', 'Commercial')])
    #         # self.category = cats.category.id
    #         for c in cats:
    #             aval_list.append(c.id)
    #
    #
    #     elif user.has_group('mt_crm_brokerage_manager.group_residential_agent_buyer_seller_id'):
    #         cats = self.env['mt.case.categ'].search([('name', '=', 'Residential')])
    #         # self.category = cats.category.id
    #         for c in cats:
    #             aval_list.append(c.id)
    #
    #     elif user.has_group('mt_crm_brokerage_manager.group_commercial_agent_buyer_seller_id'):
    #         cats = self.env['mt.case.categ'].search([('name', '=', 'Commercial')])
    #         # self.category = cats.category.id
    #         for c in cats:
    #             aval_list.append(c.id)
    #
    #
    #     domain = [('id', 'in', aval_list)]
    #     print 'aaaaaaaaaaaaaaaaaaa', domain
    #
    #     return domain
    #
    # category = fields.Many2one(comodel_name="mt.case.categ", string="Category List", required=False,
    #                            domain=_get_my_list)
