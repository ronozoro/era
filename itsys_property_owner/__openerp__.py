# -*- coding: utf-8 -*-
{
    'name': "itsys_property_owner",

    'summary': """
        property owner showed to the creator and his team leader""",

    'description': """
        property owner showed to the creator and his team leader
    """,

    'author': "Itsys- Ahmed Gaber ",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base',
                'crm',
                'sale_crm',
                'mt_crm_proprety',
                'mt_crm_brokerage_manager',
                'website_property',
                'client_req_enhancement',
                'website_sale',
                'crm_partner_assign',
                'survey',
                'search_criteria',
                'add_messaging_to_developers_and_projects_views',
                'itsys_era_coordinator'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/product_template_view.xml',
        'views/crm_lead_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}