# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.osv import osv, orm


class CrmLeadOwner(models.Model):
    _inherit = 'crm.lead'

    def _get_create_uid(self):
        user = self.env['res.users'].browse(self.env.uid)
        lst = []
        if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            teams = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)
        elif user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
            teams = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)


        elif user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
            teams = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)

        elif user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
            teams = self.env['crm.case.section'].search([('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)

        for rec in self:
            if (rec.create_uid == self.env.user) or (rec.create_uid.id in lst) or user.has_group('base.group_system'):
                rec.is_create_uid = True
                rec.is_team_leader = True
            else:
                rec.is_create_uid = False
                rec.is_team_leader = False

    is_create_uid = fields.Boolean(compute="_get_create_uid", string="Is Create UID", default=True)
    is_team_leader = fields.Boolean(compute="_get_create_uid", string="Is Team Leader", default=True)




class CrmLead2OpportunityPartner(osv.osv_memory):
    _inherit = 'crm.lead2opportunity.partner'

    # @api.onchange('section_id')
    def _get_my_list_teams_convert(self):

        user = self.env['res.users'].browse(self.env.uid)
        if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            team_id = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)], limit=1)
            self.section_id = team_id.id
            teams_list = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)]).ids
            return {'domain': {'section_id': [('id', 'in', teams_list)]}}

        if user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group(
                'itsys_era_coordinator.group_division_head_commercial'):
            team_id = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')], limit=1)
            self.section_id = team_id.id
            teams_list = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')]).ids
            return {'domain': {'section_id': [('id', 'in', teams_list)]}}

        if user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group(
                'itsys_era_coordinator.group_division_head_resedential'):
            team_id = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')], limit=1)
            self.section_id = team_id.id
            teams_list = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')]).ids
            return {'domain': {'section_id': [('id', 'in', teams_list)]}}

        if user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
            team_id = self.env['crm.case.section'].search(
                [('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)], limit=1)
            self.section_id = team_id.id
            teams_list = self.env['crm.case.section'].search(
                [('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)]).ids
            return {'domain': {'section_id': [('id', 'in', teams_list)]}}

        if user.has_group('base.group_system'):
            team_id = self.env['crm.case.section'].search([], limit=1)
            self.section_id = team_id.id
            teams_list = self.env['crm.case.section'].search([]).ids
            return {'domain': {'section_id': [('id', 'in', teams_list)]}}

    @api.multi
    @api.onchange('section_id')
    def convert_to_client_users(self):
        lst1 = []
        res = {}
        user = self.env.user
        # for rec in self:
        if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
            self.user_id = user.id
            self.section_id = user.sale_team_id.id
            lst1.append(user.id)
            res['domain'] = {'user_id': [('id', 'in', lst1)]}
            # res['value'] = {'user_id': user.id}
        if self.section_id:
            lst1 = self.section_id.member_ids.ids
            res['value'] = {'user_id': self.env.user}
            res['domain'] = {'user_id': [('id', 'in', lst1)]}

            # return {'domain': {'user_id': [('id', 'in', lst1)]}}
        else:
            res['domain'] = {'user_id': [('id', 'in', lst1)]}
            # return {'domain': {'user_id': [('id', 'in', lst1)]}}
        return res

        # res = {}
        # lst1 = []
        # pars = []
        # user = self.env['res.users'].browse(self.env.uid)
        # lst1.append(user.id)
        # if user.has_group('base.group_system'):
        #     sales_team = self.env['crm.case.section'].search([])
        #     leads = self.env['crm.lead'].search([('id', '=', self.crm_lead_id.id)])
        #
        #     for rec in sales_team:
        #         for user in rec.member_ids:
        #             # if leads.service_ids in ['buyer_rent', 'share_buyer', 'buy']:
        #             #     if user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id') or user.has_group('mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
        #             #         lst1.append(user.id)
        #             # elif leads.service_ids in ['seller_rent', 'share_seller', 'sale']:
        #             #     if user.has_group('mt_crm_brokerage_manager.group_residential_seller_agent_id') or user.has_group('mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
        #             lst1.append(user.id)
        #
        #     pars = self.env['res.partner'].search([('user_id', 'in', lst1)]).ids
        #
        #     res['domain'] = {'user_id': [('id', 'in', lst1)],
        #                      'partner_id': [('is_company', '=', False), ('customer', '=', True),
        #                                     ('id', 'in', pars)]}
        #
        # if user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
        #     sales_team = self.env['crm.case.section'].search(
        #         [('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
        #     for rec in sales_team:
        #         for user in rec.member_ids:
        #             lst1.append(user.id)
        #
        #     pars = self.env['res.partner'].search([('user_id', 'in', lst1)]).ids
        #
        #     res['domain'] = {'user_id': [('id', 'in', lst1)],
        #                      'partner_id': [('is_company', '=', False), ('customer', '=', True),
        #                                     ('id', 'in', pars)]}
        #
        # if user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group(
        #         'itsys_era_coordinator.group_division_head_commercial'):
        #     sales_team = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
        #     leads = self.env['crm.lead'].search([('id', '=', self.crm_lead_id.id)])
        #     for rec in sales_team:
        #         for user in rec.member_ids:
        #             if leads.service_ids in ['buyer_rent', 'share_buyer', 'buy']:
        #                 if user.has_group(
        #                         'mt_crm_brokerage_manager.group_residential_buyer_agent_id') or user.has_group(
        #                     'mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
        #                     lst1.append(user.id)
        #
        #                 if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
        #                     lst1.append(user.id)
        #
        #             elif leads.service_ids in ['seller_rent', 'share_seller', 'sale']:
        #                 if user.has_group(
        #                         'mt_crm_brokerage_manager.group_residential_seller_agent_id') or user.has_group(
        #                     'mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
        #                     lst1.append(user.id)
        #                 if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
        #                     lst1.append(user.id)
        #             # lst1.append(user.id)
        #
        #     pars = self.env['res.partner'].search([('user_id', 'in', lst1)]).ids
        #
        #     res['domain'] = {'user_id': [('id', 'in', lst1)],
        #                      'partner_id': [('is_company', '=', False), ('customer', '=', True),
        #                                     ('id', 'in', pars)]}
        #
        # if user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group(
        #         'itsys_era_coordinator.group_division_head_resedential'):
        #     sales_team = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')])
        #     leads = self.env['crm.lead'].search([('id', '=', self.crm_lead_id.id)])
        #     for rec in sales_team:
        #         for user in rec.member_ids:
        #             if leads.service_ids in ['buyer_rent', 'share_buyer', 'buy']:
        #                 if user.has_group(
        #                         'mt_crm_brokerage_manager.group_residential_buyer_agent_id') or user.has_group(
        #                     'mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
        #                     lst1.append(user.id)
        #
        #                 if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
        #                     lst1.append(user.id)
        #
        #             elif leads.service_ids in ['seller_rent', 'share_seller', 'sale']:
        #                 if user.has_group(
        #                         'mt_crm_brokerage_manager.group_residential_seller_agent_id') or user.has_group(
        #                     'mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
        #                     lst1.append(user.id)
        #                 if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
        #                     lst1.append(user.id)
        #             # lst1.append(user.id)
        #
        #     pars = self.env['res.partner'].search([('user_id', 'in', lst1)]).ids
        #
        #     res['domain'] = {'user_id': [('id', 'in', lst1)],
        #                      'partner_id': [('is_company', '=', False), ('customer', '=', True),
        #                                     ('id', 'in', pars)]}
        #
        # if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
        #     sales_team = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
        #     leads = self.env['crm.lead'].search([('id', '=', self.crm_lead_id.id)])
        #     for rec in sales_team:
        #         for user in rec.member_ids:
        #             if leads.service_ids in ['buyer_rent', 'share_buyer', 'buy']:
        #                 if user.has_group(
        #                         'mt_crm_brokerage_manager.group_residential_buyer_agent_id') or user.has_group(
        #                     'mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
        #                     lst1.append(user.id)
        #
        #                 # if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
        #                 #     lst1.append(user.id)
        #
        #             elif leads.service_ids in ['seller_rent', 'share_seller', 'sale']:
        #                 if user.has_group(
        #                         'mt_crm_brokerage_manager.group_residential_seller_agent_id') or user.has_group(
        #                     'mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
        #                     lst1.append(user.id)
        #                 # if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
        #                 #     lst1.append(user.id)
        #             # lst1.append(user.id)
        #
        #     pars = self.env['res.partner'].search([('user_id', 'in', lst1)]).ids
        #
        #     res['domain'] = {'user_id': [('id', 'in', lst1)],
        #                      'partner_id': [('is_company', '=', False), ('customer', '=', True),
        #                                     ('id', 'in', pars)]}
        #
        # if user.has_group(
        #         'mt_crm_brokerage_manager.group_residential_buyer_agent_id') or user.has_group(
        #     'mt_crm_brokerage_manager.group_commercial_buyer_agent_id') or user.has_group(
        #     'mt_crm_brokerage_manager.group_residential_seller_agent_id') or user.has_group(
        #     'mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
        #     sales_team = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)])
        #     leads = self.env['crm.lead'].search([('id', '=', self.crm_lead_id.id)])
        #
        #     lst1.append(self.env.user.id)
        #
        #     pars = self.env['res.partner'].search([('user_id', '=', self.env.user.id)]).ids
        #
        #     res['domain'] = {'user_id': [('id', 'in', lst1)],
        #                      'partner_id': [('is_company', '=', False), ('customer', '=', True),
        #                                     ('id', 'in', pars)]}
        #
        # return res

    def _get_create_uid(self):
        teams = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])

        lst = []
        for each in teams:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        user = self.env['res.users'].browse(self.env.uid)

        for rec in self:
            if (rec.create_uid == self.env.user) or \
                    (rec.create_uid.id in lst) or \
                    user.has_group('base.group_system') or \
                    user.has_group('itsys_era_coordinator.group_coordinator_commercial') or \
                    user.has_group('itsys_era_coordinator.group_division_head_commercial') or \
                    user.has_group('itsys_era_coordinator.group_coordinator_resedential') or \
                    user.has_group('itsys_era_coordinator.group_division_head_resedential') or \
                    user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
                rec.is_create_uid = True
                rec.is_team_leader = True
            else:
                rec.is_create_uid = False
                rec.is_team_leader = False

    @api.one
    @api.depends('partner_id')
    def _get_create_uid_readonly(self):

        user = self.env['res.users'].browse(self.env.uid)

        for rec in self:
            if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
                rec.is_assigned = True
            else:
                rec.is_assigned = False

    is_assigned = fields.Boolean(compute="_get_create_uid_readonly", string="is_assigned")
    is_create_uid = fields.Boolean(compute="_get_create_uid", string="Is Create UID", default=True)
    is_team_leader = fields.Boolean(compute="_get_create_uid", string="Is Team Leader", default=True)



class crm_lead2opportunity_mass_convert(osv.osv_memory):
    _inherit = 'crm.lead2opportunity.partner.mass'




    crm_lead_id = fields.Many2one('crm.lead', string="leads"),


    # @api.onchange('section_id')
    def _get_my_list_teams_convert(self):

        user = self.env['res.users'].browse(self.env.uid)
        if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            team_id = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)], limit=1)
            self.section_id = team_id.id
            teams_list = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)]).ids
            return {'domain': {'section_id': [('id', 'in', teams_list)]}}

        if user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group(
                'itsys_era_coordinator.group_division_head_commercial'):
            team_id = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')], limit=1)
            self.section_id = team_id.id
            teams_list = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')]).ids
            return {'domain': {'section_id': [('id', 'in', teams_list)]}}

        if user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group(
                'itsys_era_coordinator.group_division_head_resedential'):
            team_id = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')], limit=1)
            self.section_id = team_id.id
            teams_list = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')]).ids
            return {'domain': {'section_id': [('id', 'in', teams_list)]}}

        if user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
            team_id = self.env['crm.case.section'].search(
                [('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)], limit=1)
            self.section_id = team_id.id
            teams_list = self.env['crm.case.section'].search(
                [('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)]).ids
            return {'domain': {'section_id': [('id', 'in', teams_list)]}}

        if user.has_group('base.group_system'):
            team_id = self.env['crm.case.section'].search([], limit=1)
            self.section_id = team_id.id
            teams_list = self.env['crm.case.section'].search([]).ids
            return {'domain': {'section_id': [('id', 'in', teams_list)]}}

    @api.multi
    @api.onchange('section_id')
    def convert_to_client_users(self):
        lst1 = []
        res = {}
        user = self.env.user
        # for rec in self:
        if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
            self.user_ids = user.id
            self.section_id = user.sale_team_id.id
            lst1.append(user.id)
            res['domain'] = {'user_ids': [('id', 'in', lst1)]}
            # res['value'] = {'user_id': user.id}
        if self.section_id:
            lst1 = self.section_id.member_ids.ids
            res['value'] = {'user_ids': self.env.user}
            res['domain'] = {'user_ids': [('id', 'in', lst1)]}

            # return {'domain': {'user_id': [('id', 'in', lst1)]}}
        else:
            res['domain'] = {'user_ids': [('id', 'in', lst1)]}
            # return {'domain': {'user_id': [('id', 'in', lst1)]}}
        return res

    def _get_create_uid(self):
        teams = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])

        lst = []
        for each in teams:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        user = self.env['res.users'].browse(self.env.uid)

        for rec in self:
            if (rec.create_uid == self.env.user) or \
                    (rec.create_uid.id in lst) or \
                    user.has_group('base.group_system') or \
                    user.has_group('itsys_era_coordinator.group_coordinator_commercial') or \
                    user.has_group('itsys_era_coordinator.group_division_head_commercial') or \
                    user.has_group('itsys_era_coordinator.group_coordinator_resedential') or \
                    user.has_group('itsys_era_coordinator.group_division_head_resedential') or \
                    user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
                rec.is_create_uid = True
                rec.is_team_leader = True
            else:
                rec.is_create_uid = False
                rec.is_team_leader = False

    @api.one
    @api.depends('partner_id')
    def _get_create_uid_readonly(self):

        user = self.env['res.users'].browse(self.env.uid)

        for rec in self:
            if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
                rec.is_assigned = True
            else:
                rec.is_assigned = False

    is_assigned = fields.Boolean(compute="_get_create_uid_readonly", string="is_assigned")
    is_create_uid = fields.Boolean(compute="_get_create_uid", string="Is Create UID", default=True)
    is_team_leader = fields.Boolean(compute="_get_create_uid", string="Is Team Leader", default=True)


class CrmMakeSale(osv.osv_memory):
    """ Make sale  order for crm """
    _inherit = "crm.make.sale"

    crm_lead_id = fields.Many2one('crm.lead', "Leads")
    section_id = fields.Many2one('crm.case.section', "Sections")

    @api.multi
    @api.onchange('partner_id')
    def get_clients_per_user(self):
        users = []
        pars = []
        user = self.env['res.users'].browse(self.env.uid)
        leads = self.env['crm.lead'].search([('id', '=', self.crm_lead_id.id)])
        records = self.env['crm.case.section'].sudo().search([('member_ids', '=', self.env.user.id)])

        for each in records:
            for each_user in each.member_ids:
                if user != each_user.user_id.id and user.has_group(
                        'mt_crm_brokerage_manager.group_residential_buyer_agent_id') or user.has_group(
                    'mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
                    users.append(user.id)
                    pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', users)])

                elif user != each_user.user_id.id and user.has_group(
                        'mt_crm_brokerage_manager.group_residential_seller_agent_id') or user.has_group(
                    'mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
                    users.append(user.id)
                    pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', users)])
                elif user.has_group('mt_crm_brokerage_manager.group_see_his_team_id') or user.has_group(
                        'base.group_system'):

                    users_ids = each.member_ids.ids
                    pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', users_ids)])

                elif user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group(
                        'itsys_era_coordinator.group_division_head_commercial'):

                    users_ids = each.member_ids.ids
                    pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', users_ids)])

                elif user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group(
                        'itsys_era_coordinator.group_division_head_resedential'):

                    users_ids = each.member_ids.ids
                    pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', users_ids)])

                elif user.has_group('base.group_system'):
                    pars = self.env['res.partner'].search([('customer', '=', True)])

                elif user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
                    lst1 = []
                    sales_team = self.env['crm.case.section'].search(
                        [('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
                    for rec in sales_team:
                        for user in rec.member_ids:
                            lst1.append(user.id)

                    pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', lst1)])

        if pars:
            return {'domain': {'partner_id': [('id', 'in', pars.ids)]}}
        else:
            return {'domain': {'partner_id': [('id', 'in', pars)]}}

        # return res


class invite_wizard(osv.osv_memory):
    """ Wizard to invite partners and make them followers. """
    _inherit = 'mail.wizard.invite'

    @api.multi
    @api.onchange('partner_ids')
    def get_clients_per_user(self):
        lis = []
        users_login = self.env['res.users'].search([])
        for r in users_login:
            lis.append(r.partner_id.id)

        return {'domain': {'partner_ids': [('id', 'in', lis)]}}


class mail_compose_message(orm.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    @api.onchange('partner_ids')
    def get_clients_per_user(self):
        lis = []
        users_login = self.env['res.users'].search([])
        for r in users_login:
            lis.append(r.partner_id.id)
        return {'domain': {'partner_ids': [('id', 'in', lis)]}}


class crm_lead_forward_to_partner(osv.TransientModel):
    """ Forward info history to partners. """
    _inherit = 'crm.lead.forward.to.partner'

    crm_lead_id = fields.Many2one('crm.lead', "Leads")
    section_id = fields.Many2one('crm.case.section', "Sections")

    @api.multi
    @api.onchange('partner_id')
    def get_clients_per_user(self):
        users = []
        pars = []
        user = self.env['res.users'].browse(self.env.uid)
        leads = self.env['crm.lead'].search([('id', '=', self.crm_lead_id.id)])
        records = self.env['crm.case.section'].sudo().search([('member_ids', '=', self.env.user.id)])

        for each in records:
            for each_user in each.member_ids:
                if user != each_user.user_id.id and user.has_group(
                        'mt_crm_brokerage_manager.group_residential_buyer_agent_id') or user.has_group(
                    'mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
                    users.append(user.id)
                    pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', users)])

                elif user != each_user.user_id.id and user.has_group(
                        'mt_crm_brokerage_manager.group_residential_seller_agent_id') or user.has_group(
                    'mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
                    users.append(user.id)
                    pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', users)])


                elif user.has_group('mt_crm_brokerage_manager.group_see_his_team_id') or user.has_group(
                        'base.group_system'):

                    users_ids = each.member_ids.ids
                    pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', users_ids)])

                elif user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group(
                        'itsys_era_coordinator.group_division_head_commercial'):

                    users_ids = each.member_ids.ids
                    pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', users_ids)])



                elif user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group(
                        'itsys_era_coordinator.group_division_head_resedential'):

                    users_ids = each.member_ids.ids
                    pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', users_ids)])


        if user.has_group('base.group_system'):
            pars = self.env['res.partner'].search([('customer', '=', True)])

        if user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
            lst1 = []
            sales_team = self.env['crm.case.section'].search(
                [('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
            for rec in sales_team:
                for user in rec.member_ids:
                    lst1.append(user.id)

            pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', lst1)])

        if pars:

            return {'domain': {'partner_id': [('id', 'in', pars.ids)]}}
        else:
            return {'domain': {'partner_id': [('id', 'in', pars)]}}


class survey_mail_compose_message(osv.TransientModel):
    _inherit = 'survey.mail.compose.message'

    crm_lead_id = fields.Many2one('crm.lead', "Leads")
    section_id = fields.Many2one('crm.case.section', "Sections")

    @api.model
    def _get_domain_user(self):
        lst = []
        pars = []
        user = self.env.user
        if user.has_group('base.group_system'):
            return [('customer', '=', True)]
        if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            lst.append(user.id)
            return [('customer', '=', True), ('user_id', 'in', lst)]

        if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            teams = self.env['crm.case.section'].search([('user_id', '=', user.id)])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)
            return [('customer', '=', True), ('user_id', 'in', lst)]

        if user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
            teams = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)
            return [('customer', '=', True), ('user_id', 'in', lst)]

        if user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
            teams = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)
            return [('customer', '=', True), ('user_id', 'in', lst)]

        if user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
            teams = self.env['crm.case.section'].search(
                [('operating_unit_id', '=', user.sale_team_id.operating_unit_id.id)])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)
            return [('customer', '=', True), ('user_id', 'in', lst)]

        return [('id', 'in', self.env['res.partner'].sudo().search([])._ids)]

    partner_ids = fields.Many2many('res.partner', 'survey_mail_compose_message_res_partner_rel',
                                   'wizard_id', 'partner_id', string='Existing contacts',
                                   domain=lambda self: self._get_domain_user())




class crm_lead_assignation(osv.TransientModel):
    _inherit = 'crm.lead.assignation'

    @api.model
    def _get_domain_user(self):
        lst = []
        pars = []
        user = self.env.user
        if user.has_group('base.group_system'):
            return [('customer', '=', True)]
        if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            lst.append(user.id)
            return [('customer', '=', True), ('user_id', 'in', lst)]

        if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            teams = self.env['crm.case.section'].search([('user_id', '=', user.id)])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)
            return [('customer', '=', True), ('user_id', 'in', lst)]

        if user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
            teams = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)
            return [('customer', '=', True), ('user_id', 'in', lst)]

        if user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
            teams = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)
            return [('customer', '=', True), ('user_id', 'in', lst)]

        if user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
            teams = self.env['crm.case.section'].search(
                [('operating_unit_id', '=', user.sale_team_id.operating_unit_id.id)])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)
            return [('customer', '=', True), ('user_id', 'in', lst)]

        return [('id', 'in', self.env['res.partner'].sudo().search([])._ids)]

    partner_assigned_id = fields.Many2one('res.partner', string='Assigned Partner', domain=lambda self: self._get_domain_user())


