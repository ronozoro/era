# -*- coding: utf-8 -*-

from openerp import models, fields, api


class PropertyOwner(models.Model):
    _inherit = 'product.template'
    @api.multi
    def on_change_service_type(self, category, service_type):
        res = super(PropertyOwner,self).on_change_service_type(category, service_type)
        curre_user = self.env['res.users'].browse(self.env.uid)
        res = {}
        users = []
        users2 = []
        pars = []
        lst = []
        if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
            lst.append(curre_user.id)

        if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
            lst.append(curre_user.id)

        if curre_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            lst.append(curre_user.id)



        if service_type:

            if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial') or curre_user.has_group(
                    'itsys_era_coordinator.group_division_head_commercial'):
                teams = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
                for each in teams:
                    for each_user in each.member_ids:
                        print service_type, "SSSSSSSSSSSSSSSSSSSSSSSs"
                        if service_type in ('buyer_rent', 'share_buyer', 'buy'):
                            print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$44"
                            if each_user.has_group('mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
                                lst.append(each_user.id)
                            elif each_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                                lst.append(each_user.id)
                            pars = self.env['res.partner'].search([('customer', '=', True), ('agent_id', 'in', lst)])
                        elif service_type in ['seller_rent', 'share_seller', 'sale']:
                            print "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"
                            if each_user.has_group('mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
                                lst.append(each_user.id)
                            elif each_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                                lst.append(each_user.id)

                            pars = self.env['res.partner'].search([('customer', '=', True), ('agent_id', 'in', lst)])

            if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential') or curre_user.has_group(
                    'itsys_era_coordinator.group_division_head_resedential'):
                teams = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')])
                for each in teams:
                    for each_user in each.member_ids:
                        print service_type, "SSSSSSSSSSSSSSSSSSSSSSSs"
                        if service_type in ('buyer_rent', 'share_buyer', 'buy'):
                            print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
                            if each_user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id'):
                                lst.append(each_user.id)
                            elif each_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                                lst.append(each_user.id)
                        elif service_type in ['seller_rent', 'share_seller', 'sale']:
                            print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                            if each_user.has_group('mt_crm_brokerage_manager.group_residential_seller_agent_id'):
                                lst.append(each_user.id)
                            elif each_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                                lst.append(each_user.id)


            if curre_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                teams = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
                for each in teams:
                    for each_user in each.member_ids:
                        print service_type, "SSSSSSSSSSSSSSSSSSSSSSSs"
                        if service_type in ('buyer_rent', 'share_buyer', 'buy'):
                            print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
                            if each_user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id') or \
                                    each_user.has_group('mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
                                lst.append(each_user.id)

                        elif service_type in ['seller_rent', 'share_seller', 'sale']:
                            print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                            if each_user.has_group('mt_crm_brokerage_manager.group_residential_seller_agent_id') or  each_user.has_group('mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
                                lst.append(each_user.id)



            if curre_user.has_group('base.group_system'):
                teams = self.env['crm.case.section'].search([ ])
                for each in teams:
                    for each_user in each.member_ids:
                       lst.append(each_user.id)



            if curre_user.has_group('mt_crm_brokerage_manager.group_agent_id'):
                res['domain'] = {'agent_id': [('id', 'in', lst)]}
            if curre_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                res['domain'] = {'agent_id': [('id', 'in', lst)]}

            if curre_user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
                res['domain'] = {'agent_id': [('id', 'in', lst)]}

            if curre_user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
                res['domain'] = {'agent_id': [('id', 'in', lst)]}

        return res

    # @api.multi
    # @api.onchange('created_by')
    # def on_change_agent_id(self):
    #     user = self.env['res.users'].browse(self.env.uid)
    #     lst =[]
    #     for rec in self:
    #         if user.has_group('base.group_system'):
    #             lst2 = []
    #             teams = self.env['crm.case.section'].search([])
    #             for each in teams:
    #                 for each_user in each.member_ids:
    #                     lst2.append(each_user.id)
    #             return {'domain': {'agent_id': [('id', 'in', lst2)]}}
    #
    #         else:
    #             lst = []
    #             if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
    #                 teams = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
    #                 for each in teams:
    #                     for each_user in each.member_ids:
    #                         lst.append(each_user.id)
    #             elif user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group('itsys_era_coordinator.group_division_head_commercial'):
    #                 teams = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
    #                 for each in teams:
    #                     for each_user in each.member_ids:
    #                         # print rec.service_type,"SSSSSSSSSSSSSSSSSSSSSSSs"
    #                         # if rec.service_type in ('buyer_rent', 'share_buyer', 'buy'):
    #                         #     print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$44"
    #                         #     if each_user.has_group('mt_crm_brokerage_manager.group_commercial_buyer_agent_id') and user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
    #                         #         lst.append(each_user.id)
    #                         #     elif each_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
    #                         #         lst.append(each_user.id)
    #                         # elif rec.service_type in ['seller_rent', 'share_seller', 'sale']:
    #                         #     print "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"
    #                         #     if each_user.has_group('mt_crm_brokerage_manager.group_commercial_seller_agent_id') and user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
    #                         #         lst.append(each_user.id)
    #                         #     elif each_user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
    #                         lst.append(each_user.id)
    #
    #
    #             elif user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group('itsys_era_coordinator.group_division_head_resedential'):
    #                 teams = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')])
    #                 for each in teams:
    #                     for each_user in each.member_ids:
    #                         lst.append(each_user.id)
    #
    #             return {'domain': {'agent_id': [('id', 'in', lst)]}}
    #



    def _get_create_uid(self):
        user = self.env['res.users'].browse(self.env.uid)
        lst = []
        if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            teams = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)
        elif user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
            teams = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)

        elif user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
            teams = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')])
            for each in teams:
                for each_user in each.member_ids:
                    lst.append(each_user.id)

        for rec in self:
            if (rec.agent_id == self.env.user) or (rec.create_uid.id in lst) or user.has_group('base.group_system'):
                rec.is_create_uid = True
                rec.is_team_leader = True
            else:
                rec.is_create_uid = False
                rec.is_team_leader = False





    @api.one
    @api.depends('partner_id')
    def _get_create_uid_property_readonly(self):
        user = self.env['res.users'].browse(self.env.uid)

        for rec in self:
            if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
                rec.is_assigned = True
            else:
                rec.is_assigned = False

    is_assigned = fields.Boolean(compute="_get_create_uid_property_readonly", string="is_assigned")
    is_create_uid = fields.Boolean(compute="_get_create_uid", string="Is Create UID", default=True)
    is_team_leader = fields.Boolean(compute="_get_create_uid", string="Is Team Leader", default=True)


