# -*- coding: utf-8 -*-

from openerp import models, fields, api


class PropertyOwner(models.Model):
    _inherit = 'product.template'

    def _get_create_uid(self):
        teams = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
        print "%%%%%%%%%%%%%%%%%%%%%%%%555", teams
        lst = []
        for each in teams:
            for each_user in each.member_ids:
                lst.append(each_user.id)
            # lst.append(each.user_id.id)

                # if (self.create_uid in lst):
                #     self.is_team_leader = True
                # else:
                #     self.is_team_leader = False

        user = self.env['res.users'].browse(self.env.uid)
        # print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",user.id
        # if user.has_group('base.group_system'):
        #     print "*******************************"
        #     lst.append(user.id)

        print (lst)
        print "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"

        for rec in self:
            if (rec.create_uid == self.env.user) or (rec.create_uid.id in lst) or user.has_group('base.group_system'):
                rec.is_create_uid = True
                rec.is_team_leader = True
            else:
                rec.is_create_uid = False
                rec.is_team_leader = False

    is_create_uid = fields.Boolean(compute="_get_create_uid", string="Is Create UID", default=True)
    is_team_leader = fields.Boolean(compute="_get_create_uid", string="Is Team Leader", default=True)
