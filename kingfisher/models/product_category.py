# -*- coding: utf-8 -*-

from openerp.osv import osv, orm, fields
from openerp.modules.module import get_module_resource
from openerp import tools, api, addons, SUPERUSER_ID

class product_category(osv.Model):
    _inherit='product.public.category'
    
#     def _get_default_image(self, cr, uid, context=None):
#         #rent
#         if parent_id.name =='RENT ' :
#             if name == 'Residential' :
#                 image_path = get_module_resource('kingfisher', 'static/img', 'Featured.jpg')
#             if name == 'Commercial' :
#                 image_path = get_module_resource('kingfisher', 'static/img', 'Residential.jpg')
#         return tools.image_resize_image_big(open(image_path, 'rb').read().encode('base64'))
    
    
    _columns={
              'include_in_menu':fields.boolean("Include in Navigation Menu",help="Include in Navigation Menu"),
               #'image': fields.binary("Photo",help="Equipment Photo, limited to 1024x1024px."),
              'image_header': fields.binary("Website Header Image"),
                
              }
    
    _defaults = {
#                 'image': _get_default_image,
                
                }

product_category()

class website(orm.Model):
    _inherit = 'website'
    
    def get_product_category(self, cr, uid,ids, context=None):  
        category_ids=self.pool.get('product.public.category').search(cr,uid,[('parent_id', '=', False),('include_in_menu','!=',False)])
        if category_ids and len(category_ids)>5:
            category_ids=category_ids[:5]
        elif category_ids and len(category_ids)<=5:
            category_ids=category_ids
        category_data = self.pool.get('product.public.category').browse(cr,uid,category_ids,context)
        return category_data
        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:        