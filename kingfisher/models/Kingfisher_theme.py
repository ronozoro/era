# -*- coding: utf-8 -*-

from openerp.osv import osv, orm, fields
from datetime import date

class product_template(osv.Model):
    _inherit = "product.template"    
    _columns = {
                'is_arrival':fields.boolean('Arrival properties'),
                'is_features': fields.boolean('Feature properties'),
    }    
    _defaults = {
                 'is_arrival':False,
                 'is_features':False,
    }
product_template()

class res_partner(osv.osv):
    _description = 'Supplier Brand'
    _inherit = "res.partner"
    _columns={                               
              'is_home_brand':fields.boolean('Brand'),             
    }
    _defaults = {
                 'is_home_brand':False,     
    }    
res_partner()

class website(orm.Model):
    _inherit = 'website'
      
    def get_arrival_product(self, cr, uid,ids, context=None):        
        prod_ids=self.pool.get('product.template').search(cr, uid, [('is_arrival','=','True'),("sale_ok", "=", True)], context=context)
        if prod_ids and len(prod_ids)>16:
            prod_ids=prod_ids[:16]
        elif prod_ids and len(prod_ids)<=16:
            prod_ids=prod_ids
        price_list=self.price_list_get(cr,uid,context);
        product_data = self.pool.get('product.template').browse(cr,uid,prod_ids,{'pricelist':int(price_list)})
        return product_data    
    
    def price_list_get(self,cr,uid,context=None):
            partner = self.pool.get('res.users').browse(cr, uid, uid, context=context)
            if partner:
                price_list=partner.partner_id.property_product_pricelist
                return price_list.id
            return True
    
    def get_feature_product(self, cr, uid,ids, context=None):            
        prod_ids=self.pool.get('product.template').search(cr, uid,[('is_features','=','True'),("sale_ok", "=", True)], context=context)
        if prod_ids and len(prod_ids)>8:
            prod_ids=prod_ids[:8]
        elif prod_ids and len(prod_ids)<=8:
            prod_ids=prod_ids
        price_list=self.price_list_get(cr,uid,context);
        product_data = self.pool.get('product.template').browse(cr,uid,prod_ids,{'pricelist':int(price_list)})
        return product_data
        
    def get_brand_img(self, cr, uid,ids, context=None):    
        brand_ids=self.pool.get('res.partner').search(cr, uid, [('is_home_brand','=','True')], context=context)
        brand_data = self.pool.get('res.partner').browse(cr,uid,brand_ids,context)
        return brand_data
    
    def get_current_year(self):
        return date.today().year
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: