# -*- coding: utf-8 -*-

from openerp import models, fields, api


class itsys_currency_updater(models.Model):
    _inherit = 'crm.lead'

    @api.multi
    @api.onchange('asking_price', 'currency_id3', 'budget2', 'currency_id')
    def calc_currency(self):

        rates = self.env['currency.rate.update.service'].search([])
        dollar_rate = 0.0
        euro_rate = 0.0
        for rate in rates.currency_to_update:
            if rate.name == 'USD':
                dollar_rate = rate.rate_silent

            if rate.name == 'EUR':
                euro_rate = rate.rate_silent

        for rec in self:
            if rec.request_type == 'buyer':
                if rec.currency_id.name == 'EGP':
                    rec.egypt_currency = float(rec.budget2)
                    rec.euro_currency = euro_rate * float(rec.budget2)
                    rec.dollar_currency = dollar_rate * float(rec.budget2)

                elif rec.currency_id.name == 'USD':
                    rec.dollar_currency = float(rec.budget2)
                    rec.egypt_currency = (1 / dollar_rate) * float(rec.budget2)
                    rec.euro_currency = euro_rate * rec.egypt_currency


                elif rec.currency_id.name == 'EUR':
                    rec.euro_currency = float(rec.budget2)
                    rec.egypt_currency = (1 / euro_rate) * float(rec.budget2)
                    rec.dollar_currency = dollar_rate * rec.egypt_currency

            if rec.request_type == 'seller':
                if rec.currency_id3.name == 'EGP':
                    rec.egypt_currency = rec.asking_price
                    rec.euro_currency = euro_rate * rec.asking_price
                    rec.dollar_currency = dollar_rate * rec.asking_price

                elif rec.currency_id3.name == 'USD':
                    rec.dollar_currency = rec.asking_price
                    rec.egypt_currency = (1 / dollar_rate) * rec.asking_price
                    rec.euro_currency = euro_rate * rec.egypt_currency


                elif rec.currency_id3.name == 'EUR':
                    rec.euro_currency = rec.asking_price
                    rec.egypt_currency = (1 / euro_rate) * rec.asking_price
                    rec.dollar_currency = dollar_rate * rec.egypt_currency

    dollar_currency = fields.Float("Dollar ($)" ,compute='calc_currency')
    egypt_currency = fields.Float("Egyption (EGP)",compute='calc_currency')
    euro_currency = fields.Float("Euro (EUR €)",compute='calc_currency')
    currency_lead_id = fields.Many2one('res.currency', string="currency")
    service_id = fields.Many2one('currency.rate.update.service',
                                 string='Currency update services')


class ItsysCurrencyUpdaterProperty(models.Model):
    _inherit = 'product.template'

    @api.multi
    @api.depends('asking_price', 'currency_id')
    def calc_currency(self):
        rates = self.env['currency.rate.update.service'].search([])
        dollar_rate = 0.0
        euro_rate = 0.0
        for rate in rates.currency_to_update:
            if rate.name == 'USD':
                dollar_rate = rate.rate_silent

            if rate.name == 'EUR':
                euro_rate = rate.rate_silent

        for rec in self:
            if rec.currency_id.name == 'EGP':
                rec.egypt_currency = rec.asking_price
                rec.euro_currency = euro_rate * rec.asking_price
                rec.dollar_currency = dollar_rate * rec.asking_price

            elif rec.currency_id.name == 'USD':
                rec.dollar_currency = rec.asking_price
                rec.egypt_currency = (1 / dollar_rate) * rec.asking_price
                rec.euro_currency = euro_rate * rec.egypt_currency


            elif rec.currency_id.name == 'EUR':
                rec.euro_currency = rec.asking_price
                rec.egypt_currency = (1 / euro_rate) * rec.asking_price
                rec.dollar_currency = dollar_rate * rec.egypt_currency

    dollar_currency = fields.Float("Dollar ($)", compute='calc_currency', store=True)
    egypt_currency = fields.Float("Egyption (EGP)", compute='calc_currency', store=True)
    euro_currency = fields.Float("Euro (EUR €)", compute='calc_currency', store=True)
