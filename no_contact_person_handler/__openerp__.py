# -*- coding: utf-8 -*-
{
    'name': "No contact person handler in client request",



    'description': """
        No contact person handler in client request
    """,

    'author': "Ahmed Tarek - IT Systems",
    'website': "www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'CRM',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','mt_crm_brokerage_manager'],


}