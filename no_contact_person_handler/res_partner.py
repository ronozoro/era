# -*- coding: utf-8 -*-

from openerp import models, fields, api


class ResPartnerInherit(models.Model):
    _inherit = "res.partner"

    @api.onchange('section_id')
    def on_change_user_section(self):
        lst1 = []
        res = {}
        user = self.env.user
        # for rec in self:
        if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
            self.user_id = user.id
            self.section_id = user.sale_team_id.id
            lst1.append(user.id)
            res['domain'] = {'user_id': [('id', 'in', lst1)]}
            # res['value'] = {'user_id': user.id}
        if self.section_id:
            lst1 = self.section_id.member_ids.ids
            res['value'] = {'user_id': self.env.user}
            res['domain'] = {'user_id': [('id', 'in', lst1)]}

            # return {'domain': {'user_id': [('id', 'in', lst1)]}}
        else:
            res['domain'] = {'user_id': [('id', 'in', lst1)]}
            # return {'domain': {'user_id': [('id', 'in', lst1)]}}
        return res

    # def get_sales_team(self):
    #     print "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhh"
    #     user = self.env['res.users'].browse(self.env.uid)
    #     if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
    #
    #         sales_team = user.sale_team_id.id
    #         print "##########################33",sales_team
    #         return {'domain': {'section_id': [('id', 'in', sales_team)]}}
    #     else:
    #         print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
    #         team_id = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)], limit=1)
    #         self.section_id = team_id.id
    #         teams_list = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)]).ids
    #         return {'domain': {'section_id': [('id', 'in', teams_list)]}}

    # @api.onchange('section_id')
    # def _get_my_list_teams(self):
    #     lst1 = []
    #     # for rec in self:
    #     if self.section_id:
    #         lst1 = self.section_id.member_ids.ids
    #         return {'domain': {'user_id': [('id', 'in', lst1)]}}
    #     else:
    #         return {'domain': {'user_id': [('id', 'in', lst1)]}}




    # @api.onchange('section_id')
    # def _get_my_list_teams(self):
    #     print "AHmeddddddddddddddddddddddddddd"
    #     team_id = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)], limit=1)
    #     self.section_id = team_id.id
    #     teams_list = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)]).ids
    #
    #     lst = []
    #     teams = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)])
    #
    #     for each in teams:
    #         for each_user in each.member_ids:
    #             if each_user == self.env.user:
    #                 lst.append(each_user.id)
    #                 print "QQQQQQQQQQQQQQQQQQQQ",lst
    #     return {'domain': {'section_id': [('id', 'in', teams_list)], 'user_id': [('id', 'in', lst)]}}

    def create(self, cr, uid, vals, context=None):
        vals.update({'user_id': uid})
        return super(ResPartnerInherit, self).create(cr, uid, vals, context=context)

    # @api.onchange('section_id')
    # def _get_my_list_teams(self):
    #     print "AHmeddddddddddddddddddddddddddd"
    #     team_id = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)], limit=1)
    #     self.section_id = team_id.id
    #     teams_list = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)]).ids
    #     return {'domain': {'section_id': [('id', 'in', teams_list)]}}
    #
    # @api.onchange('section_id')
    # def _get_my_list_users(self):
    #
    #     lst = []
    #     teams = self.env['crm.case.section'].search([('member_ids', '=', self.env.user.id)])
    #
    #
    #     for each in teams:
    #         for each_user in each.member_ids:
    #             lst.append(each_user.id)
    #
    #     return {'domain': {'user_id': [('id', 'in', lst)]}}
