# -*- coding: utf-8 -*-

from openerp import models, fields, api
import datetime
from mako.runtime import _inherit_from

from openerp.osv import fields
from openerp.osv import osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from dateutil.relativedelta import relativedelta
from openerp import SUPERUSER_ID
from openerp.osv.orm import setup_modifiers
from lxml import etree
import webbrowser


class no_contact_person_handler(models.Model):
    _inherit = "crm.lead"

    def on_change_partner_id(self, cr, uid, ids, partner_id, is_client, context=None):
        child_ids = []
        partner = self.pool.get('res.partner').browse(cr, uid, partner_id)
        if partner:
            title_name = ""
            if partner.title:
                title_name = self.pool.get('res.partner.title').browse(
                    cr, uid, partner.title.id).name + " " + partner.name
            else:
                title_name = partner.name
            print is_client
            if is_client == 'org':
                for child in partner.child_ids:
                    child_ids.append(child.id)

                if (partner.child_ids):
                    our_contact = self.pool.get('res.partner').browse(cr, uid, child_ids[0])
                    contact = our_contact.name
                    print contact

                elif not partner.child_ids:
                    raise osv.except_osv(_('Warning'), _('This Organization has no contact person'))


            else:
                contact = partner.name

            return {'value': {
                'name': title_name,
                'street': partner.street,
                'city': partner.city,
                'city_id': partner.city_id.id,
                'zip': partner.zip,
                'country_id': partner.country_id,
                'phone': partner.phone,
                'fax': partner.fax,
                'email_from': partner.email,
                'user_id': partner.user_id.id,
                'name_arabic': partner.name_arabic,
                #                 'organisation_id': partner.parent_id.id,
                'title': partner.title.id,
                'client_type_id': partner.client_type_id.id,
                'contact_name': contact,
            }
            }

    # @api.onchange('section_id')
    # def put_domain_tl(self):
    #     user = self.env['res.users'].browse(self.env.uid)
    #
    #     lst =[]
    #     if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
    #         lst2 = []
    #         print "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN"
    #         teams = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
    #
    #         print "JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJjj",teams
    #         for each in teams:
    #             for each_user in each.member_ids:
    #                 par = self.env['res.partner'].search([('id', '=', each_user)])
    #                 print "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN",each_user.name , par
    #                 lst2.append(each_user.id)
    #         return {'domain': {'partner_id': [('id', 'in', lst2)]}}


