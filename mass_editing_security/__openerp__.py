# -*- coding: utf-8 -*-
{
    'name': 'Mass Editing Security',
    'version': '8.0',
    'category': 'Other',
    'summary': 'Allow add mass editing with security groups',
    'depends': ['base','mass_editing'],
    'data': [
        'views/mass_editing_view.xml',
    ],
    'installable': True,
}
