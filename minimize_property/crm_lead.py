from openerp import models, fields, api
from datetime import date, datetime, time, timedelta
from pytz import timezone
from openerp.tools.translate import _
from openerp.exceptions import ValidationError


class CrmPhoneCall(models.Model):
    _inherit = 'crm.phonecall'

    @api.multi
    def unlink(self):
        user = self.env.user
        if user.has_group('base.group_system'):
            return
        if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            raise ValidationError(_('Agent Can not Delete any logged calls'))
        if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            raise ValidationError(_('TeamLeader Can not Delete any logged calls'))

        if user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group(
                'itsys_era_coordinator.group_division_head_resedential'):
            raise ValidationError(_('Resedential Coordinator Can not Delete any logged calls'))

        if user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group(
                'itsys_era_coordinator.group_division_head_commercial'):
            raise ValidationError(_('Commercial Coordinator Can not Delete any logged calls'))

    @api.model
    def _get_domain_user(self):
        user = self.env.user
        if user.has_group('base.group_system'):
            return
        if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            return [('id', '=', user.id)]
        if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            return [('id', 'in', user.sale_team_id.member_ids.ids)]

        if user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group(
                'itsys_era_coordinator.group_division_head_resedential'):
            return [('id', 'in', user.sale_team_id.member_ids.ids)]

        if user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group(
                'itsys_era_coordinator.group_division_head_commercial'):
            return [('id', 'in', user.sale_team_id.member_ids.ids)]
        return [('id', 'in', self.env['res.users'].sudo().search([])._ids)]

    user_id = fields.Many2one('res.users', string='Responsible', domain=lambda self: self._get_domain_user())

    def on_change_partner_id(self, cr, uid, ids, partner_id):
        res = super(CrmPhoneCall, self).on_change_partner_id(cr, uid, ids, partner_id)
        user = self.pool.get('res.users').browse(cr, uid, uid)
        if user.has_group('base.group_system'):
            return res
        if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            res['domain'] = {'partner_id': [('customer', '=', True), ('user_id', '=', user.id)]}
        if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            res['domain'] = {
                'partner_id': [('customer', '=', True), ('user_id', 'in', user.sale_team_id.member_ids.ids)]}

        if user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group(
                'itsys_era_coordinator.group_division_head_resedential'):
            res['domain'] = {
                'partner_id': [('customer', '=', True), ('user_id', 'in', user.sale_team_id.member_ids.ids)]}

        if user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group(
                'itsys_era_coordinator.group_division_head_commercial'):
            res['domain'] = {
                'partner_id': [('customer', '=', True), ('user_id', 'in', user.sale_team_id.member_ids.ids)]}

        return res


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.model
    @api.multi
    def open_closed(self):
        # here you can filter you records as you want
        records = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
        search_view_id = self.env.ref('base.view_partner_tree')
        lst = []
        for each in records:
            for each_user in each.member_ids:
                lst.append(each_user.id)

        partns = self.search([('user_id', 'in', lst)])
        return {
            'name': _('Clients'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form,kanban',
            'res_model': 'res.partner',
            'search_view_id': search_view_id.id,
            'target': 'current',
            'domain': [('id', 'in', partns.ids)]
        }

    @api.model
    @api.onchange('email')
    def check_email(self):
        return {'warning': {'title': 'warning',
                            'message': "you should enter E-mail to be able to receive mails from MailChimp"}}


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    company_id = fields.Many2one(comodel_name='operating.unit', string='Branch',
                                 default=lambda self: self.env['res.users'].operating_unit_default_get(self._uid))

    # @api.model
    # def _get_domain_property(self):
    #     user = self.env.user
    #     lst = []
    #     if user.has_group('base.group_system'):
    #         print "ADMIN"
    #         return [('category', '=', self.category.id)]
    #     if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
    #         return [('agent_id', '=', user.id), ('category', '=', self.category.id)]
    #     if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
    #         return [('category', '=', self.category.id), '|', ('agent_id', '=', user.id),
    #                 ('agent_id', 'in', user.sale_team_id.member_ids.ids)]
    #
    #     if user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
    #         records = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')])
    #         for rec in records:
    #             for each in rec.member_ids:
    #                 lst.append(each.id)
    #
    #         return [('agent_id', 'in', lst), ('category.categ_type', '=', 'residential')]
    #
    #     #
    #     # if user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
    #     #     return [('id', 'in', user.sale_team_id.member_ids.ids)]
    #     return [('id', 'in', self.env['product.template'].sudo().search([])._ids)]
    #
    # property = fields.Many2one('product.template', string='Property', domain=lambda self: self._get_domain_property())


    @api.onchange('property')
    def change_property(self):
        res = {}
        lst = []
        user = self.env.user

        if user.has_group('base.group_system'):
            prop = self.env['product.template'].search([]).ids
            res['domain'] = {'property': [('id', 'in', prop)]}

        if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            res['domain'] = {'property': [('agent_id', '=', user.id), ('category', '=', self.category.id)]}
        if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            res['domain'] = {'property': [('category', '=', self.category.id), '|', ('agent_id', '=', user.id),
                                          ('agent_id', 'in', user.sale_team_id.member_ids.ids)]}

        if user.has_group('itsys_era_coordinator.group_coordinator_resedential'):
            records = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')])
            for rec in records:
                for each in rec.member_ids:
                    lst.append(each.id)
            prop = self.env['product.template'].search(
                [('agent_id', 'in', lst), ('category.categ_type', '=', 'residential')]).ids

            res['domain'] = {'property': [('id', 'in', prop)]}
        if user.has_group('itsys_era_coordinator.group_coordinator_commercial'):
            records = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
            for rec in records:
                for each in rec.member_ids:
                    lst.append(each.id)
            prop = self.env['product.template'].search(
                [('agent_id', 'in', lst), ('category.categ_type', '=', 'commercial')]).ids

            res['domain'] = {'property': [('id', 'in', prop)]}

        if user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
            records = self.env['crm.case.section'].search([('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
            for rec in records:
                for each in rec.member_ids:
                    lst.append(each.id)
            prop = self.env['product.template'].search([('agent_id', 'in', lst)]).ids

            res['domain'] = {'property': [('id', 'in', prop)]}

        return res

# def onchange_client_code_id(self, cr, uid, ids, is_client):
#     res = super(CrmLead, self).onchange_client_code_id(cr, uid, ids, is_client)
#     user = self.pool.get('res.users').browse(cr, uid, uid)
#     if is_client == 'client':
#         if user.has_group('base.group_system'):
#             res['domain'] = {'partner_id': [("is_company", "=", False),
#                                             ('customer', '=', True)]}
#             return True
#         if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
#             print "AGENT"
#             res['domain'] = {'partner_id': [("is_company", "=", False),
#                                             ('customer', '=', True),
#                                             ('user_id', '=', uid)]}
#         elif user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
#             print "TEAMLEADER"
#             res['domain'] = {'partner_id': [("is_company", "=", False),
#                                             ('customer', '=', True),
#                                             ('user_id', 'in', user.sale_team_id.member_ids.ids)]}
#
#         elif user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group('itsys_era_coordinator.group_division_head_resedential'):
#             print "coordinator_resedential"
#             res['domain'] = {'partner_id': [("is_company", "=", False),
#                                             ('customer', '=', True),
#                                             ('user_id', 'in', user.sale_team_id.member_ids.ids)]}
#
#         elif user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group('itsys_era_coordinator.group_division_head_commercial'):
#             print "coordinator_commercial"
#             res['domain'] = {'partner_id': [("is_company", "=", False),
#                                             ('customer', '=', True),
#                                             ('user_id', 'in', user.sale_team_id.member_ids.ids)]}
#
#
#     elif is_client == 'org':
#         if user.has_group('base.group_system'):
#             res['domain'] = {'partner_id': [("is_company", "=", True),
#                                             ('customer', '=', True)]}
#             return True
#         if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
#             print "AGENT"
#             res['domain'] = {'partner_id': [("is_company", "=", True),
#                                             ('customer', '=', True),
#                                             ('user_id', '=', uid)]}
#         elif user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
#             print "TEAMLEADER"
#             res['domain'] = {'partner_id': [("is_company", "=", True),
#                                             ('customer', '=', True),
#                                             ('user_id', 'in', user.sale_team_id.member_ids.ids)]}
#
#         elif user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group('itsys_era_coordinator.group_division_head_resedential'):
#             print "coordinator_resedential"
#             res['domain'] = {'partner_id': [("is_company", "=", False),
#                                             ('customer', '=', True),
#                                             ('user_id', 'in', user.sale_team_id.member_ids.ids)]}
#
#         elif user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group('itsys_era_coordinator.group_division_head_commercial'):
#             print "coordinator_commercial"
#             res['domain'] = {'partner_id': [("is_company", "=", False),
#                                             ('customer', '=', True),
#                                             ('user_id', 'in', user.sale_team_id.member_ids.ids)]}
#
#     return res

    @api.one
    @api.constrains('budget')
    def check_budget(self):
        if self.budget2 < 0:
            raise Warning("the max budget MUST be Positive number")
        if self.budget < 0:
            raise Warning("the min budget MUST be Positive number")
        if self.budget2 < self.budget:
            raise Warning("the max budget Must be > min budget")


    @api.model
    def _get_my_list(self):
        user = self.env.user
        domain = []
        if user.has_group('base.group_system'):
            if self._context.get('default_is_buyer') is not None:
                if self._context.get('default_is_buyer') == 1:
                    selections = [('buy', 'Buy'),
                                  ('buyer_rent', 'Rent (Buyer)'),
                                  ('share_buyer', 'Share (Buyer)')]
                else:
                    selections = [('sale', 'Sale'),
                                  ('seller_rent', 'Rent (Seller)'),
                                  ('share_seller', 'Share (Seller)')]
                domain.extend(selections)
            else:
                selections = [('sale', 'Sale'),
                              ('seller_rent', 'Rent (Seller)'),
                              ('share_seller', 'Share (Seller)'),
                              ('buy', 'Buy'),
                              ('buyer_rent', 'Rent (Buyer)'),
                              ('share_buyer', 'Share (Buyer)')
                              ]
                domain.extend(selections)
            return list(set(domain))

        if user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
            if self._context.get('default_is_buyer') is not None:
                if self._context.get('default_is_buyer') == 1:
                    selections = [('buy', 'Buy'),
                                  ('buyer_rent', 'Rent (Buyer)'),
                                  ('share_buyer', 'Share (Buyer)')]
                if self._context.get('default_is_buyer') == 0:
                    selections = [('sale', 'Sale'),
                                  ('seller_rent', 'Rent (Seller)'),
                                  ('share_seller', 'Share (Seller)')]
                domain.extend(selections)
            else:
                selections = [('sale', 'Sale'),
                              ('seller_rent', 'Rent (Seller)'),
                              ('share_seller', 'Share (Seller)'),
                              ('buy', 'Buy'),
                              ('buyer_rent', 'Rent (Buyer)'),
                              ('share_buyer', 'Share (Buyer)')
                              ]
                domain.extend(selections)
            return list(set(domain))

        if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            if self._context.get('default_is_buyer') is not None:
                if self._context.get('default_is_buyer') == 1:
                    selections = [('buy', 'Buy'),
                                  ('buyer_rent', 'Rent (Buyer)'),
                                  ('share_buyer', 'Share (Buyer)')]
                else:
                    selections = [('sale', 'Sale'),
                                  ('seller_rent', 'Rent (Seller)'),
                                  ('share_seller', 'Share (Seller)')]
                domain.extend(selections)
            else:
                selections = [('sale', 'Sale'),
                              ('seller_rent', 'Rent (Seller)'),
                              ('share_seller', 'Share (Seller)'),
                              ('buy', 'Buy'),
                              ('buyer_rent', 'Rent (Buyer)'),
                              ('share_buyer', 'Share (Buyer)')
                              ]
                domain.extend(selections)

        if user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group(
                'itsys_era_coordinator.group_division_head_resedential'):
            if self._context.get('default_is_buyer') is not None:
                if self._context.get('default_is_buyer') == 1:
                    selections = [('buy', 'Buy'),
                                  ('buyer_rent', 'Rent (Buyer)'),
                                  ('share_buyer', 'Share (Buyer)')]
                else:
                    selections = [('sale', 'Sale'),
                                  ('seller_rent', 'Rent (Seller)'),
                                  ('share_seller', 'Share (Seller)')]
                domain.extend(selections)
            else:
                selections = [('sale', 'Sale'),
                              ('seller_rent', 'Rent (Seller)'),
                              ('share_seller', 'Share (Seller)'),
                              ('buy', 'Buy'),
                              ('buyer_rent', 'Rent (Buyer)'),
                              ('share_buyer', 'Share (Buyer)')
                              ]
                domain.extend(selections)

        if user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group(
                'itsys_era_coordinator.group_division_head_commercial'):
            if self._context.get('default_is_buyer') is not None:
                if self._context.get('default_is_buyer') == 1:
                    selections = [('buy', 'Buy'),
                                  ('buyer_rent', 'Rent (Buyer)'),
                                  ('share_buyer', 'Share (Buyer)')]
                else:
                    selections = [('sale', 'Sale'),
                                  ('seller_rent', 'Rent (Seller)'),
                                  ('share_seller', 'Share (Seller)')]
                domain.extend(selections)
            else:
                selections = [('sale', 'Sale'),
                              ('seller_rent', 'Rent (Seller)'),
                              ('share_seller', 'Share (Seller)'),
                              ('buy', 'Buy'),
                              ('buyer_rent', 'Rent (Buyer)'),
                              ('share_buyer', 'Share (Buyer)')
                              ]
                domain.extend(selections)
            return list(set(domain))

        if user.has_group('mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
            selections = [('buy', 'Buy'),
                          ('buyer_rent', 'Rent (Buyer)'),
                          ('share_buyer', 'Share (Buyer)')]
            domain.extend(selections)

        if user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id'):
            selections = [('buy', 'Buy'),
                          ('buyer_rent', 'Rent (Buyer)'),
                          ('share_buyer', 'Share (Buyer)')]
            domain.extend(selections)

        if user.has_group('mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
            selections = [('sale', 'Sale'),
                          ('seller_rent', 'Rent (Seller)'),
                          ('share_seller', 'Share (Seller)')]
            domain.extend(selections)

        if user.has_group('mt_crm_brokerage_manager.group_residential_seller_agent_id'):
            selections = [('sale', 'Sale'),
                          ('seller_rent', 'Rent (Seller)'),
                          ('share_seller', 'Share (Seller)')]
            domain.extend(selections)

        return list(set(domain))


    service_ids = fields.Selection(selection=_get_my_list)


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    _defaults = {
        'created_time': lambda self, cr, uid, context=None: fields.datetime.now(),
    }

    @api.onchange('partner_id')
    def change_partner(self):
        lst = []
        user = self.env.user
        if not self.partner_id:
            res = {}
            if user.has_group('base.group_system'):
                res['domain'] = {'partner_id': [('customer', '=', True)]}
                return
            if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
                res['domain'] = {'partner_id': [('user_id', '=', user.id), ('customer', '=', True)]}
            if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
                sales_team = self.env['crm.case.section'].search([('user_id', '=', self.env.user.id)])
                for rec in sales_team:
                    for user in rec.member_ids:
                        lst.append(user.id)

                pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', lst)])

                res['domain'] = {'partner_id': [('id', 'in', pars.ids)]}

                # res['domain'] = {'partner_id': [('customer', '=', True),
                #                               ('user_id', 'in', user.sale_team_id.member_ids.ids)]}

            if user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group(
                    'itsys_era_coordinator.group_division_head_resedential'):
                sales_team = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'residential')])
                for rec in sales_team:
                    for user in rec.member_ids:
                        lst.append(user.id)

                pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', lst)])

                res['domain'] = {'partner_id': [('id', 'in', pars.ids)]}

                # res['domain'] = {'partner_id': [('customer', '=', True),
                #                               ('user_id', 'in', user.sale_team_id.member_ids.ids)]}

            if user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group(
                    'itsys_era_coordinator.group_division_head_commercial'):
                sales_team = self.env['crm.case.section'].search([('category_id.categ_type', '=', 'commercial')])
                for rec in sales_team:
                    for user in rec.member_ids:
                        lst.append(user.id)

                pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', lst)])

                res['domain'] = {'partner_id': [('id', 'in', pars.ids)]}

            if user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
                sales_team = self.env['crm.case.section'].search(
                    [('operating_unit_id', '=', self.env.user.sale_team_id.operating_unit_id.id)])
                for rec in sales_team:
                    for user in rec.member_ids:
                        lst.append(user.id)

                pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', lst)])

                res['domain'] = {'partner_id': [('id', 'in', pars.ids)]}

            if user.has_group('base.group_system'):
                sales_team = self.env['crm.case.section'].search([])
                for rec in sales_team:
                    for user in rec.member_ids:
                        lst.append(user.id)

                pars = self.env['res.partner'].search([('customer', '=', True), ('user_id', 'in', lst)])

                res['domain'] = {'partner_id': [('id', 'in', pars.ids)]}

            return res


class CalendarEvent(models.TransientModel):
    _inherit = 'calendar.event'

    @api.model
    def _get_my_list(self):
        user = self.env.user
        domain = []
        if user.has_group('base.group_system'):
            if self._context.get('default_is_buyer') is not None:
                if self._context.get('default_is_buyer') == 1:
                    selections = [('buy', 'Buy'),
                                  ('buyer_rent', 'Rent (Buyer)'),
                                  ('share_buyer', 'Share (Buyer)')]
                else:
                    selections = [('sale', 'Sale'),
                                  ('seller_rent', 'Rent (Seller)'),
                                  ('share_seller', 'Share (Seller)')]
                domain.extend(selections)
            else:
                selections = [('sale', 'Sale'),
                              ('seller_rent', 'Rent (Seller)'),
                              ('share_seller', 'Share (Seller)'),
                              ('buy', 'Buy'),
                              ('buyer_rent', 'Rent (Buyer)'),
                              ('share_buyer', 'Share (Buyer)')
                              ]
                domain.extend(selections)
            return list(set(domain))

        if user.has_group('mt_crm_brokerage_manager.group_ceo_id'):
            if self._context.get('default_is_buyer') is not None:
                if self._context.get('default_is_buyer') == 1:
                    selections = [('buy', 'Buy'),
                                  ('buyer_rent', 'Rent (Buyer)'),
                                  ('share_buyer', 'Share (Buyer)')]
                else:
                    selections = [('sale', 'Sale'),
                                  ('seller_rent', 'Rent (Seller)'),
                                  ('share_seller', 'Share (Seller)')]
                domain.extend(selections)
            else:
                selections = [('sale', 'Sale'),
                              ('seller_rent', 'Rent (Seller)'),
                              ('share_seller', 'Share (Seller)'),
                              ('buy', 'Buy'),
                              ('buyer_rent', 'Rent (Buyer)'),
                              ('share_buyer', 'Share (Buyer)')
                              ]
                domain.extend(selections)
            return list(set(domain))

        if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
            if self._context.get('default_is_buyer') is not None:
                if self._context.get('default_is_buyer') == 1:
                    selections = [('buy', 'Buy'),
                                  ('buyer_rent', 'Rent (Buyer)'),
                                  ('share_buyer', 'Share (Buyer)')]
                else:
                    selections = [('sale', 'Sale'),
                                  ('seller_rent', 'Rent (Seller)'),
                                  ('share_seller', 'Share (Seller)')]
                domain.extend(selections)
            else:
                selections = [('sale', 'Sale'),
                              ('seller_rent', 'Rent (Seller)'),
                              ('share_seller', 'Share (Seller)'),
                              ('buy', 'Buy'),
                              ('buyer_rent', 'Rent (Buyer)'),
                              ('share_buyer', 'Share (Buyer)')
                              ]
                domain.extend(selections)

        if user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group(
                'itsys_era_coordinator.group_division_head_resedential'):
            if self._context.get('default_is_buyer') is not None:
                if self._context.get('default_is_buyer') == 1:
                    selections = [('buy', 'Buy'),
                                  ('buyer_rent', 'Rent (Buyer)'),
                                  ('share_buyer', 'Share (Buyer)')]
                else:
                    selections = [('sale', 'Sale'),
                                  ('seller_rent', 'Rent (Seller)'),
                                  ('share_seller', 'Share (Seller)')]
                domain.extend(selections)
            else:
                selections = [('sale', 'Sale'),
                              ('seller_rent', 'Rent (Seller)'),
                              ('share_seller', 'Share (Seller)'),
                              ('buy', 'Buy'),
                              ('buyer_rent', 'Rent (Buyer)'),
                              ('share_buyer', 'Share (Buyer)')
                              ]
                domain.extend(selections)

        if user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group(
                'itsys_era_coordinator.group_division_head_commercial'):
            if self._context.get('default_is_buyer') is not None:
                if self._context.get('default_is_buyer') == 1:
                    selections = [('buy', 'Buy'),
                                  ('buyer_rent', 'Rent (Buyer)'),
                                  ('share_buyer', 'Share (Buyer)')]
                else:
                    selections = [('sale', 'Sale'),
                                  ('seller_rent', 'Rent (Seller)'),
                                  ('share_seller', 'Share (Seller)')]
                domain.extend(selections)
            else:
                selections = [('sale', 'Sale'),
                              ('seller_rent', 'Rent (Seller)'),
                              ('share_seller', 'Share (Seller)'),
                              ('buy', 'Buy'),
                              ('buyer_rent', 'Rent (Buyer)'),
                              ('share_buyer', 'Share (Buyer)')
                              ]
                domain.extend(selections)

        if user.has_group('mt_crm_brokerage_manager.group_commercial_buyer_agent_id'):
            selections = [('buy', 'Buy'),
                          ('buyer_rent', 'Rent (Buyer)'),
                          ('share_buyer', 'Share (Buyer)')]
            domain.extend(selections)

        if user.has_group('mt_crm_brokerage_manager.group_residential_buyer_agent_id'):
            selections = [('buy', 'Buy'),
                          ('buyer_rent', 'Rent (Buyer)'),
                          ('share_buyer', 'Share (Buyer)')]
            domain.extend(selections)

        if user.has_group('mt_crm_brokerage_manager.group_commercial_seller_agent_id'):
            selections = [('sale', 'Sale'),
                          ('seller_rent', 'Rent (Seller)'),
                          ('share_seller', 'Share (Seller)')]
            domain.extend(selections)

        if user.has_group('mt_crm_brokerage_manager.group_residential_seller_agent_id'):
            selections = [('sale', 'Sale'),
                          ('seller_rent', 'Rent (Seller)'),
                          ('share_seller', 'Share (Seller)')]
            domain.extend(selections)

        return list(set(domain))

    service_type = fields.Selection(selection=_get_my_list)

# class CrmLead2OpportunityPartner(models.TransientModel):
#     _inherit = 'crm.lead2opportunity.partner'
#
#     @api.model
#     def _get_domain_partner(self):
#         user = self.env.user
#         if user.has_group('base.group_system'):
#             return[('customer', '=', True)]
#         if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
#             return [('customer', '=', True),('user_id', '=', user.id)]
#         if user.has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
#             return [('customer', '=', True),('user_id', 'in', user.sale_team_id.member_ids.ids)]
#
#         if user.has_group('itsys_era_coordinator.group_coordinator_resedential') or user.has_group('itsys_era_coordinator.group_division_head_resedential'):
#             return [('customer', '=', True),('user_id', 'in', user.sale_team_id.member_ids.ids)]
#
#         if user.has_group('itsys_era_coordinator.group_coordinator_commercial') or user.has_group('itsys_era_coordinator.group_division_head_commercial'):
#             return [('customer', '=', True),('user_id', 'in', user.sale_team_id.member_ids.ids)]
#         return [('user_id', 'in', self.env['res.partner'].sudo().search([])._ids)]
#
#     partner_id = fields.Many2one('res.partner', string='Customer', domain=lambda self: self._get_domain_partner())
#
