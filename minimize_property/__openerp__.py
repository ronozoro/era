# -*- coding: utf-8 -*-
{
    'name': "Minimize Property Name",

    'summary': """Minimize the name of the property in the property profile""",

    'description': """Minimize the name of the property in the property profile""",

    'author': "ITSYS BY Shimaa",
    'website': "http://www.it-syscorp.com",

    'category': 'Sale',
    'version': '0.1',

    'depends': ['base', 'mt_crm_proprety' ,'itsys_era_coordinator', 'mt_crm_brokerage_manager','itsys_assign_to_sales_team'],

    'data': [
        'views.xml',
    ],

}