# -*- coding: utf-8 -*-

from openerp import models, fields, api


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    @api.model
    def create(self, vals):
        if vals.get('name',False):
            vals['name'] = vals['name'][0:127]
        res = super(ProductTemplate, self).create(vals)
        return res

    @api.multi
    def write(self, vals):
        if vals.get('name',False):
            vals['name'] = vals['name'][0:127]
        res = super(ProductTemplate, self).write(vals)
        return res


