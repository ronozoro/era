
# -*- coding: utf-8 -*-
##############################################################################
#
#     This file is part of distribution_list, an Odoo module.
#
#     Copyright (c) 2015 ACSONE SA/NV (<http://acsone.eu>)
#
#     distribution_list is free software: you can redistribute it and/or
#     modify it under the terms of the GNU Affero General Public License
#     as published by the Free Software Foundation, either version 3 of
#     the License, or (at your option) any later version.
#
#     distribution_list is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the
#     GNU Affero General Public License
#     along with distribution_list.
#     If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'its_merga_leads',
    'version': '1.0',
    "author": "IT-SYS Corporation By Said YAHIA",
    'category': 'Marketing',
    'depends': [
        'calendar',
        'base',
        'web',
        'mt_crm_brokerage_manager',
        'sale',
        'crm',
        'survey',
        'itsys_era_coordinator',
    ],
    'description': """
its_merga_leads
=================

""",
    'data': [
        # 'security/security.xml',
        # 'security/ir.model.access.csv',
        'view/crm_merge_opportunities_view.xml',
    ],

    'test': [
    ],
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': False,
}

