##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import api

class crm_merge_opportunity(osv.osv_memory):

    _inherit = 'crm.merge.opportunity'

    @api.onchange('section_id')
    def on_change_user_section(self):
        lst1 = []
        res={}
        user = self.env.user
        # for rec in self:
        if user.has_group('mt_crm_brokerage_manager.group_agent_id'):
            print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
            self.user_id = user.id
            self.section_id = user.sale_team_id.id
            lst1.append(user.id)
            res['domain'] = {'user_id': [('id', 'in', lst1)]}
            # res['value'] = {'user_id': user.id}
        if self.section_id:
            lst1 = self.section_id.member_ids.ids
            res['value'] = {'user_id': self.env.user}
            res['domain'] = {'user_id': [('id', 'in', lst1)]}

            # return {'domain': {'user_id': [('id', 'in', lst1)]}}
        else:
            res['domain'] = {'user_id': [('id', 'in', lst1)]}
            # return {'domain': {'user_id': [('id', 'in', lst1)]}}
        return res





    # @api.multi
    # @api.onchange('section_id')
    # def get_clients_per_user(self):
    #     users = []
    #     pars = []
    #     user = self.env['res.users'].browse(self.env.uid)
    #     print "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWww",self.opportunity_ids ,self.opportunity_ids.type

    # def on_change_user(self, cr, uid, ids, user_id, section_id, context=None):
    #
    #     """ When changing the user, also set a section_id or restrict section id
    #         to the ones user_id is member of. """
    #
    #     print "3333333333333333333333333333333333333333333"
    #     res={}
    #     lst = []
    #     if self.pool.get('res.users').browse(cr, uid).has_group('mt_crm_brokerage_manager.group_agent_id'):
    #         userss=[]
    #         userss.append(uid)
    #         user_id=uid
    #         res['domain'] = {'user_id': [('id', 'in', userss)]}
    #         res['value'] = {'user_id': uid}
    #     if self.pool.get('res.users').browse(cr, uid).has_group('mt_crm_brokerage_manager.group_see_his_team_id'):
    #         records =self.pool.get('crm.case.section').search(cr, uid,[('user_id', '=', uid)])
    #         recordslst =self.pool.get('crm.case.section').browse(cr, uid,records)
    #         for each in recordslst:
    #             for each_user in each.member_ids:
    #                 lst.append(each_user.id)
    #         res['domain'] = {'user_id': [('id', 'in', lst)]}
    #
    #     if self.pool.get('res.users').browse(cr, uid).has_group('itsys_era_coordinator.group_coordinator_resedential'):
    #         records = self.pool.get('crm.case.section').search(cr, uid, [('category_id.categ_type','=','residential')])
    #         recordslst = self.pool.get('crm.case.section').browse(cr, uid, records)
    #
    #         for each in recordslst:
    #             for each_user in each.member_ids:
    #                 lst.append(each_user.id)
    #         res['domain'] = {'user_id': [('id', 'in', lst)]}
    #
    #     if self.pool.get('res.users').browse(cr, uid).has_group('itsys_era_coordinator.group_coordinator_commercial'):
    #         records = self.pool.get('crm.case.section').search(cr, uid,[('category_id.categ_type','=','commercial')])
    #         recordslst = self.pool.get('crm.case.section').browse(cr, uid, records)
    #
    #
    #         for each in recordslst:
    #             for each_user in each.member_ids:
    #                 lst.append(each_user.id)
    #         res['domain'] = {'user_id': [('id', 'in', lst)]}
    #
    #     if self.pool.get('res.users').browse(cr, uid).has_group('mt_crm_brokerage_manager.group_ceo_id'):
    #         records = self.pool.get('crm.case.section').search(cr, uid, [('operating_unit_id', '=', self.pool.get('res.users').browse(cr, uid, uid).sale_team_id.operating_unit_id.id)])
    #         recordslst = self.pool.get('crm.case.section').browse(cr, uid, records)
    #
    #         for each in recordslst:
    #             if each:
    #                 for each_user in each.member_ids:
    #                     lst.append(each_user.id)
    #                 res['domain'] = {'user_id': [('id', 'in', lst)]}
    #             else:
    #                 res['domain'] = {'user_id': [('id', 'in', lst)]}
    #
    #
    #     if self.pool.get('res.users').browse(cr, uid).has_group('base.group_system'):
    #         records = self.pool.get('crm.case.section').search(cr, uid, [])
    #         recordslst = self.pool.get('crm.case.section').browse(cr, uid, records)
    #
    #         for each in recordslst:
    #             for each_user in each.member_ids:
    #                 lst.append(each_user.id)
    #         res['domain'] = {'user_id': [('id', 'in', lst)]}
    #
    #
    #
    #     # if user_id:
    #     #     if section_id:
    #     #         user_in_section = self.pool.get('crm.case.section').search(cr, uid, [('id', '=', section_id), '|', ('user_id', '=', user_id), ('member_ids', '=', user_id)], context=context, count=True)
    #     #     else:
    #     #         user_in_section = False
    #     #     if not user_in_section:
    #     #         section_id = False
    #     #         section_ids = self.pool.get('crm.case.section').search(cr, uid, ['|', ('user_id', '=', user_id), ('member_ids', '=', user_id)], context=context)
    #     #         if section_ids:
    #     #             section_id = section_ids[0]
    #     # res['value'] = {'section_id': section_id}
    #     return res

        # return {'value': {'section_id': section_id}}


#
# # vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
