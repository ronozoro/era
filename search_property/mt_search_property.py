# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from openerp import api
from openerp.tools.translate import _



class MtSearchProperty(osv.osv_memory):
    _inherit = 'mt.search_property'

    _columns = {
        'service_type': fields.selection([('buy', 'Buy'),
                                          ('sale', 'Sale'),
                                          ('buyer_rent', 'Rent (Buyer)'),
                                          ('seller_rent', 'Rent (Seller)'),
                                          ('share_buyer', 'Share (Buyer)'),
                                          ('share_seller', 'Share (Seller)')],
                                         'Service Type', translate=True),

        'bedrooms_no_ids': fields.many2one('mt.bedrooms', string='No. of Bedrooms'),
        'reference_property': fields.char("Property ID"),
        'area_sqm_max': fields.float('Maximum Area'),
        'area_sqm_min': fields.float('Minimum Area'),
    }

    @api.constrains('area_sqm_min','area_sqm_max', 'asking_price_moins_que', 'asking_price_plus_que')
    def check_number(self):
        for rec in self:
            if rec.area_sqm_max < 0.00 or rec.area_sqm_min < 0.00 \
                    or rec.asking_price_moins_que < 0.00 or rec.asking_price_plus_que < 0.00:
                raise Warning("the Number must be POSITIVE")

    @api.constrains('area_sqm_max', 'asking_price_plus_que')
    def check_amount(self):
        for rec in self:
            if rec.area_sqm_max >= 0.00:
                if rec.area_sqm_max < rec.area_sqm_min:
                    raise Warning("the max amount must be greater than min amount")
            if rec.asking_price_plus_que >= 0.00:
                if rec.asking_price_plus_que < rec.asking_price_moins_que:
                    raise Warning("the max amount must be greater than min amount")


    def button_ok(self, cr, uid, ids, context=None):
        data_obj = self.pool.get('ir.model.data')
        id2 = data_obj._get_id(cr, uid, 'mt_crm_proprety', 'product_template_form_view_b2b')
        if id2:
            id2 = data_obj.browse(cr, uid, id2, context=context).res_id

        id3 = data_obj._get_id(cr, uid, 'mt_crm_proprety', 'mt_property_tree')
        if id3:
            id3 = data_obj.browse(cr, uid, id3, context=context).res_id

        domain = []
        for rec in self.browse(cr, uid, ids):

            if rec.category.categ_type:
                domain.append(('category.categ_type', '=', rec.category.categ_type))

            if rec.district_ids.id:
                domain.append(('district_ids', '=', rec.district_ids.id))

            if rec.neighborhood_ids.id:
                domain.append(('neighborhood_ids', '=', rec.neighborhood_ids.id))

            if rec.bedrooms_no_ids.id:
                domain.append(('bedrooms_no_ids', '=', rec.bedrooms_no_ids.id))

            if rec.property_type_id.id:
                domain.append(('property_type_id', '=', rec.property_type_id.id))

            if rec.service_type:
                domain.append(('service_type', '=', rec.service_type))

            if rec.reference_property:
                domain.append(('reference_property', 'ilike', rec.reference_property))

            if rec.kitchen_type_id.id:
                domain.append(('kitchen_type_id', '=', rec.kitchen_type_id.id))

            if rec.reception_nbr:
                domain.append(('reception_nbr', '=', rec.reception_nbr))

            if rec.blaconies_nbr:
                domain.append(('blaconies_nbr', '=', rec.blaconies_nbr))

            if rec.bathrooms_no_ids.id:
                domain.append(('bathrooms_no_ids', '=', rec.bathrooms_no_ids.id))

            if rec.facilities_ids.id:
                domain.append(('facilities_ids', '=', rec.facilities_ids.id))

            if rec.view_ids.id:
                domain.append(('view_ids', '=', rec.view_ids.id))

            if rec.finsishing_id.id:
                domain.append(('finsishing_id', '=', rec.finsishing_id.id))

            if rec.currency_id.id:
                domain.append(('currency_id', '=', rec.currency_id.id))

            if rec.asking_price_moins_que:
                domain.append(('asking_price', '>=', rec.asking_price_moins_que))

            if rec.asking_price_plus_que:
                domain.append(('asking_price', '<=', rec.asking_price_plus_que))

            if rec.area_sqm_min:
                domain.append(('area_sqm', '>=', rec.area_sqm_min))

            if rec.area_sqm_max:
                domain.append(('area_sqm', '<=', rec.area_sqm_max))

        properties = self.pool.get('product.template').search(cr, uid, domain, context=context)

        value = {
            'name': _('Properties'),
            'domain': [('id', 'in', properties)],
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'product.template',
            'view_id': False,
            "views": [[id3, "tree"], [id2, "form"]],
            'type': 'ir.actions.act_window',
            'target': 'current',
        }
        return value
