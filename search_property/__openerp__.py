# -*- coding: utf-8 -*-
{
    'name': "search_property",

    'summary': """ Search Property""",

    'description': """ Search Property""",

    'author': "Shimaa",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Sales',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mt_crm_proprety', 'search_criteria'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'mt_search_property_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo.xml',
    ],
}