# -*- coding: utf-8 -*-
{
    'name': "Budget visibility",

    'description': """
        Hide budget in seller screen
    """,

    'author': "Ahmed Tarek - IT Systems",
    'website': "www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Sale',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','client_req_enhancement','mt_crm_brokerage_manager','crm'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'budget_visibility.xml',
    ],
}