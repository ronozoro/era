# -*- coding: utf-8 -*-
{
    'name': "Adding messaging to developers and projects views",

    'summary': """
        Adding messaging to developers and projects views""",


    'author': "Ahmed Tarek - IT Systems",
    'website': "www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Sales',
    'version': '1',

    # any module necessary for this one to work correctly
    'depends': ['base','mt_crm_proprety','product','mail'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}